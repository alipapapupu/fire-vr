using FMODUnity;

public enum Busses{
	Sounds = 0,
	Music = 1,
	Master = 2,
	Reverb = 3}
	 public static class AllBusses {
 public static string[] busPaths = new string[]
{ 

"bus:/Master/Sounds",

"bus:/Master/Music",

"bus:/Master",

"bus:/Reverb"
};
 public static FMOD.Studio.Bus GetBus(Busses bus) 
 { 
return RuntimeManager.GetBus(busPaths[(int)bus]);
}
}
