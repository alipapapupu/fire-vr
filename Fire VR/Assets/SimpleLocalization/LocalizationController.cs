﻿using Assets.SimpleLocalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LocalizationController : MonoBehaviour
{
	[SerializeField]
	private UnityEvent OnGameStart;

	readonly string saveName = "language";

    private void Awake()
    {
		LocalizationManager.Read();

		OnGameStart?.Invoke();

		InitialiseWithSystemLanguage();

		SwitchLanguage(PlayerPrefs.GetString(saveName, LocalizationManager.Language));
    }

    public void InitialiseWithSystemLanguage()
	{
		switch (Application.systemLanguage)
		{
			case SystemLanguage.Finnish:
				LocalizationManager.Language = "Finnish";
				break;
			case SystemLanguage.Swedish:
				//LocalizationManager.Language = "Swedish";
				break;
			default:
				LocalizationManager.Language = "English";
				break;
		}
	}

	public void SwitchLanguage(string language)
    {
		LocalizationManager.Language = language;
		PlayerPrefs.SetString(saveName, language);
		PlayerPrefs.Save();
    }
}
