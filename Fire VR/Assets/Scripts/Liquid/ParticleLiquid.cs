﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MohaviCreative.Sounds;

public class ParticleLiquid : MonoBehaviour
{
    [Tooltip("If in container, leave empty. Otherwise, add liquid you want the stream to use.")]
    public Liquid liquid;
    ParticleSystem ps = null;
    List<ParticleCollisionEvent> collisionEvents;
    GameObject parent = null;
    SoundPlayer soundPlayer = null;
    [SerializeField]
    int soundToPlay = 0;
    ParticleSystem.MainModule main;
    ParticleFireOff[] particleFireOffs = null;

    void Awake()
    {
        particleFireOffs = GetComponentsInChildren<ParticleFireOff>();
        ps = GetComponent<ParticleSystem>();
        main = ps.main;
        collisionEvents = new List<ParticleCollisionEvent>();

        foreach (Transform t in GetComponentsInParent<Transform>())
        {
            if (t.tag == "Container")
            {
                parent = t.gameObject;
                break;
            }
        }

        if (liquid == null)
            liquid = ScriptableObject.CreateInstance<Liquid>();
        else
            liquid = Instantiate(liquid);


        soundPlayer = GetComponent<SoundPlayer>();
    }

    public void Play()
    {
        if (!ps.isPlaying)
        {
            UpdateColor();
            ps.Play();
            soundPlayer.PlaySoundInstances(soundToPlay);
        }
    }

    public void Stop()
    {
        if (ps.isPlaying)
        {
            ps.Stop();
            soundPlayer.Stop(soundToPlay);
        }
    }

    public void PlayStop(bool b)
    {
        if (b)
            Play();
        else
            Stop();
    }

    public void UpdateColor(Color col, float change)
    {
        liquid.color = col;
        liquid.fireChange = change;

        UpdateColor();
    }

    public void UpdateColor()
    {
        main.startColor = liquid.color;

        foreach (ParticleFireOff p in particleFireOffs)
        {
            p.changeValue = liquid.fireChange;
            p.changeDistance = 2;
        }
    }

    void OnParticleCollision(GameObject other)
    {
        if (other.tag != "Container" || other == parent)
            return;

        Container container = other.GetComponentInChildren<Container>();

        if (!container)
            return;

        ps.GetCollisionEvents(other, collisionEvents);

        foreach (ParticleCollisionEvent pe in collisionEvents)
        {
            container.AddLiquid(liquid);
        }
    }
}
