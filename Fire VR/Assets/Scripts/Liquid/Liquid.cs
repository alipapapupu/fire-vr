﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Liquid", menuName = "FireSystem/Liquid", order = 1)]
public class Liquid : ScriptableObject
{
    [Tooltip("Color of liquid in container and particles.")]
    public Color color = Color.white;
    [Tooltip("How does the liquid affect fire. Negative for putting out, positive for increasing it")]
    public float fireChange = 0;
}