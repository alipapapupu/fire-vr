﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class Container : MonoBehaviour
{
    public float psDistance = 1;
    public float amount = 1;
    public float maxAmount = 1;
    public Transform centerObject = null;
    public Transform liquidObject = null;
    public float liquidSize = 1;
    public float liquidSizeMultiplier = 1;
    public Liquid liquid = null;
    Liquid _liquid = null;
    public float liquidSpeed = 1;
    bool hasLiquid = false;
    Renderer liquidRenderer = null;
    MaterialPropertyBlock liquidProperty = null;
    [SerializeField]
    ParticleLiquid particleLiquid = null;
    [SerializeField]
    ParticleFireOff fireOff = null;
    bool valuesSet = false;

    Vector3 lowestPoint
    {
        get
        {
            Vector3 returner = Vector3.zero;
            if (centerObject)
            {
                if (Mathf.Abs(centerObject.eulerAngles.x + centerObject.eulerAngles.z) > 10)
                    returner = (centerObject.position + psPoint) / 2;
                else
                    returner = centerObject.position;

                returner.y = transform.position.y;
                returner.y = Mathf.Min(returner.y, psPoint.y);
            }
            returner.y -= 0.01f;
            return returner;
        }
    }

    Vector3 psPoint
    {
        get
        {
            Transform p = transform.parent ? transform.parent : transform;
            Vector3 corPos = p.up * maxAmount + transform.position;
            Vector3 point = p.eulerAngles;
            point = ClampRotation(new Vector3(point.z, point.y, point.x));
            point.x *= -1;
            point.y = 0;
            point.Normalize();
            point = corPos + p.rotation * (point * psDistance);
            return point;
        }
    }
    float currentLiquid
    {
        get { return transform.lossyScale.x * transform.lossyScale.y * transform.lossyScale.z; }
    }

    void Reset()
    {
        if (!centerObject)
        {
            centerObject = transform.parent;
        }
    }

    void SetValues()
    {
        if (valuesSet)
            return;

        valuesSet = true;
        hasLiquid = amount > 0;

        if (liquidObject)
            liquidRenderer = liquidObject.GetComponent<Renderer>();

        if (liquid)
            _liquid = Instantiate(liquid);
        else
            _liquid = ScriptableObject.CreateInstance<Liquid>();
    }

    void Start()
    {
        if (!centerObject)
            centerObject = transform.parent;

        if (!Application.isPlaying)
            return;

        SetValues();

        if (liquid)
        {
            if (amount <= 0)
            {
                Debug.LogWarning("The container has liquid selected, but the amount is zero. Set more amount, or take off liquid.", gameObject);
                liquid = null;
            }
            else
            {
                float tempAmount = amount;
                amount = 0;
                AddLiquid(liquid, tempAmount);
            }
        }
        else
        {
            if (amount > 0)
            {
                Debug.LogWarning("The container has amount, but no liquid selected. Select liquid, or set amount to zero.", gameObject);
                amount = 0;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (centerObject)
        {
            transform.eulerAngles = Vector3.up * centerObject.eulerAngles.y;

            if (liquidObject)
            {
                float xScale = Mathf.Abs(ClampRotation(centerObject.localEulerAngles.z)) * liquidSizeMultiplier + liquidSize;
                float zScale = Mathf.Abs(ClampRotation(centerObject.localEulerAngles.x)) * liquidSizeMultiplier + liquidSize;
                liquidObject.localScale = new Vector3(xScale, amount, zScale);
                liquidObject.position = lowestPoint + Vector3.up * amount / 2;
            }
        }

        if (!Application.isPlaying || !centerObject || !hasLiquid)
        {
            return;
        }

        Vector3 currentPSPoint = psPoint;
        Vector3 liquidTop = centerObject.position + Vector3.up * amount;
        Debug.DrawRay(liquidTop, Vector3.back, Color.green);

        if (liquidTop.y > currentPSPoint.y)
        {
            RemoveLiquid();

            particleLiquid.transform.position = currentPSPoint + transform.parent.up * 0.04f;
            particleLiquid.transform.forward = (currentPSPoint - liquidTop).normalized;
        }

        particleLiquid.PlayStop(liquidTop.y > currentPSPoint.y);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        int max = 8;
        Transform p = transform.parent ? transform.parent : transform;
        Vector3 corPos = p.up * maxAmount + transform.position;

        for (int i = 0; i < max; i++)
        {

            Vector3 from = Vector3.forward * psDistance;
            from = p.rotation * Quaternion.Euler(0, 360 / max * i, 0) * from;

            Vector3 to = Vector3.forward * psDistance;
            to = p.rotation * Quaternion.Euler(0, 360 / max * (i + 1), 0) * to;

            Gizmos.DrawLine(corPos + from, corPos + to);
        }

        Gizmos.color = Color.red;

        Gizmos.DrawLine(corPos, psPoint);

        if (liquidObject && centerObject)
        {
            Gizmos.color = Color.cyan - Color.black * 0.5f;

            float xScale = Mathf.Abs(ClampRotation(centerObject.localEulerAngles.z)) * liquidSizeMultiplier + liquidSize;
            float zScale = Mathf.Abs(ClampRotation(centerObject.localEulerAngles.x)) * liquidSizeMultiplier + liquidSize;
            Vector3 scale = new Vector3(xScale, maxAmount, zScale);
            Vector3 pos = lowestPoint + Vector3.up * maxAmount / 2;


            Gizmos.DrawWireCube(pos, scale);
        }
    }

    void OnValidate()
    {
#if UNITY_EDITOR
        if (UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
            return;
#endif

        amount = Mathf.Clamp(amount, 0, maxAmount);
        UpdateColor();
    }

    float ClampRotation(float value)
    {
        value %= 360;

        value = value > 180 ? value - 360 : value;
        value = value < -180 ? value + 360 : value;

        return value;
    }

    Vector3 ClampRotation(Vector3 value)
    {
        value.x = ClampRotation(value.x);
        value.y = ClampRotation(value.y);
        value.z = ClampRotation(value.z);
        return value;
    }

    public void AddLiquid(Liquid l)
    {
        AddLiquid(l, liquidSpeed * Time.deltaTime);
    }

    public void AddLiquid(Liquid l, float amount)
    {
        if (this.amount != maxAmount)
        {
            float tempAmount = this.amount + amount - maxAmount;

            if (tempAmount > 0)
                amount -= tempAmount;

            _liquid.color = (this.amount * _liquid.color + amount * l.color) / (this.amount + amount);
            _liquid.fireChange = (this.amount * _liquid.fireChange + amount * l.fireChange) / (this.amount + amount);

            this.amount += amount;

        }

        if (!hasLiquid)
        {
            hasLiquid = true;
            liquidObject.gameObject.SetActive(true);
        }

        if (fireOff)
        {
            fireOff.changeValue = _liquid.fireChange;
            fireOff.changeDistance = 2;
        }

        UpdateColor();
    }

    void RemoveLiquid()
    {
        RemoveLiquid(liquidSpeed * Time.deltaTime);
    }

    void RemoveLiquid(float amount)
    {
        float tempAmount = this.amount;
        this.amount -= amount;
        if (this.amount <= 0.001f)
        {
            this.amount = 0;
            hasLiquid = false;
            particleLiquid.Stop();
            liquidObject.gameObject.SetActive(false);
        }
    }

    void UpdateColor()
    {
        Color col = Color.black;
        float change = 0;

        SetValues();

        if (_liquid)
        {
            col = _liquid.color;
            change = _liquid.fireChange;
        }
        else if (liquid)
        {
            col = liquid.color;
            change = liquid.fireChange;
        }
        else
        {
            col = Color.black;
            change = 0;
        }

        col.a = 1;

        if (liquidProperty == null)
            liquidProperty = new MaterialPropertyBlock();

        liquidProperty.SetColor("_BaseColor", col);


        if (liquidRenderer)
            liquidRenderer.SetPropertyBlock(liquidProperty);

        if (Application.isPlaying)
        {
            particleLiquid.UpdateColor(col, change);
        }
    }
}
