﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Liquifier : MonoBehaviour
{
    bool on = false;
    List<GameObject> objects = new List<GameObject>();
    [SerializeField]
    List<GameObject> ignoreObjects = new List<GameObject>();
    Container c = null;
    bool onPlace = false;

    void Start()
    {
        c = GetComponentInChildren<Container>();
    }


    void Update()
    {
        if (!on || !onPlace || objects.Count == 0)
            return;

        Liquid l = ScriptableObject.CreateInstance<Liquid>();
        l.fireChange = 0;

        int i = 0;

        foreach (GameObject g in objects)
        {
            foreach (Renderer r in g.GetComponentsInChildren<Renderer>())
            {
                foreach (Material m in r.materials)
                {
                    if (m.HasProperty("_Color"))
                        l.color += m.GetColor("_Color");
                    else if (m.HasProperty("_BaseColor"))
                        l.color += m.GetColor("_BaseColor");
                    else
                        i--;

                    Debug.Log(l.color);

                    i++;
                }
            }
        }

        if (i == 0)
            return;

        l.color /= i;
        l.color.a = 1;

        c.AddLiquid(l, i * 0.1f);


        for (int o = 0; o < objects.Count;)
        {
            Destroy(objects[o]);
            objects.RemoveAt(o);
        }
    }

    public void TurnOnOff(bool on)
    {
        this.on = on;
    }

    public void OnPlace(bool onPlace)
    {
        this.onPlace = onPlace;
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerEnter(Collider other)
    {
        if (!ignoreObjects.Contains(other.gameObject) && !objects.Contains(other.gameObject))
            objects.Add(other.gameObject);
    }

    /// <summary>
    /// OnTriggerExit is called when the Collider other has stopped touching the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerExit(Collider other)
    {
        if (objects.Contains(other.gameObject))
            objects.Remove(other.gameObject);
    }
}
