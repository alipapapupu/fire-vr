﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialController : MonoBehaviour
{
    [SerializeField]
    public List<Material> materials;
    
    [SerializeField]
    private new Renderer renderer;

    private void Awake()
    {
        if (renderer == null)
        {
            renderer = GetComponentInChildren<Renderer>();
        }
    }

    public void SwapMaterial(int at, int index)
    {
        Material[] rendererMaterials = renderer.materials;

        rendererMaterials[at] = materials[index];

        renderer.materials = rendererMaterials;
    }
}
