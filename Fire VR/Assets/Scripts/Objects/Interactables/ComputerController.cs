﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerController : MonoBehaviour
{
    [SerializeField]
    private MaterialController screenMaterialController;

    private bool powerOn;

    private int currentMaterial;
    private int materialCount;

    private void Start()
    {
        powerOn = false;
        screenMaterialController.SwapMaterial(1, 0);
        materialCount = screenMaterialController.materials.Count;
    }

    public void TogglePower()
    {
        powerOn = !powerOn;
        currentMaterial = powerOn ? 1 : 0;
        screenMaterialController.SwapMaterial(1, currentMaterial);
    }

    public void ToggleTextures()
    {
        if (powerOn)
        {
            currentMaterial++;
            currentMaterial = currentMaterial == materialCount ? 1 : currentMaterial;
            screenMaterialController.SwapMaterial(1, currentMaterial);

            Debug.Log(currentMaterial);
        }
    }
}
