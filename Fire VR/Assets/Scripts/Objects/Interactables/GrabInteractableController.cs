﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(XRGrabInteractable))]
public class GrabInteractableController : MonoBehaviour
{
    protected XRGrabInteractable interactable;

    protected XRBaseInteractor selectingInteractor;

    private void Start()
    {
        interactable = GetComponent<XRGrabInteractable>();
        interactable.onSelectEntered.AddListener(UpdateSelectingInteractor);
    }

    private void UpdateSelectingInteractor(XRBaseInteractor interactor)
    {
        selectingInteractor = interactor;
    }
}
