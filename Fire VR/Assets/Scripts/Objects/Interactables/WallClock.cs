﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallClock : MonoBehaviour
{
    [SerializeField]
    private GameObject minuteHand;
    private ObjectRotator minuteRotator;

    [SerializeField]
    private GameObject hourHand;
    private ObjectRotator hourRotator;



    private void Awake()
    {
        minuteRotator = minuteHand.GetComponent<ObjectRotator>();
        hourRotator = hourHand.GetComponent<ObjectRotator>();
    }
}
