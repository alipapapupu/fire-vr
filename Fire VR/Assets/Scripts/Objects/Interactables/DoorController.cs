﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

public class DoorController : MonoBehaviour
{
    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }
    [Header("Door")]
    [SerializeField]
    private GameObject door = null;

    [SerializeField]
    private Vector3 doorOpenRotation = new Vector3(0, 0, 0);

    [SerializeField]
    private Vector3 doorClosedRotation = new Vector3(0, 0, 0);

    [SerializeField]
    private float doorRotationTime = 0.5f;

    public bool doorOpen;

    [Header("Handle")]
    [SerializeField]
    private GameObject doorHandle = null;

    [SerializeField]
    private Vector3 handleOpenRotation = new Vector3(0, 0, 0);

    [SerializeField]
    private Vector3 handleClosedRotation = new Vector3(0, 0, 0);

    [SerializeField]
    private float handleRotationTime = 0.5f;

    private ObjectRotator objectRotator;

    private bool enableInteraction;
    [SerializeField]
    BoolEvent toggleDoor = null;
    [SerializeField]
    bool locked = false;
    MohaviCreative.Sounds.SoundPlayer soundPlayer = null;

    private void Start()
    {
        doorOpen = door.transform.eulerAngles == doorOpenRotation;

        objectRotator = gameObject.AddComponent<ObjectRotator>();

        enableInteraction = true;

        soundPlayer = GetComponentInChildren<MohaviCreative.Sounds.SoundPlayer>();
    }

    public void ToggleDoorOpen()
    {
        if (enableInteraction)
        {
            if (locked)
            {
                soundPlayer.PlaySoundInstances(0);
                return;
            }
            StartCoroutine(DoorToggling());
        }
    }

    private IEnumerator DoorToggling()
    {
        enableInteraction = false;

        Vector3 handleTargetRot = doorOpen ? handleClosedRotation : handleOpenRotation;
        Vector3 doorTargetRot = doorOpen ? doorClosedRotation : doorOpenRotation;

        if (doorHandle != null)
        {
            if (!doorOpen)
            {
                objectRotator.StartRotation(doorHandle.transform, handleTargetRot, handleRotationTime);

                yield return new WaitForSeconds(handleRotationTime);

                objectRotator.StartRotation(door.transform, doorTargetRot, doorRotationTime);
            }
            else
            {
                objectRotator.StartRotation(door.transform, doorTargetRot, doorRotationTime);

                yield return new WaitForSeconds(doorRotationTime);

                objectRotator.StartRotation(doorHandle.transform, handleTargetRot, handleRotationTime);
            }
        }
        else
        {
            objectRotator.StartRotation(door.transform, doorTargetRot, doorRotationTime);
        }

        doorOpen = !doorOpen;

        toggleDoor?.Invoke(doorOpen);

        enableInteraction = true;
    }

    public void StopDoor()
    {
        StopAllCoroutines();
    }
}
