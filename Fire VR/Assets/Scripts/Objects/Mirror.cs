﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteAlways]
public class Mirror : MonoBehaviour
{
    [SerializeField]
    private List<Transform> trackedObjects = null;
    [SerializeField]
    private List<Transform> mirroredObjects = null;

    [SerializeField]
    private Transform mirrorPoint = null;

    [SerializeField]
    private bool executeInEditMode = false;

    [SerializeField]
    private bool drawMirrorGizmo = false;

    [SerializeField]
    private bool executeEveryFrame = false;

    [SerializeField]
    private MirrorPlaneAxis mirrorPlaneAxis = MirrorPlaneAxis.XY;

    public enum MirrorPlaneAxis
    {
        XY,
        XZ,
        YZ
    }

    private void Update()
    {
        if ((Application.isEditor && executeInEditMode) || (!Application.isEditor && executeEveryFrame))
        {
            if (mirroredObjects == null || trackedObjects.Count != mirroredObjects.Count)
            {
                InstantiateMirrorObjects();
            }

            for (int i = 0; i < trackedObjects.Count; i++)
            {
                Transform mirroredObject = trackedObjects[i];

                MirrorObject(mirroredObject, mirroredObjects[i]);
            }
        }
    }

    public void MirrorObject(Transform trackedObject, Transform mirroredObject)
    {
        Vector3 inNormal = new Vector3(0, 0, 0);

        switch (mirrorPlaneAxis)
        {
            case MirrorPlaneAxis.XY:
                inNormal = mirrorPoint.forward;
                break;

            case MirrorPlaneAxis.XZ:
                inNormal = mirrorPoint.up;
                break;

            case MirrorPlaneAxis.YZ:
                inNormal = mirrorPoint.right;
                break;
        }

        Plane plane = new Plane(inNormal, mirrorPoint.position);

        Vector3 trackedPos = trackedObject.position;
        Quaternion trackedRot = trackedObject.rotation;

        Vector3 closestPointOnPlane = plane.ClosestPointOnPlane(trackedPos);

        mirroredObject.position = Vector3.LerpUnclamped(trackedPos, closestPointOnPlane, 2.0f);

        mirroredObject.rotation = Quaternion.LookRotation(Vector3.Reflect(trackedRot * Vector3.forward, plane.normal), Vector3.Reflect(trackedRot * Vector3.up, plane.normal));
    }

    public void InstantiateMirrorObjects()
    {
        if (mirroredObjects != null && mirroredObjects.Count > 0)
        {
            foreach (Transform trans in mirroredObjects)
            {
                Destroy(trans.gameObject);
            }
        }

        mirroredObjects = new List<Transform>();

        foreach (Transform trans in trackedObjects)
        {
            mirroredObjects.Add(Instantiate(trans.gameObject, transform).transform);
        }
    }

    private void OnDrawGizmos()
    {
        if (drawMirrorGizmo)
        {
            Vector3 cubeSize = new Vector3(1, 1, 1);

            switch (mirrorPlaneAxis)
            {
                case MirrorPlaneAxis.XY:
                    cubeSize.z = 0.001f;
                    break;
                case MirrorPlaneAxis.XZ:
                    cubeSize.y = 0.001f;
                    break;
                case MirrorPlaneAxis.YZ:
                    cubeSize.x = 0.001f;
                    break;
            }

            Gizmos.color = new Color32(40, 80, 255, 50);

            Gizmos.matrix = mirrorPoint.localToWorldMatrix;

            Gizmos.DrawCube(Vector3.zero, cubeSize);

            Gizmos.color = Color.gray;
        }
    }
}