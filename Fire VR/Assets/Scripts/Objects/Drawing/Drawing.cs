﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawing : MonoBehaviour
{
    [SerializeField]
    Vector3 _centerPosition = Vector3.zero;
    Vector3 centerPosition
    {
        get { return transform.TransformPoint(_centerPosition); }
    }

    [SerializeField]
    Vector3 _direction = Vector3.forward;
    Vector3 direction
    {
        get { return transform.rotation * _direction.normalized; }
    }

    [SerializeField]
    Color pencilColor = Color.white;
    [SerializeField]
    float drawDistance = 1;
    [SerializeField]
    int size = 1;

    Vector3 lastPosition = Vector3.zero;
    [SerializeField]
    int steps = 10;

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        CheckMany();
    }

    void CheckMany()
    {
        float distance = Vector3.Distance(lastPosition, centerPosition) * steps;

        for (int i = 0; i < distance; i++)
        {
            CheckOne(Vector3.Lerp(lastPosition, centerPosition, i / distance));
        }

        lastPosition = centerPosition;
    }

    void CheckOne(Vector3 pos)
    {
        if (Physics.Raycast(pos, direction, out RaycastHit hit, drawDistance, LayerMask.GetMask("Draw")))
        {
            Drawable drawable = hit.transform.GetComponentInChildren<Drawable>();

            if (drawable)
            {
                drawable.Draw(hit.textureCoord, pencilColor, size);
            }
        }
    }

    /// <summary>
    /// Callback to draw gizmos only if the object is selected.
    /// </summary>
    void OnDrawGizmosSelected()
    {
        Gizmos.color = pencilColor;

        Gizmos.DrawRay(centerPosition, direction * drawDistance);
    }
}
