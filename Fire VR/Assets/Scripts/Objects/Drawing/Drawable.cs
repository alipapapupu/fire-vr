﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawable : MonoBehaviour
{
    Renderer rend = null;
    Texture2D texture = null;
    [SerializeField]
    Vector2Int size = Vector2Int.one;
    [SerializeField]
    int materialNumber = 0;
    [SerializeField]
    GameObject grabbablePaper = null;
    Vector3 firstPos = Vector3.zero;
    Transform parent = null;
    bool grabbed = false;
    Rigidbody rb = null;
    [SerializeField]
    new Collider collider = null;
    [SerializeField]
    float stickyArea = 1;
    [SerializeField]
    LayerMask mask = 0;
    static GameObject emptyPrefab = null;
    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();

        if (!rend)
        {
            Debug.LogWarning("The object has no renderer. Please put the script in object with renderer.");
            return;
        }

        texture = rend.materials[materialNumber].mainTexture as Texture2D;

        EmptyTexture();


        firstPos = transform.parent.localPosition;
        parent = transform.parent.parent;

        rb = GetComponentInParent<Rigidbody>();

        if (rb)
        {
            rb.isKinematic = true;
            rb.useGravity = true;
        }

        if (collider)
            collider.enabled = true;

        if (grabbablePaper && !emptyPrefab)
        {
            emptyPrefab = Instantiate(grabbablePaper);
            emptyPrefab.SetActive(false);
        }
    }

    public void Draw(Vector2 posi, Color c, int size)
    {
        posi.x *= texture.width;
        posi.y *= texture.height;
        Vector2Int pos = Vector2Int.RoundToInt(posi);

        DrawCircle(texture, c, pos.x, pos.y, size);
    }

    public void DrawCircle(Texture2D t, Color color, int x, int y, int radius)
    {
        float yMultiply = 1;
        float xMultiply = 1;

        if (t.width > t.height)
        {
            yMultiply = (float)t.height / (float)t.width;
        }
        else
        {
            xMultiply = (float)t.width / (float)t.height;
        }

        int xRadius = Mathf.RoundToInt(radius * xMultiply);
        int yRadius = Mathf.RoundToInt(radius * yMultiply);

        Vector2Int point = new Vector2Int(x - xRadius, y - yRadius);

        Vector2Int xClamp = new Vector2Int(Mathf.Clamp(point.x, 0, t.width - 1), Mathf.Clamp(xRadius * 2, 0, t.width - point.x - 1));
        Vector2Int yClamp = new Vector2Int(Mathf.Clamp(point.y, 0, t.height - 1), Mathf.Clamp(yRadius * 2, 0, t.height - point.y - 1));

        Color[] colors = t.GetPixels(xClamp.x, yClamp.x, xClamp.y, yClamp.y);

        float rSquared = xRadius * yRadius;
        Vector2 position = Vector2.zero;

        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = color;
        }

        for (int u = -xRadius; u <= xRadius; u++)
        {
            for (int v = -yRadius; v <= yRadius; v++)
            {
                /*
            if ((u * u) + (v * v) <= rSquared)
            {
                Vector2 newPos = new Vector2((u + xRadius), (v + yRadius));

                if (position != Vector2.zero)
                    Debug.DrawLine(position, newPos, Color.green, 10000);

                position = newPos;

                Debug.Log((u + xRadius) * xRadius + (v + yRadius) + ", " + colors.Length);

                Debug.Log(v + "," + yRadius);
                Debug.Log((u + xRadius) + "," + colora.GetLength(0) + "\n" +
                (v + yRadius) + "," + colora.GetLength(1));

                colora[u + xRadius, v + yRadius] = color;

                colors[(u + xRadius) + (v + yRadius) * xRadius * 2] = color;
            }*/
            }
        }

        t.SetPixels(xClamp.x, yClamp.x, xClamp.y, yClamp.y, colors);

        t.Apply();
    }

    public void EmptyTexture()
    {
        texture = new Texture2D(size.x, size.y, TextureFormat.RGBA32, false);

        texture.Apply();

        rend.materials[materialNumber].mainTexture = texture;
    }

    public void NewPaper()
    {
        collider.enabled = false;
        if (grabbed)
            return;

        grabbed = true;

        GameObject gam = Instantiate(emptyPrefab);

        gam.transform.parent = parent;
        gam.transform.localPosition = firstPos;
        gam.transform.localEulerAngles = Vector3.zero;
        gam.name = gam.name.Replace("(Clone)", "");

        gam.SetActive(true);
    }

    public void Lock()
    {
        collider.enabled = true;
        transform.parent.position += transform.up * 0.02f;

        Transform newParent = null;

        Collider[] colliders = Physics.OverlapSphere(transform.position, stickyArea, mask);

        if (colliders != null && colliders.Length > 0)
        {
            float distance = float.PositiveInfinity;

            foreach (Collider col in colliders)
            {
                if (col.gameObject == gameObject || col.gameObject == collider.gameObject)
                    continue;

                float tempDistance = Vector3.Distance(col.ClosestPoint(transform.position), transform.position);

                if (tempDistance < distance)
                {
                    distance = tempDistance;
                    newParent = col.transform;
                }
            }
            rb.isKinematic = true;
        }

        if (newParent == null)
        {
            rb.isKinematic = false;
        }

        Debug.Log(newParent);

        transform.parent.parent = newParent;
    }

    /// <summary>
    /// Callback to draw gizmos only if the object is selected.
    /// </summary>
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawWireSphere(transform.position, stickyArea);
    }
}
