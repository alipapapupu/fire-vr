﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(XRGrabInteractable))]
public class AttachPointController : MonoBehaviour
{
    [SerializeField, Tooltip("Children of this GameObject will be used as attach points for interactors")]
    private GameObject attachPointParent = null;
    private AttachPoint[] attachPoints;

    [SerializeField]
    public Transform attachPoint = null;
    private bool settingAttachPoint = false;

    private void Awake()
    {
        attachPoints = new AttachPoint[attachPointParent.transform.childCount];

        for (int i = 0; i < attachPoints.Length; i++)
        {
            attachPoints[i] = attachPointParent.transform.GetChild(i).GetComponent<AttachPoint>();
        }
    }

    // <summary> Sets the attach point GameObjects transform to the attach point nearest to the interactor </summary>
    public void SetAttachPointPosition(XRBaseInteractor interactor)
    {
        if (!settingAttachPoint)
        {
            settingAttachPoint = true;
            Vector3 interactorPos = interactor.transform.position;

            AttachPoint nearestAttachPoint;

            if (attachPoints != null)
            {
                nearestAttachPoint = attachPoints
                    .OrderBy(t => Vector3.Distance(t.transform.position, interactorPos))
                    .FirstOrDefault();

                bool leftHand = interactor.CompareTag("LeftHand");

                Transform nearestTransform = leftHand ? nearestAttachPoint.LeftHandedPoint : nearestAttachPoint.RightHandedPoint;

                Vector3 nearestPos = nearestTransform.position;
                Quaternion nearestRot = nearestTransform.rotation;

                if (attachPoint)
                {
                    attachPoint.position = nearestPos;
                    attachPoint.rotation = nearestRot;
                }
            }
            settingAttachPoint = false;
        }
    }
}