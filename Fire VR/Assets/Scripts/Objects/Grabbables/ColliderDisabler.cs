﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderDisabler : MonoBehaviour
{
    private Collider[] colliders;

    private MonoBehaviourTimer disableTimer;
    [SerializeField]
    private float disableTime = 0.5f;

    private void Awake()
    {
        colliders = GetComponentsInChildren<Collider>();

        disableTimer = gameObject.AddComponent<MonoBehaviourTimer>();
        disableTimer.duration = disableTime;
        disableTimer.OnTimerFinished += EnableColliders;
    }

    public void DisableCollidersForAPeriod()
    {
        disableTimer.StartTimer();
        ToggleColliders(false);
    }

    public void EnableColliders()
    {
        ToggleColliders(true);
    }

    public void ToggleColliders(bool enabled)
    {
        foreach (Collider collider in colliders)
        {
            collider.enabled = enabled;
        }
    }

    private void OnDestroy()
    {
        disableTimer.OnTimerFinished -= EnableColliders;
    }
}
