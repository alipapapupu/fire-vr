﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachPoint : MonoBehaviour
{
    [SerializeField]
    private Transform rightHandedPoint = null;
    public Transform RightHandedPoint { get { return rightHandedPoint; } }

    [SerializeField]
    private Transform leftHandedPoint = null;
    public Transform LeftHandedPoint { get { return leftHandedPoint; } }
}
