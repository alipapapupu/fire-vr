﻿using EPOOutline;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineController : MonoBehaviour
{

    private Outlinable outlinable;

    private void Awake()
    {
        outlinable = GetComponentInChildren<Outlinable>();
        outlinable.AddAllChildRenderersToRenderingList();
    }
}
