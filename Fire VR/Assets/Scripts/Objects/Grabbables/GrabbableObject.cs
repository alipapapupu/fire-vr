﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

// <summary> Describes the properties of the grabbable object to the interactor processing it </summary>
public class GrabbableObject : MonoBehaviour
{
    [SerializeField]
    private bool isUIObject = false;
    public bool IsUIObject { get { return isUIObject; } }

    [SerializeField]
    private AnimationClip grabAnimation = null;
    public AnimationClip GrabAnimation { get { return grabAnimation; } }

    private Collider[] colliders;

    [SerializeField]
    private int defaultLayer = 8;

    [SerializeField]
    private int grabbedLayer = 11;

    private void Start()
    {
        colliders = gameObject.GetComponentsInChildren<Collider>();
    }

    public void SwapLayer(bool grabbed)
    {
        foreach (Collider collider in colliders)
        {
            collider.gameObject.layer = grabbed ? grabbedLayer : defaultLayer;
        }
    }
}