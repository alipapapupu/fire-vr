﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
class BoolEvent : UnityEngine.Events.UnityEvent<bool>{

} 

public class PositionLocker : MonoBehaviour
{
    [SerializeField]
    List<GameObject> lockerObjects = null;
    [SerializeField, Tooltip("Should the object be locker right away. Keep false if this object and lockable object arent positioned correctly.")]
    bool autoLock = false;
    [SerializeField, Tooltip("Num of the object in the list to lock to, 0 being first. ")]
    int autoLockObjectNum = 0;
    [SerializeField, Tooltip("Invoked when locked and unlocked. When locked, gives true. When unlocked, gives false.")]
    BoolEvent onLockUnlock = new BoolEvent();
    Vector3 originalPosition = Vector3.zero;
    GameObject currentObject = null;
    Quaternion originalRotation = Quaternion.identity;
    Rigidbody rb = null;
    // Start is called before the first frame update
    void Start()
    {
        originalPosition = transform.localPosition;
        originalRotation = transform.localRotation;
        rb = GetComponent<Rigidbody>();

        if (autoLock)
        {
            currentObject = lockerObjects[autoLockObjectNum];
            LockObject();
        }
    }

    public void UnLockObject()
    {
        currentObject = null;
        onLockUnlock.Invoke(false);
    }

    public void LockObject()
    {
        if (!currentObject)
        {
            rb.isKinematic = false;
            return;
        }

        transform.parent = currentObject.transform;
        transform.localPosition = originalPosition;
        transform.localRotation = originalRotation;
        rb.isKinematic = true;

        onLockUnlock.Invoke(true);
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerEnter(Collider other)
    {
        if (lockerObjects.Contains(other.gameObject))
        {
            currentObject = other.gameObject;
        }
    }

    /// <summary>
    /// OnTriggerExit is called when the Collider other has stopped touching the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerExit(Collider other)
    {
        if (currentObject == other.gameObject)
        {
            currentObject = null;
        }
    }
}
