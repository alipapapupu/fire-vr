﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.Animations;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField]
    int maxHealth = 1;
    [SerializeField]
    float health = 0;
    [SerializeField]
    Color hurtColor = Color.black;
    [SerializeField]
    [Range(0, 1)]
    float maxRingSize = 0;
    [SerializeField]
    AnimationCurve colorChange = AnimationCurve.Linear(0, 0, 1, 1);
    [SerializeField]
    AnimationCurve ringChange = AnimationCurve.Linear(0, 0, 1, 1);
    [SerializeField]
    Volume hurtEffect = null;
    [SerializeField]
    new Transform camera = null;
    [SerializeField]
    UnityEvent onDamaged = null;

    [SerializeField]
    UnityEvent onDeath = null;
    bool died = false;
    Vignette _hurt = null;
    Timer newSoundEffect = new Timer(0.5f);
    Vignette hurt
    {
        get
        {
            if (_hurt)
                return _hurt;

            if (!hurtEffect || !hurtEffect.profile)
                return null;

            hurtEffect.profile.TryGet<Vignette>(out _hurt);

            return _hurt;
        }
    }

    float alreadyHurt = 0;
    public static PlayerHealth instance = null;

    void Awake()
    {
        if (instance)
        {
            Debug.LogWarning("Watch out, there are 2 player healths in game.");
        }

        instance = this;
    }

    void Start()
    {
        ChangeEffect();
    }

    void Update()
    {
        transform.position = camera.position;
        newSoundEffect.NextBool();
    }

    void LateUpdate()
    {
        alreadyHurt = 0;
    }

    public void TakeDamage(float amount, Defeat d)
    {
        if (amount > alreadyHurt)
        {
            float temp = alreadyHurt;
            alreadyHurt = amount;
            amount -= temp;
        }

        health = Mathf.Clamp(health - amount, 0, maxHealth);

        ChangeEffect();

        if (health <= 0 && !died)
        {
            died = true;
            onDeath?.Invoke();
            LevelManager.LoseS(d);
        }
        else if (!died && newSoundEffect.GetBool(true))
        {
            newSoundEffect.Restart();
            onDamaged?.Invoke();
        }
    }

    void ChangeEffect()
    {
        if (!hurt)
            return;

        float change = 1 - (health / maxHealth);

        float colorValue = colorChange.Evaluate(change);
        float ringValue = ringChange.Evaluate(change) * maxRingSize;

        hurt.color.value = Color.Lerp(Color.white, hurtColor, colorValue);
        hurt.intensity.value = ringValue;
    }
    void OnValidate()
    {
        health = Mathf.Clamp(health, 0, maxHealth);

        ChangeEffect();
    }
    public bool FullHealth()
    {
        return health == maxHealth;
    }
}
