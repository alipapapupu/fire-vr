﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.XR.Interaction.Toolkit;

public class AnimatorController : MonoBehaviour
{
    [SerializeField]
    private Animator animator = null;

    [SerializeField]
    private AnimationClip defaultState = null;

    private RuntimeAnimatorController animatorController;
    private AnimationClip[] animationClips;

    private void Start()
    {
        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }

        if (animator != null)
        {
            animatorController = animator.runtimeAnimatorController;
            animationClips = animatorController.animationClips;
        }
    }

    public void StartGrabAnimation(XRGrabInteractable xRGrabInteractable)
    {
        GrabbableObject grabbableObject = xRGrabInteractable.GetComponent<GrabbableObject>();

        if (grabbableObject != null)
        {
            StartTransitionTo(grabbableObject.GrabAnimation);
        }
    }

    public void StartIdleAnimation()
    {
        StartTransitionTo(defaultState);
    }

    public void  StartTransitionTo(AnimationClip requestedClip)
    {
        if (requestedClip != null && animationClips != null)
        {
            IEnumerable<AnimationClip> animationClipQuery =
            from clip in animationClips
            where clip.name == requestedClip.name
            select clip;

            List<AnimationClip> clips = animationClipQuery.ToList();

            if (clips.Count > 0)
            {
                AnimationClip playedClip = clips[0];
                animator.SetTrigger(playedClip.name);
            }
        }
    }
}
