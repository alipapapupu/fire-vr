﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class InteractorController : MonoBehaviour
{
    [SerializeField]
    private AnimatorController animatorController;

    [Header("Interactors in this hand")]
    [SerializeField]
    private Transform attachTransform = null;

    [SerializeField]
    private XRRayInteractor rayInteractor = null;

    [SerializeField]
    private XRDirectInteractor sphereInteractor = null;

    [Header("Interactors in other hand")]
    [SerializeField]
    private XRInteractorLineVisual UIInteractorVisual = null;
    [SerializeField]
    private XRRayInteractor otherHandRayInteractor = null;
    [SerializeField]
    private XRDirectInteractor otherHandSphereInteractor = null;
    [HideInInspector]

    public XRGrabInteractable processedInteractable;

    private XRBaseInteractor selectingInteractor;
    private XRBaseInteractor nonSelectingInteractor;

    private void Start()
    {
        if (animatorController == null)
        {
            animatorController = GetComponent<AnimatorController>();
        }
    }

    public void StartInteractableGrab()
    {
        XRBaseInteractable interactable = rayInteractor.selectTarget;

        if (interactable == null)
        {
            interactable = sphereInteractor.selectTarget;
            selectingInteractor = sphereInteractor;
        }
        else
        {
            selectingInteractor = rayInteractor;
        }

        if (processedInteractable == null && interactable is XRGrabInteractable)
        {
            processedInteractable = (XRGrabInteractable)interactable;

            if (selectingInteractor == rayInteractor)
            {
                nonSelectingInteractor = sphereInteractor;
            }
            else
            {
                nonSelectingInteractor = rayInteractor;
            }

            GrabbableObject grabbableObject = interactable.gameObject.GetComponent<GrabbableObject>();
            if (grabbableObject != null && UIInteractorVisual != null && grabbableObject.IsUIObject)
            {
                UIInteractorVisual.enabled = true;
                otherHandRayInteractor.allowSelect = false;
                otherHandSphereInteractor.allowSelect = false;
            }

            animatorController.StartGrabAnimation(grabbableObject.GetComponent<XRGrabInteractable>());

            nonSelectingInteractor.enableInteractions = false;
            nonSelectingInteractor.allowSelect = false;

            //grabbableObject.GetComponent<AttachPointController>().SetInteractorAttachPoint(selectingInteractor, attachTransform);
        }
    }

    public void StopInteractableGrab()
    {
        if (processedInteractable != null)
        {
            nonSelectingInteractor.enableInteractions = true;
            nonSelectingInteractor.allowSelect = true;

            selectingInteractor = null;
            nonSelectingInteractor = null;

            processedInteractable = null;

            UIInteractorVisual.enabled = false;

            otherHandRayInteractor.allowSelect = true;
            otherHandSphereInteractor.allowSelect = true;

            animatorController.StartIdleAnimation();
        }
    }
}