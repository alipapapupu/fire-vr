﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class StickMovementProvider : LocomotionProvider
{
    [SerializeField]
    private InputActionReference moveAction = null;

    [SerializeField]
    private float speed = 2.0f;

    private void Update()
    {
        Vector2 inputValue = moveAction.action.ReadValue<Vector2>();

        if (BeginLocomotion())
        {
            var xrRig = system.xrRig;

            if (xrRig != null)
            {
                Vector3 heightAdjustment = xrRig.rig.transform.up * xrRig.cameraInRigSpaceHeight;

                Transform cameraTransform = xrRig.cameraGameObject.transform;

                Vector3 cameraDestination = cameraTransform.position + cameraTransform.forward * inputValue.y * speed + cameraTransform.right * inputValue.x * speed;
                cameraDestination *= Time.deltaTime;

                cameraDestination.y = heightAdjustment.y;

                xrRig.MoveCameraToWorldLocation(cameraDestination);
            }
            EndLocomotion();
        }
    }
}
