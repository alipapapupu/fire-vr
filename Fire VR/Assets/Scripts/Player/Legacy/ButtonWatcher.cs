﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using static InputHelpers;

public class ButtonWatcher
{
    private Button button;

    private List<XRController> inputDevices;

    private bool previousValue;

    public ButtonWatcher(Button button, List<XRController> inputDevices)
    {
        this.button = button;

        this.inputDevices = inputDevices;

        previousValue = false;
    }

    public bool ButtonStateChanged(out bool buttonValue)
    {
        bool stateChanged = false;

        buttonValue = false;

        foreach (XRController controller in inputDevices)
        {
            controller.inputDevice.TryGetFeatureValue(XRInputConverter.ButtonToInputFeatureUsage(button), out buttonValue);

            if (buttonValue != previousValue)
            {
                stateChanged = true;
                previousValue = buttonValue;
                break;
            }
        }

        return stateChanged;
    }
}
