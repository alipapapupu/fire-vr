﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    private GameSettings gameSettings = null;

    [SerializeField]
    private InputActionReference menuActionRef = null;
    private InputAction menuAction = null;

    [SerializeField]
    private GameObject[] objectsDisabledOnUIOpen = null;

    [SerializeField]
    private XRInteractorLineVisual[] uiLineVisuals = null;

    [SerializeField]
    private GameObject leftHandedMovement = null;

    [SerializeField]
    private GameObject rightHandedMovement = null;

    private GameObject activeMovementObj;

    [SerializeField]
    private TeleportationProvider teleportationProvider = null;

    [SerializeField]
    private PlayerMenuController playerMenu = null;

    [SerializeField]
    private List<GameObject> hands = null;

    private bool interactorsEnabled = false;

    private void Awake()
    {
        Application.runInBackground = true;

        ToggleLeftHanded(gameSettings.leftHandedControls.Value);
        ToggleTeleportation(gameSettings.teleportEnabled.Value);
    }

    private void Start()
    {
        menuAction = menuActionRef.ToInputAction();

        gameSettings.leftHandedControls.OnValueChanged += ToggleLeftHanded;
        gameSettings.teleportEnabled.OnValueChanged += ToggleTeleportation;
    }

    private void Update()
    {
        if (menuAction.triggered && playerMenu != null && MenuController.enableUIToggle)
        {
            playerMenu.ToggleUI();
        }
    }

    public void ToggleLeftHanded(bool leftHanded)
    {
        leftHandedMovement.SetActive(leftHanded);
        rightHandedMovement.SetActive(!leftHanded);

        activeMovementObj = leftHanded ? leftHandedMovement : rightHandedMovement;
    }

    public void ToggleTeleportation(bool enabled)
    {
        teleportationProvider.enabled = enabled;
    }

    public void ToggleUIInteractors(bool enabled)
    {
        // Disable movement and grabbing when player UI is enabled and enable UI line visuals
        foreach (XRInteractorLineVisual lineVisual in uiLineVisuals)
        {
            lineVisual.enabled = enabled;
        }

        foreach (GameObject obj in objectsDisabledOnUIOpen)
        {
            obj.SetActive(!enabled);
        }

        foreach (GameObject obj in hands)
        {
            obj.layer = enabled ? 5 : 0;
        }

        interactorsEnabled = enabled;

        ToggleActiveMovementObject(!enabled);
    }

    private void ToggleActiveMovementObject(bool enabled)
    {
        activeMovementObj.GetComponent<ActionBasedContinuousMoveProvider>().enabled = enabled;
        activeMovementObj.GetComponent<ActionBasedSnapTurnProvider>().enabled = enabled;
    }

    private void OnDestroy()
    {
        gameSettings.leftHandedControls.OnValueChanged -= ToggleLeftHanded;
        gameSettings.teleportEnabled.OnValueChanged -= ToggleTeleportation;
    }
}