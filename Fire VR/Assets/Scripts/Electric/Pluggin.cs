﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Pluggin : MonoBehaviour
{
    [SerializeField]
    bool on = true;
    [SerializeField]
    Pluggin[] connectedPlugs = null;
    Animator ar = null;
    bool ready = true;
    ControlledFire fire = null;
    [SerializeField, Tooltip("What should happen when the wire gets electricity.")]
    UnityEvent turnOn = null;
    [SerializeField, Tooltip("What should happen when the wire loses electricity.")]
    UnityEvent turnOff = null;
    bool first = true;

    public static bool allOff
    {
        get
        {
            foreach (Pluggin p in plugs)
            {
                if (p.on)
                    return false;
            }
            return true;
        }
    }

    static List<Pluggin> plugs = new List<Pluggin>();

    void Start()
    {
        PlugUnplug(on);
        ar = GetComponent<Animator>();

        fire = transform.parent.GetComponentInChildren<ControlledFire>();


        first = false;
    }

    void OnEnable()
    {

        if (!plugs.Contains(this))
            plugs.Add(this);
    }

    void OnDisable()
    {
        if (plugs.Contains(this))
            plugs.Remove(this);
    }

    public void Unplug()
    {
        PlugUnplug(false);
    }

    public void Plug()
    {
        PlugUnplug(true);
    }

    public void PlugUnplug(bool b)
    {
        on = b;

        if (b)
            turnOn?.Invoke();
        else
            turnOff?.Invoke();

        if (fire)
            fire.ForceChangeFire(Vector3.zero, (b ? 1 : -1), 0, false);

        foreach (Pluggin p in connectedPlugs)
        {
            if (p == this || p == null)
                continue;

            p.PlugUnplug(on);
        }

        if (allOff && !first)
        {
            LevelManager.WinS(LevelData.WinCondition.UnplugAll);
        }
    }

    public void PlugUnplugAnimation()
    {
        PlugUnplugAnimation(!on);
    }

    public void PlugUnplugAnimation(bool b)
    {
        if (!ready)
            return;

        on = b;
        ready = false;
        ar.SetTrigger(on ? "plug" : "unplug");
    }

    public void Ready(int i)
    {
        if (i == 1)
            PlugUnplug(on);

        if ((i == 1) != on)
            return;

        ready = true;
    }
}
