﻿using Assets.SimpleLocalization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelInfo : MonoBehaviour
{
    [SerializeField]
    private LocalizedTextMeshProUGUI infoTitleKey;
    [SerializeField]
    private LocalizedTextMeshProUGUI infoTextKey;

    [SerializeField]
    private Button startButton;

    [SerializeField]
    private Image image1;

    [SerializeField]
    private LevelData levelData;

    private LevelData.Data level;
    private int levelIndex;

    public void UpdateInfoView(int levelIndex)
    {
        level = levelData.Levels[levelIndex];
        this.levelIndex = levelIndex;

        infoTitleKey.LocalizationKey = level.infoTitleKey;
        infoTextKey.LocalizationKey = level.infoTextKey;

        infoTitleKey.Localize();
        infoTextKey.Localize();

        image1.sprite = level.infoImage;

        startButton.onClick.RemoveAllListeners();
        startButton.onClick.AddListener(LoadLevel);
    }

    public void NextInfoText()
    {
        int nextIndex = levelData.Levels.Length - 1 > levelIndex ? levelIndex + 1 : 0;

        Debug.Log(levelData.Levels.Length);

        UpdateInfoView(nextIndex);
    }

    public void PreviousInfoText()
    {
        int nextIndex = levelIndex > 0 ? levelIndex - 1 : levelData.Levels.Length - 1;

        UpdateInfoView(nextIndex);
    }

    private void LoadLevel()
    {
        levelData.currentLevelIndex = levelIndex;
        SceneManager.LoadScene(level.levelName);
    }
}