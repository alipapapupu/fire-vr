﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteAlways]
[RequireComponent(typeof(MeshFilter), typeof(RectTransform), typeof(BoxCollider))]
public class XRUIObject : MonoBehaviour
{
    [SerializeField]
    private Color32 defaultColor = new Color32(255, 255, 255, 255);

    [SerializeField]
    private Color32 highlightColor = new Color32(255, 255, 255, 255);

    [SerializeField]
    private float colorTransitionTime = 0.2f;

    private MeshFilter meshFilter = null;

    private RectTransform rectTransform = null;

    private BoxCollider boxCollider = null;

    private MeshRenderer meshRenderer = null;

    [SerializeField]
    private bool updateColliderSize = true;

    [SerializeField]
    private bool expandChildren = false;
    private List<GameObject> children;

    void Awake()
    {
        Initialise();
        FindChildren();
        SetChildrenZValue();
        UpdateMesh();
    }

    private void FindChildren()
    {
        children = new List<GameObject>();

        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject child = rectTransform.GetChild(i).gameObject;
            
            if (child != null)
            {
                children.Add(child);
            }
        }
    }

    private void SetChildrenZValue()
    {
        foreach (GameObject child in children)
        {
            RectTransform rectTransform = child.GetComponent<RectTransform>();

            rectTransform.localPosition = new Vector3(rectTransform.localPosition.x, rectTransform.localPosition.y, -0.01f);
        }
    }

    private void UpdateMesh()
    {
        if (meshFilter == null || rectTransform == null || boxCollider == null)
            Initialise();

        Mesh mesh = GetRectangleMesh(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y);
        meshFilter.mesh = mesh;

        if (expandChildren)
        {
            FindChildren();

            foreach (GameObject child in children)
            {
                RectTransform childRectTransform = child.GetComponent<RectTransform>();

                if (childRectTransform != null)
                {
                    childRectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, rectTransform.sizeDelta.y);
                }
            }
        }

        ChangeColor(defaultColor);

        if (updateColliderSize)
            boxCollider.size = rectTransform.sizeDelta;
    }

    private Mesh GetRectangleMesh(float width, float height)
    {
        Mesh mesh = new Mesh();

        Vector3[] vertices =
        {
            new Vector3(width / 2, -height / 2, 0),
            new Vector3(width / 2, height / 2, 0),
            new Vector3(-width / 2, -height / 2, 0),
            new Vector3(-width / 2, height / 2, 0)
        };

        int[] triangles =
        {
            0, 2, 1,
            2, 3, 1
        };

        Vector2[] uvs = new Vector2[vertices.Length];

        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
        }

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;

        return mesh;
    }

    public void HighlightColor(bool smoothColorChange)
    {
        StartColorChange(smoothColorChange, highlightColor);
    }

    public void DefaultColor(bool smoothColorChange)
    {
        StartColorChange(smoothColorChange, defaultColor);
    }

    public void StartColorChange(bool smoothColorChange, Color32 color)
    {
        if (smoothColorChange && gameObject.activeInHierarchy)
        {
            StopAllCoroutines();
            StartCoroutine(SmoothColorChange(color));
        }
        else
        {
            ChangeColor(color);
        }
    }

    private void ChangeColor(Color32 color)
    {
        Mesh mesh = meshFilter.sharedMesh;

        Color32[] colors = new Color32[mesh.vertices.Length];

        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = color;
        }

        mesh.colors32 = colors;
    }

    private IEnumerator SmoothColorChange(Color32 newColor)
    {
        float t = colorTransitionTime;

        Color32 oldColor = meshFilter.mesh.colors32[0];

        float elapsedT = 0;

        do
        {
            Color32 color = Color32.Lerp(oldColor, newColor, elapsedT);

            elapsedT += Time.deltaTime;

            ChangeColor(color);

            yield return new WaitForEndOfFrame();
        }
        while (elapsedT < t);

        Debug.Log("Change complete");

        ChangeColor(newColor);
    }

    private void Initialise()
    {
        meshFilter = GetComponent<MeshFilter>();
        rectTransform = GetComponent<RectTransform>();
        boxCollider = GetComponent<BoxCollider>();

        meshRenderer = GetComponent<MeshRenderer>();
        if (meshRenderer == null)
        {
            gameObject.AddComponent<MeshRenderer>();
        }
    }

    public void OnRectTransformDimensionsChange()
    {
        UpdateMesh();
    }
}
