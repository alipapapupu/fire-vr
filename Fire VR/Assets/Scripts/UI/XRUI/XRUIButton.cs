﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class XRUIButton : XRUIObject
{
    [SerializeField]
    private Color32 pressedColor = new Color32(255, 255, 255, 255);

    [SerializeField]
    public UnityEvent onPress;

    public void Invoke()
    {
        onPress.Invoke();
    }

    public void PressedColor(bool smoothColorChange)
    {
        StartColorChange(smoothColorChange, pressedColor);
    }
}
