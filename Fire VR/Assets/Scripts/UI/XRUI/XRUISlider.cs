﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XRUISlider : XRUIObject
{
    [SerializeField]
    private RectTransform slidingArea;
    [SerializeField]
    private bool matchParentSize;

    [SerializeField]
    private GameObject handle;



    public new void OnRectTransformDimensionsChange()
    {
        base.OnRectTransformDimensionsChange();


    }
}
