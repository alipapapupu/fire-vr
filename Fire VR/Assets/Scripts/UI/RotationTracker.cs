﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTracker : MonoBehaviour
{
    [SerializeField]
    private List<Transform> rotatedTransforms = null;

    [SerializeField]
    private Transform trackedRotation;

    [SerializeField]
    private float rotationTreshold = 45.0f;

    [SerializeField]
    private float rotationSpeed = 1.0f;

    [SerializeField]
    private float updateInterval = 1.0f;
    private float updateTimer = 0.0f;

    [SerializeField, Header("Tracked axes")]
    private bool x = false;

    [SerializeField]
    private bool y = true;

    [SerializeField]
    private bool z = false;

    private void Start()
    {
        updateTimer = 0.0f;
        StartCoroutine(RotateTowardsTarget());

        if (trackedRotation == null)
        {
            trackedRotation = transform;
        }
    }

    private void Update()
    {
        updateTimer += Time.unscaledDeltaTime;

        if (updateTimer > updateInterval && IsRotationAboveThreshold())
        {
            updateTimer = 0.0f;

            StopAllCoroutines();
            StartCoroutine(RotateTowardsTarget());
        }
    }

    private bool IsRotationAboveThreshold()
    {
        Vector3 rot = rotatedTransforms[0].eulerAngles;
        Vector3 targetRot = trackedRotation.eulerAngles;

        float xDiff = Mathf.Abs(Mathf.DeltaAngle(rot.x, targetRot.x));
        float yDiff = Mathf.Abs(Mathf.DeltaAngle(rot.y, targetRot.y));
        float zDiff = Mathf.Abs(Mathf.DeltaAngle(rot.z, targetRot.z));

        return (x && xDiff > rotationTreshold) || (y && yDiff > rotationTreshold) || (z && zDiff > rotationTreshold);
    }

    private IEnumerator RotateTowardsTarget()
    {
        float t = 0;

        Vector3 rotation = rotatedTransforms[0].eulerAngles;
        Vector3 targetRot = trackedRotation.eulerAngles;

        targetRot = new Vector3(x ? targetRot.x : rotation.x, y ? targetRot.y : rotation.y, z ? targetRot.z : rotation.z);

        while (t < 1)
        {
            t += Time.unscaledDeltaTime / rotationSpeed;
            t = t > 1 ? 1 : t;

            Vector3 appliedRot = new Vector3();

            appliedRot.x = Mathf.LerpAngle(rotation.x, targetRot.x, t);
            appliedRot.y = Mathf.LerpAngle(rotation.y, targetRot.y, t);
            appliedRot.z = Mathf.LerpAngle(rotation.z, targetRot.z, t);

            foreach (Transform transform in rotatedTransforms)
            {
                transform.eulerAngles = appliedRot;
            }

            yield return null;
        }
    }
}
