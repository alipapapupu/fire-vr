﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryMenuController : DeathMenuController
{
    [SerializeField]
    TMPro.TextMeshProUGUI[] conditions = null;

    public override void ToggleUI(string s)
    {
        base.ToggleUI(s);

        int i = 0;
        foreach (LevelData.ExtraCondition c in LevelManager.instance.currentData.extraConditions)
        {
            string ss = "* " + GetString(c.ToString(), "ExtraCondition");

            conditions[i].text = ss;
            conditions[i].gameObject.SetActive(true);

            if (LevelManager.instance.CheckExtraCondition(c))
            {
                conditions[i].fontStyle = TMPro.FontStyles.Strikethrough;
            }
            else
            {
                conditions[i].fontStyle = TMPro.FontStyles.Normal;
            }

            i++;
        }

        for (int o = i; o < conditions.Length; o++)
        {
            conditions[o].gameObject.SetActive(false);
        }
    }
}
