﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Clock : MonoBehaviour
{
    private MonoBehaviourTimer timer;
    [SerializeField]
    private float interval = 0.5f;

    public UnityEvent OnTick;

    private void Start()
    {
        timer = gameObject.AddComponent<MonoBehaviourTimer>();
        timer.duration = interval;
        timer.StartTimer();
    }

    private void Update()
    {
        if (timer.isFinished)
        {
            OnTick?.Invoke();
            timer.StartTimer();
        }
    }
}
