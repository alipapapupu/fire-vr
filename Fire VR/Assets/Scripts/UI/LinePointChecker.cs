﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LinePointChecker : MonoBehaviour
{
    LineRenderer lineRenderer = null;

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }
    protected void OnEnable()
    {
        Application.onBeforeRender += CheckLinePoints;
    }

    protected void OnDisable()
    {
        Application.onBeforeRender -= CheckLinePoints;
    }

    [BeforeRenderOrder(UnityEngine.XR.Interaction.Toolkit.XRInteractionUpdateOrder.k_BeforeRenderLineVisual + 1)]
    void CheckLinePoints()
    {
        if (!lineRenderer.enabled)
            return;

        Vector3[] points = new Vector3[lineRenderer.positionCount];
        lineRenderer.GetPositions(points);

        int zeroPoints = 0;

        foreach (Vector3 p in points)
        {
            if (p.magnitude < 0.01f)
            {
                zeroPoints++;
            }
        }

        lineRenderer.enabled = lineRenderer.positionCount - zeroPoints > 1;
    }
}
