﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    private Canvas canvas;

    void Start()
    {
        canvas = GetComponent<Canvas>();
        if (canvas.worldCamera == null)
        {
            canvas.worldCamera = FindObjectOfType<Camera>();
        }
    }
}
