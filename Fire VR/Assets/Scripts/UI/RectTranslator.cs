﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class RectTranslator : MonoBehaviour
{
    private RectTransform rectTrans;

    private void Start()
    {
        if (rectTrans == null)
        {
            rectTrans = GetComponent<RectTransform>();
        }
    }

    public void TranslateX(float value)
    {
        Translate(value, 0, 0);
    }
    public void TranslateY(float value)
    {
        Translate(0, value, 0);
    }
    public void TranslateZ(float value)
    {
        Translate(0, 0, value);
    }

    public void Translate(float x, float y, float z)
    {
        Vector3 translation = new Vector3(x, y, z);
        rectTrans.Translate(translation);
    }
}
