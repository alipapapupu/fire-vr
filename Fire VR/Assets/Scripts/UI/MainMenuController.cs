﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MenuController
{
    private IEnumerator Start()
    {
        yield return null;
        ToggleUI();
    }

    public void LoadDefaultLevel()
    {
        Debug.Log("WORKING");
        LoadScene(levelData.DefaultLevel);
    }
}
