﻿using Assets.SimpleLocalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuController : MenuController
{
    [SerializeField]
    private LocalizedTextMeshProUGUI levelTitle;

    [SerializeField]
    private LocalizedTextMeshProUGUI levelDescription;

    private void Start()
    {
        if (levelData.currentLevelIndex > -1)
        {
            LevelData.Data level = levelData.Levels[levelData.currentLevelIndex];
            levelTitle.LocalizationKey = level.infoTitleKey;

            levelDescription.LocalizationKey = level.infoTextKey;

            levelTitle.Localize();
            levelDescription.Localize();

            if (stopTime)
                Time.timeScale = 0;

            OnUIOpen?.Invoke();
        }
        else
        {
            ToggleUI();
        }
    }
}