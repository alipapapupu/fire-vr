﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateLevelSelectImage : MonoBehaviour
{
    [SerializeField]
    int levelNumber = 0;
    [SerializeField]
    LevelData levelData = null;

    void Awake()
    {
        GetComponent<Image>().sprite = levelData.Levels[levelNumber].infoImage;
    }
}
