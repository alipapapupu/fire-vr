﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.SimpleLocalization;

/// <summary>
/// Localize text component.
/// </summary>
[RequireComponent(typeof(Image))]
public class LogoChanger : MonoBehaviour
{
    [SerializeField]
    LocalizedImage[] images = null;

    public void Start()
    {
        Localize();
        LocalizationManager.LocalizationChanged += Localize;
    }

    public void OnDestroy()
    {
        LocalizationManager.LocalizationChanged -= Localize;
    }

    public void Localize()
    {
        Sprite s = null;
        string name = LocalizationManager.Language;

        foreach (LocalizedImage li in images)
        {
            if (name == li.language)
            {
                s = li.sprite;
            }
        }

        GetComponent<Image>().sprite = s;
    }

    [System.Serializable]
    class LocalizedImage
    {
        public string language = "";
        public Sprite sprite = null;
    }
}
