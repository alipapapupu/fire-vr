﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class DialPadController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI dialText = null;

    private string number;
    [SerializeField]
    UnityEvent call112 = new UnityEvent();

    public void AppendText(string text)
    {
        dialText.text += text;
        number += text;
    }

    public void EraseText()
    {
        dialText.text = "";
        number = "";
    }

    public void CallNumber()
    {
        Debug.Log("Calling " + number);

        if (!LevelManager.instance.callEmergencyNumber)
        {
            if (number == "112" || number == "911")
            {
                Debug.Log("Emergency Number");
                call112.Invoke();
                LevelManager.instance.callEmergencyNumber = true;
            }
        }
    }
}