﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using System.Data;

public class Calculator : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI text;

    private bool showsValue = false;

    public void AppendText(string text)
    {
        if (!showsValue)
        {
            this.text.text += text;
        }
        else
        {
            this.text.text = text;
            showsValue = false;
        }
    }

    public void EraseText()
    {
        text.text = "";
    }

    public void Calculate()
    {
        string value = new DataTable().Compute(text.text, null).ToString();

        text.text = value;

        showsValue = true;
    }
}
