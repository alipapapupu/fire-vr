﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SettingsViewController : MonoBehaviour
{
    [SerializeField]
    private GameSettings gameSettings = null;

    [SerializeField]
    private Slider masterVolume = null;

    [SerializeField]
    private Slider soundVolume = null;

    [SerializeField]
    private Slider musicVolume = null;

    [SerializeField]
    private Toggle leftHandedToggle = null;

    [SerializeField]
    private Toggle rightHandedToggle = null;

    [SerializeField]
    private Toggle teleportToggle = null;

    [SerializeField]
    private Toggle walkToggle = null;

    private void Start()
    {
        InitialiseSlider(masterVolume, gameSettings.masterVolume.Value, ChangeMasterVolume);
        InitialiseSlider(soundVolume, gameSettings.soundVolume.Value, ChangeSoundVolume);
        InitialiseSlider(musicVolume, gameSettings.musicVolume.Value, ChangeMusicVolume);

        InitialiseToggle(leftHandedToggle, gameSettings.leftHandedControls.Value, ToggleLeftHanded);
        InitialiseToggle(teleportToggle, gameSettings.teleportEnabled.Value, ToggleTeleport);

        InitialiseToggle(rightHandedToggle, !gameSettings.leftHandedControls.Value);
        InitialiseToggle(walkToggle, !gameSettings.teleportEnabled.Value);
    }

    private void InitialiseSlider(Slider slider, float initialValue, UnityAction<float> listener = null)
    {
        slider.value = initialValue;

        if (listener != null)
        {
            slider.onValueChanged.AddListener(listener);
        }
    }

    private void InitialiseToggle(Toggle toggle, bool initialValue, UnityAction<bool> listener = null)
    {
        toggle.isOn = initialValue;

        if (listener != null)
        {
            toggle.onValueChanged.AddListener(listener);
        }
    }

    public void ToggleTeleport(bool teleportEnabled)
    {
        gameSettings.teleportEnabled.Value = teleportEnabled;
    }

    public void ToggleLeftHanded(bool leftHanded)
    {
        gameSettings.leftHandedControls.Value = leftHanded;
    }

    public void ChangeMasterVolume(float masterVolume)
    {
        gameSettings.masterVolume.Value = masterVolume;
    }

    public void ChangeSoundVolume(float soundVolume)
    {
        gameSettings.soundVolume.Value = soundVolume;
    }

    public void ChangeMusicVolume(float musicVolume)
    {
        gameSettings.musicVolume.Value = musicVolume;
    }
}
