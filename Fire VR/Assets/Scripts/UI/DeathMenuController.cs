﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.SimpleLocalization;

public class DeathMenuController : MenuController
{
    [SerializeField]
    protected TMPro.TextMeshProUGUI text = null;
    [SerializeField]
    string startAppendix = "";

    public virtual void ToggleUI(string s)
    {
        ToggleUI();
        string sas = GetString(s);
        text.text = sas;
        Debug.Log(sas);
    }

    protected string GetString(string name)
    {
        return GetString(name, startAppendix);
    }

    protected static string GetString(string name, string appendix)
    {
        if (appendix.Length > 0)
            appendix += ".";

        string key = appendix + name;
        if (LocalizationManager.HasKey(key))
        {
            return LocalizationManager.Localize(key);
        }
        else
            return "Translation not found.\n Launch program: skynet.";
    }
}
