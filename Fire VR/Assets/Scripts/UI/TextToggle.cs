﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle), typeof(TextMeshProUGUI))]
public class TextToggle : MonoBehaviour
{
    private Toggle toggle;

    private TextMeshProUGUI text;

    private void Awake()
    {
        GetComponents();
    }

    public void ToggleTextSelectedColor(bool selected)
    {
        GetComponents();
        text.color = selected ? toggle.colors.selectedColor : toggle.colors.normalColor;
    }

    void GetComponents()
    {
        if (!toggle)
        {
            toggle = GetComponent<Toggle>();
            text = GetComponent<TextMeshProUGUI>();
        }
    }
}
