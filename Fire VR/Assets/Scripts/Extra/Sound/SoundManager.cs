﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD.Studio;
using FMODUnity;


namespace MohaviCreative
{
    namespace Sounds
    {
        /// <summary>
        /// Soundmanager. Does everything needed with sound. There should be only one of these in the game.
        /// Play sounds from any script by writing SoundManager.Instance.Play() / any other method you want.
        /// </summary>
        public class SoundManager : Singleton<SoundManager>
        {
            /// <summary>
            /// Background musics.
            /// </summary>
            [Tooltip("Background musics.")]
            [FMODUnity.EventRef]
            public string[] backGroundMusic = null;

            /// <summary>
            /// Background ambients.
            /// </summary>
            [Tooltip("Background ambients.")]
            [FMODUnity.EventRef]
            public string[] backGroundAmbience = null;

            /// <summary>
            /// SoundEffects.
            /// </summary>
            [Tooltip("SoundEffects.")]
            [FMODUnity.EventRef]
            public string[] soundEffects = null;

            /// <summary>
            /// Instance of current backgroundmusic.
            /// </summary>
            [Tooltip("Instance of current backgroundmusic.")]
            [HideInInspector]
            public EventInstance bgMusic;

            /// <summary>
            /// Instance of current backgroundambience.
            /// </summary>
            [Tooltip("Instance of current backgroundambience.")]
            EventInstance bgAmbience;

            /// <summary>
            /// Should the background music at 0 be played right on the start.
            /// </summary>
            [Tooltip("Should the background music be played right on the start.")]
            public bool playBGMusicAtStart = true;

            /// <summary>
            /// Is any backgroundmusic playing right now.
            /// </summary>
            [Tooltip("Is any backgroundmusic playing right now.")]
            [HideInInspector]
            public bool musicPlaying = false;

            /// <summary>
            /// Is any backgroundambience playing right now.
            /// </summary>
            [Tooltip("Is any backgroundambience playing right now.")]
            [HideInInspector]
            public bool ambiencePlaying = false;

            /// <summary>
            /// String of the current backgroundmusic.
            /// </summary>
            [Tooltip("String of the current backgroundmusic.")]
            string currentBGMusic = "";

            /// <summary>
            /// String of the current backgroundambience.
            /// </summary>
            [Tooltip("String of the current backgroundambience.")]
            string currentBGAmbience = "";

            /// <summary>
            /// Should all the changes be saved to playerprefs.
            /// </summary>
            bool save = false;

            #region BaseMethods

            /// <summary>
            /// Reset, is called when component is added. Makes sure there is only 1 soundmanager in scene.
            /// </summary>
            private void Reset()
            {
                SoundManager[] tester = GameObject.FindObjectsOfType<SoundManager>();
                if (tester.Length > 1 || tester[0] != this)
                {
                    Debug.LogWarning("There are more than 1 sound manager! Newly added soundmanager is destroied!");

                    for (int i = 0; i < tester.Length; i++)
                    {
                        Debug.LogWarning("There is soundmanager in this object.", tester[i].gameObject);
                    }

                    Destroy(this);
                }
            }


            /// <summary>
            /// Awake, is called on the first frame a gameobject is created. Creates everything needed to work.
            /// </summary>
            override protected void Awake()
            {
                base.Awake();

                if (!validate)
                    return;

                if (backGroundMusic.Length > 0)
                {
                    bgMusic = Create(gameObject, backGroundMusic[0]);
                    currentBGMusic = backGroundMusic[0];
                }

                if (backGroundAmbience.Length > 0)
                {
                    bgAmbience = Create(gameObject, backGroundAmbience[0]);
                    currentBGAmbience = backGroundAmbience[0];
                }

                if (playBGMusicAtStart)
                {
                    PlayBGMusic();
                }

                LoadVolumes();
                LoadMutes();

                DontDestroyOnLoad(gameObject);
            }

            #endregion

            /// Create methods. Use these to create a looping sound effect or song, so you can stop it when needed.
            #region Creates

            /// <summary>
            /// Creates eventinstance for song or sound effect. Usually used for looping sounds so the loop can be stopped when needed. Doesnt play the sound automatically.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="s">String, name of the soundeffect/song that should be played.</param>
            public EventInstance Create(GameObject go, string s)
            {
                return StaticCreate(go, s, false);
            }


            /// <summary>
            /// Creates eventinstance for sound effect. Usually used for looping sounds so the loop can be stopped when needed. Doesnt play the sound automatically.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="i">Int, number of the soundeffect to be created.</param>
            public EventInstance Create(GameObject go, int i)
            {
                return StaticCreate(go, i, false);
            }

            /// <summary>
            /// Creates eventinstance for song or sound effect. Usually used for looping sounds so the loop can be stopped when needed.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="s">String, name of the soundeffect/song that should be played.</param>
            /// <param name="play">Bool, should the sound be played right away.</param>
            public EventInstance Create(GameObject go, string s, bool play)
            {
                return StaticCreate(go, s, play);
            }


            /// <summary>
            /// Creates eventinstance for sound effect. Usually used for looping sounds so the loop can be stopped when needed.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="i">Int, number of the soundeffect to be created.</param>
            /// <param name="play">Bool, should the sound be played right away.</param>
            public EventInstance Create(GameObject go, int i, bool play)
            {
                return Create(go, soundEffects[i], play);
            }

            /// <summary>
            /// Creates eventinstance for song or sound effect. Usually used for looping sounds so the loop can be stopped when needed. Doesnt play the sound automatically.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="s">String, name of the soundeffect/song that should be played.</param>
            public static EventInstance StaticCreate(GameObject go, string s)
            {
                return StaticCreate(go, s, false);
            }


            /// <summary>
            /// Creates eventinstance for sound effect. Usually used for looping sounds so the loop can be stopped when needed. Doesnt play the sound automatically.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="i">Int, number of the soundeffect to be created.</param>
            public static EventInstance StaticCreate(GameObject go, int i)
            {
                return StaticCreate(go, i, false);
            }

            /// <summary>
            /// Creates eventinstance for song or sound effect. Usually used for looping sounds so the loop can be stopped when needed.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="s">String, name of the soundeffect/song that should be played.</param>
            /// <param name="play">Bool, should the sound be played right away.</param>
            public static EventInstance StaticCreate(GameObject go, string s, bool play)
            {
                if (s == "")
                {
                    Debug.LogWarning("No name for sound!");
                    return new EventInstance();
                }

                EventInstance ei = RuntimeManager.CreateInstance(s);
                RuntimeManager.AttachInstanceToGameObject(ei, go.transform, go.GetComponentInParent<Rigidbody>());

                if (play)
                {
                    StaticPlay(ei);
                }

                return ei;
            }


            /// <summary>
            /// Creates eventinstance for sound effect. Usually used for looping sounds so the loop can be stopped when needed.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="i">Int, number of the soundeffect to be created.</param>
            /// <param name="play">Bool, should the sound be played right away.</param>
            public static EventInstance StaticCreate(GameObject go, int i, bool play)
            {
                return instance.Create(go, i, play);
            }

            #endregion

            /// Play methods. Use these to simply play a sound effect once. Works like SoundManager.Instnace.Play();
            #region Plays

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="ei">Eventinstance that should be played.</param>
            public void Play(GameObject go, EventInstance ei)
            {
                StaticPlay(go, ei);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="r">Transform the sound effect is attached to.</param>
            /// <param name="s">String of the soundeffect that should be played.</param>
            public void Play(Transform r, string s)
            {
                StaticPlay(r, s);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="r">Transform the sound effect is attached to.</param>
            /// <param name="i">Int, number of the soundeffect that should be played.</param>
            public void Play(Transform r, int i)
            {
                RuntimeManager.PlayOneShot(soundEffects[i], r.position);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="i">Int, number of the soundeffect that should be played.</param>
            public void Play(int i)
            {
                if (i < soundEffects.Length)
                    RuntimeManager.PlayOneShot(soundEffects[i], transform.position);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="s">String of the soundeffect that should be played.</param>
            public void Play(GameObject go, string s)
            {
                StaticPlay(go, s);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="s">String of the soundeffect that should be played.</param>
            public void Play(string s)
            {
                Play(gameObject, s);
            }


            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="ei">EventInstace of the soundeffect that should be played.</param>
            public void Play(EventInstance ei)
            {
                StaticPlay(ei);
            }

            #endregion
            /// StaticPlay methods. Use these to simply play a sound effect once. Works like SoundManager.Play();
            #region StaticPlays

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="ei">Eventinstance that should be played.</param>
            public static void StaticPlay(GameObject go, EventInstance ei)
            {
                RuntimeManager.AttachInstanceToGameObject(ei, go.transform, go.GetComponentInParent<Rigidbody>());
                StaticPlay(ei);
            }
            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="ei">Eventinstance that should be played.</param>
            public static void StaticPlay(Rigidbody rb, EventInstance ei)
            {
                StaticPlay(ei);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="ei">Eventinstance that should be played.</param>
            public static void StaticPlay(Transform t, EventInstance ei)
            {
                StaticPlay(t.gameObject, ei);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="r">Transform the sound effect is attached to.</param>
            /// <param name="s">String of the soundeffect that should be played.</param>
            public static void StaticPlay(Transform r, string s)
            {
                RuntimeManager.PlayOneShot(s, r.position);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="s">String of the soundeffect that should be played.</param>
            public static void StaticPlay(GameObject go, string s)
            {
                RuntimeManager.PlayOneShotAttached(s, go);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="ei">EventInstace of the soundeffect that should be played.</param>
            public static void StaticPlay(EventInstance ei)
            {
                ei.start();
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="r">Transform the sound effect is attached to.</param>
            /// <param name="i">Int, number of the soundeffect that should be played.</param>
            public static void StaticPlay(Transform r, int i)
            {
                instance.Play(r, i);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="r">Transform the sound effect is attached to.</param>
            /// <param name="s">String of the soundeffect that should be played.</param>
            public static IEnumerator StaticWaitPlay(Transform r, string s, float delay)
            {
                yield return new WaitForSeconds(delay);
                StaticPlay(r, s);
            }

            /// <summary>
            /// Plays the given song or sound effect.
            /// </summary>
            /// <param name="go">GameObject the sound effect is attached to.</param>
            /// <param name="s">String of the soundeffect that should be played.</param>
            public static IEnumerator StaticWaitPlay(GameObject go, string s, float delay)
            {
                yield return new WaitForSeconds(delay);
                StaticPlay(go, s);
            }

            #endregion

            ///Stop methods. Use these to simply stop a sound effects that are long or looping.
            #region Stops

            /// <summary>
            /// Stops the given song or sound effect.
            /// </summary>
            /// <param name="ei">EventInstace of the soundeffect that should be stopped.</param>
            public void Stop(EventInstance ei)
            {
                StaticStop(ei);
            }

            public static void StaticStop(EventInstance ei)
            {
                ei.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }

            #endregion

            ///Volume methods.!-- 
            #region SetVolumes

            /// <summary>
            /// Base volume changer.
            /// </summary>
            /// <param name="b">Buss to change the volume for.</param>
            /// <param name="f">Float where to change the volume.</param>
            public void SetVolume(Busses b, float f)
            {
                StaticSetVolume(b, f);
            }

            /// <summary>
            /// Base volume changer.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public static void StaticSetVolume(Busses b, float f)
            {
                SetVolume(AllBusses.GetBus(b), f);
            }

            /// <summary>
            /// Base volume changer.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public static void SetVolume(Bus b, float f)
            {
                string name = "";
                b.getPath(out name);
                PlayerPrefs.SetFloat(name, f);
                b.setVolume(f);

                Save();
            }

            /// <summary>
            /// Base volume changer.
            /// </summary>
            /// <param name="b">Buss to change the volume for.</param>
            /// <param name="f">Float where to change the volume.</param>
            public void SetVolume(VCAs v, float f)
            {
                StaticSetVolume(v, f);
            }

            /// <summary>
            /// Base volume changer.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public static void StaticSetVolume(VCAs v, float f)
            {
                SetVolume(AllVCAs.GetVCA(v), f);
            }

            /// <summary>
            /// Base volume changer.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public static void SetVolume(VCA v, float f)
            {
                string name = "";
                v.getPath(out name);
                PlayerPrefs.SetFloat(name, f);
                v.setVolume(f);

                Save();
            }

            #endregion
            ///Volume methods.!-- 
            #region GetVolumes


            /// <summary>
            /// Returns wanted bus volume.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public float GetVolume(Busses b)
            {
                return StaticGetVolume(b);
            }

            /// <summary>
            /// Returns wanted bus volume.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public static float StaticGetVolume(Busses b)
            {
                return GetVolume(AllBusses.GetBus(b));
            }

            /// <summary>
            /// Returns wanted bus volume.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public static float GetVolume(Bus b)
            {
                b.getVolume(out float f);
                return f;
            }

            /// <summary>
            /// Returns wanted bus volume.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public float GetVolume(VCAs v)
            {
                return StaticGetVolume(v);
            }

            /// <summary>
            /// Returns wanted bus volume.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public static float StaticGetVolume(VCAs v)
            {
                return GetVolume(AllVCAs.GetVCA(v));
            }

            /// <summary>
            /// Returns wanted bus volume.
            /// </summary>
            /// <param name="f">Float where to change the volume.</param>
            public static float GetVolume(VCA v)
            {
                v.getVolume(out float f);
                return f;
            }
            #endregion

            public static void LoadVolumes()
            {
                foreach (string s in AllBusses.busPaths)
                {
                    if (s == null || s.Length == 0)
                        continue;

                    Bus b = GetBus(s);

                    float value = PlayerPrefs.GetFloat(s, 1);

                    SetVolume(b, value);
                }

                foreach (string s in AllVCAs.VCAPaths)
                {
                    if (s == null || s.Length == 0)
                        continue;

                    VCA v = GetVCA(s);

                    float value = PlayerPrefs.GetFloat(s, 1);

                    SetVolume(v, value);
                }
            }

            #region BackgroundMusic

            /// <summary>
            /// Plays the background music if its paused.
            /// </summary>
            public void PlayBGMusic()
            {
                if (musicPlaying)
                    return;
                musicPlaying = true;

                Play(bgMusic);
            }

            /// <summary>
            /// Changes the background music. Plays right away.
            /// </summary>
            /// <param name="i">Int, number of the new song to play.</param>
            public void ChangeBGMusic(int i)
            {
                ChangeBGMusic(i, true);
            }

            /// <summary>
            /// Changes the background music. Choose if it should be played right away.
            /// </summary>
            /// <param name="i">Int, number of the new song to play.</param>
            /// <param name="playNow">Bool, should the song be played right away.</param>
            public void ChangeBGMusic(int i, bool playNow)
            {
                if (i >= backGroundMusic.Length)
                    return;

                ChangeBGMusic(backGroundMusic[i], playNow);
            }

            /// <summary>
            /// Changes the background music.
            /// </summary>
            /// <param name="s">String of the new song to play.</param>
            public void ChangeBGMusic(string s)
            {
                ChangeBGMusic(s, true);
            }

            /// <summary>
            /// Changes the background music. Choose if it should be played right away.
            /// </summary>
            /// <param name="i">Int, number of the new song to play.</param>
            /// <param name="playNow">Bool, should the song be played right away.</param>
            public void ChangeBGMusic(string s, bool playNow)
            {
                if (s == currentBGMusic)
                    return;

                StopBGMusic();
                currentBGMusic = s;
                bgMusic = Create(gameObject, s);

                if (playNow)
                    PlayBGMusic();
            }


            /// <summary>
            /// Stops the background music.
            /// </summary>
            public void StopBGMusic()
            {
                if (!musicPlaying)
                    return;

                musicPlaying = false;
                Stop(bgMusic);
            }

            #endregion
            #region BackgroundAmbience

            /// <summary>
            /// Plays the background music if its paused.
            /// </summary>
            public void PlayBGAmbience()
            {
                if (ambiencePlaying)
                    return;
                ambiencePlaying = true;

                Play(bgAmbience);
            }

            /// <summary>
            /// Changes the background ambience. Plays right away.
            /// </summary>
            /// <param name="i">Int, number of the new ambience to play.</param>
            public void ChangeBGAmbience(int i)
            {
                ChangeBGAmbience(i, true);
            }

            /// <summary>
            /// Changes the background Ambience. Choose if it should be played right away.
            /// </summary>
            /// <param name="i">Int, number of the new ambience to play.</param>
            /// <param name="playNow">Bool, should the ambience be played right away.</param>
            public void ChangeBGAmbience(int i, bool playNow)
            {
                if (i >= backGroundAmbience.Length)
                    return;

                ChangeBGAmbience(backGroundAmbience[i], playNow);
            }

            /// <summary>
            /// Changes the background ambience.
            /// </summary>
            /// <param name="s">String of the new ambience to play.</param>
            public void ChangeBGAmbience(string s)
            {
                ChangeBGAmbience(s, true);
            }

            /// <summary>
            /// Changes the background ambience. Choose if it should be played right away.
            /// </summary>
            /// <param name="i">Int, number of the new ambience to play.</param>
            /// <param name="playNow">Bool, should the ambience be played right away.</param>
            public void ChangeBGAmbience(string s, bool playNow)
            {
                if (s == currentBGAmbience)
                    return;

                StopBGAmbience();
                currentBGAmbience = s;
                bgAmbience = Create(gameObject, s);

                if (playNow)
                    PlayBGAmbience();
            }


            /// <summary>
            /// Stops the background ambience.
            /// </summary>
            public void StopBGAmbience()
            {
                if (!ambiencePlaying)
                    return;

                ambiencePlaying = false;
                Stop(bgAmbience);
            }

            public static void SetValues(EventInstance ei, string parameterName, float value)
            {
                ei.setParameterByName(parameterName, value);
            }

            #endregion

            public static void SetGlobalValue(string parameterName, float value)
            {
                RuntimeManager.StudioSystem.setParameterByName(parameterName, value);
            }

            public static float GetGlobalValue(string parameterName)
            {
                RuntimeManager.StudioSystem.getParameterByName(parameterName, out float parameter);
                return parameter;
            }

            public static Bus GetBus(Busses b)
            {
                return AllBusses.GetBus(b);
            }

            public static Bus GetBus(string s)
            {
                return RuntimeManager.GetBus(s);
            }

            public static VCA GetVCA(VCAs v)
            {
                return AllVCAs.GetVCA(v);
            }

            public static VCA GetVCA(string s)
            {
                return RuntimeManager.GetVCA(s);
            }

            public void LoadMutes()
            {
                foreach (string s in AllBusses.busPaths)
                {
                    if (s == null || s.Length == 0)
                        continue;

                    Bus b = GetBus(s);

                    string name = s + "mute";

                    bool value = GetBool(PlayerPrefs.GetInt(name, SetBool(GetMute(b))));

                    SetMute(b, value);
                }

                foreach (string s in AllVCAs.VCAPaths)
                {
                    if (s == null || s.Length == 0)
                        continue;

                    VCA v = GetVCA(s);

                    string name = s + "mute";

                    bool value = GetBool(PlayerPrefs.GetInt(name, SetBool(GetMute(v))));

                    SetMute(v, value);
                }
            }

            public static void SetMute(Busses b, bool mute)
            {
                SetMute(GetBus(b), mute);
            }

            public static bool GetMute(Busses b)
            {
                return GetMute(GetBus(b));
            }
            public static void SetMute(VCAs v, bool mute)
            {
                SetMute(GetVCA(v), mute);
            }

            public static bool GetMute(VCAs v)
            {
                return GetMute(GetVCA(v));
            }

            public static void SetMute(Bus b, bool mute)
            {
                SetMuteBus(b, mute);
            }

            public static bool GetMute(Bus b)
            {
                return GetMuteBus(b);
            }
            public static void SetMute(VCA v, bool mute)
            {
                SetMuteVCA(v, mute);
            }

            public static bool GetMute(VCA v)
            {
                return GetMuteVCA(v);
            }


            static void SetMuteBus(Bus b, bool mute)
            {
                string name = "";
                b.getPath(out name);
                name += "mute";
                PlayerPrefs.SetInt(name, SetBool(mute));
                b.setMute(mute);

                Save();
            }

            static bool GetMuteBus(Bus b)
            {
                string name = "";
                b.getPath(out name);
                bool mute = false;
                b.getMute(out mute);
                return mute;
            }

            static void SetMuteVCA(VCA v, bool mute)
            {
                string name = "";
                v.getPath(out name);
                name += "mute";
                PlayerPrefs.SetInt(name, SetBool(mute));
                v.setVolume(SetBool(mute));

                Save();
            }

            static bool GetMuteVCA(VCA v)
            {
                return GetVolume(v) <= 0;
            }

            /// <summary>
            /// Returns wanted bus mute.
            /// </summary>
            public static bool StaticGetMute(Busses b)
            {
                return GetMute(AllBusses.GetBus(b));
            }

            /// <summary>
            /// Returns wanted bus mute.
            /// </summary>
            public static bool StaticGetMute(VCAs v)
            {
                return GetMute(AllVCAs.GetVCA(v));
            }

            /// <summary>
            /// Returns wanted bus mute.
            /// </summary>
            public static void StaticSetMute(Busses b, bool mute)
            {
                SetMute(AllBusses.GetBus(b), mute);
            }

            /// <summary>
            /// Returns wanted bus mute.
            /// </summary>
            public static void StaticSetMute(VCAs v, bool mute)
            {
                SetMute(AllVCAs.GetVCA(v), mute);
            }

            ///<summary>
            ///Change int value into a bool. 0 equals false and 1 equals true
            ///</summary>
            static bool GetBool(int i)
            {
                return i <= 0;
            }

            ///<summary>
            ///Change bool value into an int. false equals 0 and true equals 1
            ///</summary>
            static int SetBool(bool b)
            {
                return b ? -100 : 1;
            }

            static void Save()
            {
                PlayerPrefs.Save();
            }
        }
    }
}