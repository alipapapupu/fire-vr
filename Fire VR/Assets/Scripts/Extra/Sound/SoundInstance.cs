﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;



namespace MohaviCreative
{
    namespace Sounds
    {
        [System.Serializable]
        public class SoundInstance
        {
            [Tooltip("Name of the sound.")]
            public string name = "";
            [Tooltip("Number of the sound"), ReadOnly]
            public int number = -1;
            [Tooltip("Is the sound now playable?")]
            public bool canPlay = true;

            [Tooltip("Should there only be one event instance created, or can there be several overlapping.")]
            public bool oneInstance = true;
            [Tooltip("Name of the instance"), SerializeField, EventRef]
            public string playableEvent = null;

            /// <summary>
            /// The actualy instances created from the event.
            /// </summary>
            public List<EventInstance> instances = new List<EventInstance>();

            /// <summary>
            /// Creates new instance to this playable, and attaches them to gameObject
            /// </summary>
            /// <param name="g">GameObject the sound instance will be attached to.</param>
            public EventInstance CreateInstance(GameObject g)
            {
                EventInstance ei = SoundManager.StaticCreate(g, playableEvent, false);
                instances.Add(ei);
                return ei;
            }

            public void Play(GameObject g)
            {
                if (CanPlay(out bool create))
                {
                    EventInstance ei;

                    if (create)
                        ei = CreateInstance(g);
                    else
                        ei = instances[0];

                    SoundManager.StaticPlay(g, ei);
                }
            }

            /// <summary>
            /// Stops all instances from playing further.
            /// </summary>
            public void Stop()
            {
                for (int i = 0; i < instances.Count; i++)
                {
                    PLAYBACK_STATE playing = 0;
                    instances[i].getPlaybackState(out playing);

                    if (instances[i].isValid() && playing == PLAYBACK_STATE.PLAYING)
                        SoundManager.StaticStop(instances[i]);
                }
            }

            public void ChangeValue(string parameterName, float parameterValue)
            {
                foreach (EventInstance ei in instances)
                {
                    ei.setParameterByName(parameterName, parameterValue);
                }
            }

            /// <summary>
            /// Checker to see if given integer contains event in array, and if it can actually be played.
            /// </summary>
            /// <param name="create">bool given out that tells if new instance should be created or not.</param>
            public bool CanPlay(out bool create)
            {
                bool usable = false;
                create = true;

                //If there can be several, just create a new one
                if (oneInstance)
                    return true;

                //If there is already one, just check if it can be played again.
                if (instances.Count == 1)
                {
                    instances[0].getPlaybackState(out PLAYBACK_STATE state);
                    usable = state == PLAYBACK_STATE.STOPPED;
                    create = false;
                }
                //if there isnt any, just create a new one.
                else if (instances.Count == 0)
                {
                    usable = true;
                }

                //Return true if the sound can be played without problems.
                return (canPlay && usable);
            }
        }
    }
}