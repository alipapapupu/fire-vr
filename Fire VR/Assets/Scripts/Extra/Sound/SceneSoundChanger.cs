﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FMODUnity;
using FMOD.Studio;

namespace MohaviCreative
{
    namespace Sounds
    {
        public class SceneSoundChanger : Singleton<SceneSoundChanger>
        {
            [SerializeField, HideInInspector]
            int length = 0;
            [SerializeField]
            SceneInfo[] sceneInfos = new SceneInfo[0];
            [SerializeField]
            bool useDefault = false;
            [SerializeField]
            SceneInfo defaultSceneInfo = null;

            protected override void Awake()
            {
                base.Awake(true);
                transform.parent = null;
                DontDestroyOnLoad(gameObject);
            }

            /// <summary>
            /// Awake is called when the script instance is being loaded.
            /// </summary>
            void Start()
            {
                if (validate)
                {
                    SceneChanged(SceneManager.GetActiveScene(), LoadSceneMode.Single);
                    SceneManager.sceneLoaded += SceneChanged;
                }
            }

            void SceneChanged(Scene scene, LoadSceneMode mode)
            {
                if (defaultSceneInfo.stopSongOnSceneChange)
                    defaultSceneInfo.Stop();

                bool useDefault = true;

                foreach (SceneInfo si in sceneInfos)
                {
                    if (scene.buildIndex != si.sceneNumber)
                    {
                        if (si.stopSongOnSceneChange)
                            si.Stop();
                        continue;
                    }

                    useDefault = false;

                    SceneInfoFunction(si);
                }

                if (useDefault)
                {
                    SceneInfoFunction(defaultSceneInfo);
                }
            }

            void SceneInfoFunction(SceneInfo si)
            {
                if (si.changeParameter)
                {
                    if (si.globalParameter)
                    {
                        si.ChangeGlobalParameter();
                    }
                    else
                    {
                        foreach (SceneInfo s in sceneInfos)
                        {
                            if (s.instance.playableEvent == si.instance.playableEvent)
                            {
                                s.ChangeLocalParameter(si.parameterValue);
                            }
                        }
                    }
                }
                else if (si.playSong)
                {
                    si.Play(gameObject);
                }
            }

            void Reset()
            {
                sceneInfos = new SceneInfo[SceneManager.sceneCountInBuildSettings];
                OnValidate();
            }

            void OnValidate()
            {
                if (sceneInfos == null)
                {
                    return;
                }

                for (int i = 0; i < sceneInfos.Length; i++)
                {
                    if (i > length)
                        sceneInfos[i].Reset();

                    sceneInfos[i].CheckName();
                    sceneInfos[i].CheckNumber();

                    if (sceneInfos[i] == null)
                        sceneInfos[i] = new SceneInfo();

                    if (i > length && sceneInfos[i].tempNumber == -1)
                        sceneInfos[i].tempNumber = i;

                    sceneInfos[i].SetTemps();
                }

                length = sceneInfos.Length;
            }

            [System.Serializable]
            public class SceneInfo
            {
                [Tooltip("Name of the scene.")]
                public string sceneName = "";
                [Tooltip("Number of the scene in build settings.")]
                public int sceneNumber = -1;
                [Tooltip("Should the script change parameter.")]
                public bool changeParameter = false;
                [Tooltip("Is the parameter global")]
                public bool globalParameter = true;
                [Tooltip("Parameters name")]
                public string parameterName = "";
                [Tooltip("What value to change the parameter into.")]
                public float parameterValue = 0;
                [Tooltip("Should the script change song when scene changing.")]
                public bool playSong = false;
                [Tooltip("Should the script stop the song when scene changes.")]
                public bool stopSongOnSceneChange = false;
                [Tooltip("Info for instance.")]
                public SoundInstance instance = null;
                [HideInInspector]
                public string tempName = "";
                [HideInInspector]
                public int tempNumber = -1;

                public void ChangeGlobalParameter()
                {
                    RuntimeManager.StudioSystem.getParameterByName(parameterName, out float test);

                    SoundManager.SetGlobalValue(parameterName, parameterValue);

                    RuntimeManager.StudioSystem.getParameterByName(parameterName, out test);
                }

                public void ChangeLocalParameter(float value)
                {
                    instance.ChangeValue(parameterName, value);
                }

                public void Play(GameObject g)
                {
                    instance.Play(g);
                }

                public void Stop()
                {
                    instance.Stop();
                }

                public void Reset()
                {
                    sceneName = "";
                    sceneNumber = -1;
                    changeParameter = false;
                    globalParameter = true;
                    parameterName = "";
                    parameterValue = 0;
                    playSong = false;
                    stopSongOnSceneChange = false;
                    instance = null;
                }

                public void CheckName()
                {
                    if (sceneName != "")
                    {
                        if (CheckSceneName(sceneName, out int num))
                        {
                            if (tempName != sceneName)
                                sceneNumber = num;
                        }
                        else if (tempName == sceneName)
                        {
                            Debug.LogWarning("No scene by the name of " + sceneName);
                            sceneNumber = tempNumber;
                            sceneName = GetSceneName(sceneNumber);
                        }
                    }
                    else if (tempName != sceneName)
                    {
                        sceneNumber = -1;
                    }
                }

                public void CheckNumber()
                {
                    if (sceneNumber != tempNumber && sceneNumber != -1)
                    {
                        if (sceneNumber < -1)
                        {
                            sceneNumber = -1;
                        }
                        else if (SceneManager.sceneCountInBuildSettings > sceneNumber)
                        {
                            sceneName = GetSceneName(sceneNumber);
                        }
                        else
                        {
                            Debug.LogWarning("There is no scene by number " + sceneNumber);
                            sceneNumber = tempNumber;
                            sceneName = GetSceneName(sceneNumber);
                        }
                    }
                    else if (sceneNumber == -1)
                    {
                        sceneName = "";
                    }
                }

                public void SetTemps()
                {
                    tempName = sceneName;
                    tempNumber = sceneNumber;
                }

                string GetSceneName(int num)
                {
                    return System.IO.Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(num));
                }

                bool CheckSceneName(string s, out int num)
                {
                    num = -1;
                    for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
                    {
                        if (GetSceneName(i) == s)
                        {
                            num = i;
                            return true;
                        }
                    }
                    return false;
                }
            }
        }

    }
}