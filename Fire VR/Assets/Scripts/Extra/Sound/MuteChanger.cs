﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MohaviCreative
{
    namespace Sounds
    {
        public class MuteChanger : MonoBehaviour
        {
            [System.Serializable]
            public class BoolEvent : UnityEvent<bool> { }
            [Tooltip("True = use Bus\nFalse = use vca")]
            public bool useBus = true;
            [Tooltip("Bus to change the volume for.")]
            public Busses bus = 0;
            [Tooltip("vca to change the volume for.")]
            public VCAs vca = 0;
            [Tooltip("When the scene starts, where the volume should initally be set.")]
            public BoolEvent muteSetter = null;
            [Tooltip("Inverse the outgoing bool.\nTrue: when mute, sends out true. When unmute, send out false.\nFalse: when mute, sends out false. When unmute, sends out true.")]
            public bool inverseMute = false;
            bool mute = false;

            void Start()
            {
                if (useBus)
                    mute = SoundManager.StaticGetMute(bus);
                else
                    mute = SoundManager.StaticGetMute(vca);

                muteSetter?.Invoke(!inverseMute ? !mute : mute);
            }

            public void ChangeMute()
            {
                mute = !mute;
                if (useBus)
                    SoundManager.StaticSetMute(bus, mute);
                else
                    SoundManager.StaticSetMute(vca, mute);

                muteSetter?.Invoke(!inverseMute ? !mute : mute);
            }
        }

#if UNITY_EDITOR
        [CustomEditor(typeof(MuteChanger))]
        [CanEditMultipleObjects]
        public class MuteChangerEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                serializedObject.Update();

                SerializedProperty useBus = serializedObject.FindProperty("useBus");
                EditorGUILayout.PropertyField(useBus);

                if (useBus.boolValue)
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("bus"));
                }
                else
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("vca"));
                }

                EditorGUILayout.PropertyField(serializedObject.FindProperty("inverseMute"));

                EditorGUILayout.PropertyField(serializedObject.FindProperty("muteSetter"));


                serializedObject.ApplyModifiedProperties();
            }
        }

#endif
    }
}