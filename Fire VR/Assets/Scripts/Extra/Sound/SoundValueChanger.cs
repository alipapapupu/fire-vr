﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MohaviCreative
{
    namespace Sounds
    {
        public class SoundValueChanger : MonoBehaviour
        {
            [SerializeField]
            string parameterName = "";

            public void SetGlobalValue(float value)
            {
                SetGlobalValue(parameterName, value);
            }

            public void SetGlobalValue(string parameterName, float value)
            {
                SoundManager.SetGlobalValue(parameterName, value);
            }
        }
    }
}
