﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MohaviCreative
{
    namespace Sounds
    {
        public class VolumeChanger : MonoBehaviour
        {
            [System.Serializable]
            public class FloatEvent : UnityEvent<float> { }
            [Tooltip("True = use Bus\nFalse = use vca")]
            public bool useBus = true;
            [Tooltip("Bus to change the volume for.")]
            public Busses bus = 0;
            [Tooltip("vca to change the volume for.")]
            public VCAs vca = 0;
            [Tooltip("When the scene starts, where the volume should initally be set.")]
            public FloatEvent volumeSetter = null;
            bool first = true;

            void Start()
            {
                float volume = 0;

                if (useBus)
                    volume = SoundManager.StaticGetVolume(bus);
                else
                    volume = SoundManager.StaticGetVolume(vca);

                volumeSetter?.Invoke(volume);
            }

            public void ChangeVolume(float f)
            {
                if (first)
                {
                    first = false;
                    return;
                }

                if (useBus)
                    SoundManager.StaticSetVolume(bus, f);
                else
                    SoundManager.StaticSetVolume(vca, f);

                volumeSetter?.Invoke(f);
            }
        }

#if UNITY_EDITOR
        [CustomEditor(typeof(VolumeChanger))]
        [CanEditMultipleObjects]
        public class VolumeChangerEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                serializedObject.Update();

                SerializedProperty useBus = serializedObject.FindProperty("useBus");
                EditorGUILayout.PropertyField(useBus);

                if (useBus.boolValue)
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("bus"));
                }
                else
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("vca"));
                }

                EditorGUILayout.PropertyField(serializedObject.FindProperty("volumeSetter"));


                serializedObject.ApplyModifiedProperties();
            }
        }
#endif
    }
}
