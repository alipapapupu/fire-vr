﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;


namespace MohaviCreative
{
    namespace Sounds
    {
        /// <summary>
        /// Class for quick sound effects. Use PlaySound to play sound.
        /// </summary>
        public class SoundPlayer : Collection<SoundPlayer>
        {

            [Tooltip("Checker if this sound player can currently play anything.")]
            public bool canPlay = true;
            public SoundPlayable[] playables = null;

            /// <summary>
            /// Checks and plays all the sounds that should play when created. Takes in consideration are the created to the game or loaded by scene.
            /// </summary>
            void Awake()
            {
                for (int i = 0; i < playables.Length; i++)
                {
                    bool playNow = playables[i].playAutomatically &&
                    (UnityEngine.SceneManagement.SceneManager.GetActiveScene().isLoaded ||
                    (!UnityEngine.SceneManagement.SceneManager.GetActiveScene().isLoaded &&
                    !playables[i].dontPlayOnSceneLoad));

                    if (playNow)
                        PlaySoundInstances(i);
                }
            }

            /// <summary>
            /// This function is called when the behaviour becomes disabled or inactive.
            /// </summary>
            override protected void OnDisable()
            {
                base.OnDisable();
                StopAll();
            }

            /// <summary>
            /// Plays the wanted sound. Just give the correct int from SoundManagers sound list. Scene has to have SoundManager for this to work.
            /// </summary>
            /// <param name="i">Plays the sound from soundmanagers list in placement I. Should be smaller than soundEffects in soundmanager/param>
            public void PlaySound(int i)
            {
                if (canPlay)
                    SoundManager.instance.Play(transform, i);
            }

            /// <summary>
            /// Plays the wanted sound. Just give the correct string location of the sound. Basically uses FMODs event references.
            /// </summary>
            /// <param name="s">Plays the sound with the name s. The s should basically be fmod event ref.</param>
            public void PlaySoundString(string s)
            {
                if (canPlay)
                    SoundManager.StaticPlay(transform, s);
            }

            /// <summary>
            /// Plays the 0 sound of the player. Requires that the sound player has at least 1 playable in array.
            /// </summary>
            public void PlaySoundPlayable()
            {
                if (canPlay)
                {
                    PlaySoundInstances(0);
                }
            }


            /// <summary>
            /// Attaches and playes the wanted sound on the player. Wont play anything if given number is bigger than length of array.
            /// </summary>
            /// <param name="i">Plays the sound from playables array. The value of i should be smaller than length of playables array.</param>
            public void PlaySoundInstances(int i)
            {
                PlaySoundInstances(i, gameObject);
            }

            /// <summary>
            /// Plays the wanted sound of the player, and attach it to wanted gameObject. Wont play anything if given number is bigger than length of array, or gameObject is null.
            /// </summary>
            /// <param name="i">Plays the sound from playables array. The value of i should be smaller than length of playables array.</param>
            /// <param name="g">GameObject the sound will be attached to.</param>
            public void PlaySoundInstances(int i, GameObject g)
            {
                if (CanPlay(i) && g != null)
                {
                    playables[i].Play(g);
                }
            }

            /// <summary>
            /// Attaches and plays the wanted sound on the player. Wont play anything if given number is bigger than length of array.
            /// </summary>
            /// <param name="s">Plays the sound from playables array. S should be name of sound</param>
            public void PlaySoundInstancesString(string s)
            {
                PlaySoundInstances(s);
            }

            /// <summary>
            /// Attaches and plays the wanted sound on the player. Wont play anything if given number is bigger than length of array.
            /// </summary>
            /// <param name="s">Plays the sound from playables array. S should be name of sound</param>
            public void PlaySoundInstances(string s)
            {
                PlaySoundInstances(s, gameObject);
            }


            public void PlaySoundInstances(string s, GameObject g)
            {
                PlaySoundInstances(CheckSoundName(s), g);
            }

            /// <summary>
            /// Plays the wanted eventInstance created and stored in other script. Wont play anything if the instance isn't valid playale.
            /// </summary>
            /// <param name="ei">EventInstance to play.</param>
            public void PlaySoundInstances(EventInstance ei)
            {
                if (CanPlay(ei))
                    SoundManager.StaticPlay(gameObject, ei);
            }

            /// <summary>
            /// Stops all instances of the wanted sound this sound player creates. Requires that the sound is valid and actually playing.
            /// </summary>
            /// <param name="i">Stops the sound from playables array. The value of i should be smaller than length of playables array.</param>
            public void Stop(int i)
            {
                if (i >= playables.Length)
                    return;


                playables[i].Stop();
            }

            /// <summary>
            /// Stops all instances of the wanted sound this sound player creates. Requires that the sound is valid and actually playing.
            /// </summary>
            /// <param name="s">Stops the sound from playables array. The value of s should be the name given to sound.</param>
            public void Stop(string s)
            {
                Stop(CheckSoundName(s));
            }

            ///<summary>
            ///Stops all the sounds from this instance
            ///</summary>
            public void StopAll()
            {
                for (int i = 0; i < playables.Length; i++)
                {
                    Stop(i);
                }
            }

            ///<summary>
            ///Stops all the sounds from all the instances
            ///</summary>
            public void StopAllInstances()
            {
                StopAllInstancesStatic();
            }

            ///<summary>
            ///Stops all the sounds from all the instances
            ///</summary>
            public static void StopAllInstancesStatic()
            {
                foreach (SoundPlayer sp in instances)
                {
                    sp.StopAll();
                }
            }

            int CheckSoundName(string s)
            {
                for (int i = 0; i < playables.Length; i++)
                {
                    if (playables[i].name == s)
                    {
                        return i;
                    }
                }
                return -1;
            }

            /// <summary>
            /// Sets the wanted parameter if given the sound it belongs to, the name of the parameter, and value that is wanted to give to it.
            /// </summary>
            /// <param name="i">The sound from playables array the value is changed from.</param>
            /// <param name="parameterName">Name of the parameter we want to change.</param>
            /// <param name="value">Value we want to set to the parameter.</param>
            public void SetValue(int i, string parameterName, float value)
            {
                for (int o = 0; o < playables[i].instances.Count; o++)
                {
                    SoundManager.SetValues(playables[i].instances[o], parameterName, value);
                }
            }

            /// <summary>
            /// Checker to see if given integer contains event in array, and if it can actually be played.
            /// </summary>
            /// <param name="i">The sound from playables array we want to check.</param>
            bool CanPlay(int i)
            {
                //If there is nothing to play, just return false
                if (playables == null || i >= playables.Length)
                    return false;

                //Return true if the sound can be played without problems.
                return canPlay;

            }

            /// <summary>
            /// Checker to see if given eventInstance is something that can actually be played.
            /// </summary>
            /// <param name="ei">EventInstance that is checked if it can be played.</param>
            bool CanPlay(EventInstance ei)
            {
                PLAYBACK_STATE state;
                ei.getPlaybackState(out state);

                return (canPlay && state == PLAYBACK_STATE.STOPPED);

            }

            /// <summary>
            /// Creates new instance to playable at position i.
            /// </summary>
            /// <param name="i">Playable to create the new instance to. Should be smaller than playable array.</param>
            EventInstance CreateInstance(int i)
            {
                return CreateInstance(i, gameObject);
            }

            /// <summary>
            /// Creates new instance to playable at position i, and attaches it to gameObject g.
            /// </summary>
            /// <param name="i">Playable to create the new instance to. Should be smaller than playable array.</param>
            /// <param name="g">GameObject to attach the sound to.</param>
            EventInstance CreateInstance(int i, GameObject g)
            {
                return playables[i].CreateInstance(g);
            }

            void OnValidate()
            {
                if (playables == null)
                    return;

                for (int i = 0; i < playables.Length; i++)
                {
                    playables[i].number = i;
                }
            }

            /// <summary>
            /// Class to contain all needed things to play all wanted sounds correctly.<!---->
            /// </summary>
            [System.Serializable]
            public class SoundPlayable : SoundInstance
            {
                public bool isPlaying = false;
                [Tooltip("Should the sound be played when object is created?")]
                public bool playAutomatically = false;
                [Tooltip("Should the sound be not played when its created by scene load?")]
                public bool dontPlayOnSceneLoad = false;
            }
        }
    }
}