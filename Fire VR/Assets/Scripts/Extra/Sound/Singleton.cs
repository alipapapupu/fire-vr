﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;
    public static T instance
    {
        get { return _instance; }
    }
    public bool validate
    {
        get { return _instance == this; }
    }
    virtual protected void Awake()
    {
        Awake(false);
    }

    protected void Awake(bool noMessage)
    {
        if (_instance != null)
        {
            if (!noMessage)
                Debug.LogWarning("There are 2 singletons in scene. Take care of it.");

            Destroy(gameObject);
            return;
        }

        _instance = this as T;
    }
}
