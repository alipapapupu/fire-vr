﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using FMODUnity;
using FMOD;
using FMOD.Studio;

public class CreateBusses
{
    [MenuItem("Tools/CreateBusses")]
    public static void Go()
    {
        CreateBanks();
        CreateVCA();

        AssetDatabase.Refresh();
    }

    static void CreateBanks()
    {
        string enumName = "Busses";
        Bank[] banks = null;
        RuntimeManager.StudioSystem.getBankList(out banks);
        List<string> busNames = new List<string>();

        foreach (Bank b in banks)
        {
            Bus[] tempBus = null;
            b.getBusList(out tempBus);

            foreach (Bus bb in tempBus)
            {
                string path = "";
                bb.getPath(out path);

                if (!busNames.Contains(path) && path.Length > 0)
                {
                    int lastIndex = path.LastIndexOf("/") + 1;
                    string name = path.Remove(0, lastIndex);

                    if (name.Length > 0)
                        busNames.Add(path);
                }
            }
        }


        string directory = "Assets/Sounds/Banks";

        if (!Directory.Exists(directory))
            Directory.CreateDirectory(directory);

        string filePathAndName = directory + "/" + enumName + ".cs"; //The folder Scripts/Enums/ is expected to exist

        string[] enumEntries = GetEnumEntries(filePathAndName, busNames);

        using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
        {
            streamWriter.WriteLine("using FMODUnity;\n\npublic enum " + enumName + "{");
            for (int i = 0; i < enumEntries.Length; i++)
            {
                if (enumEntries[i] == null || enumEntries.Length == 0)
                    continue;

                int lastIndex = enumEntries[i].LastIndexOf("/") + 1;
                string name = enumEntries[i].Remove(0, lastIndex);
                name = name.Replace(" ", "");

                if (name.Length == 0)
                    continue;

                streamWriter.Write("\t" + name + " = " + i);

                if (i != enumEntries.Length - 1)
                    streamWriter.WriteLine(",");
            }
            streamWriter.WriteLine("}\n\t public static class AllBusses {\n public static string[] busPaths = new string[]\n{ ");

            for (int i = 0; i < enumEntries.Length; i++)
            {

                streamWriter.Write("\n\"" + enumEntries[i] + "\"");
                if (i != enumEntries.Length - 1)
                    streamWriter.WriteLine(",");
            }

            streamWriter.WriteLine("\n};\n public static FMOD.Studio.Bus GetBus(" + enumName + " bus) \n { \n" +
            "return RuntimeManager.GetBus(busPaths[(int)bus]);\n" +
            "}\n}");
        }
    }

    static void CreateVCA()
    {
        string vcaEnumName = "VCAs";
        Bank[] banks = null;
        RuntimeManager.StudioSystem.getBankList(out banks);
        List<string> vcaNames = new List<string>();

        foreach (Bank b in banks)
        {
            VCA[] tempVCA = null;
            b.getVCAList(out tempVCA);

            foreach (VCA v in tempVCA)
            {
                string path = "";
                v.getPath(out path);

                if (!vcaNames.Contains(path) && path.Length > 0)
                {
                    int lastIndex = path.LastIndexOf("/") + 1;
                    string name = path.Remove(0, lastIndex);

                    if (name.Length > 0)
                        vcaNames.Add(path);
                }
            }
        }

        string directory = "Assets/Sounds/Banks";

        if (!Directory.Exists(directory))
            Directory.CreateDirectory(directory);

        string filePathAndName = directory + "/" + vcaEnumName + ".cs"; //The folder Scripts/Enums/ is expected to exist

        string[] enumEntries = GetEnumEntries(filePathAndName, vcaNames);

        using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
        {
            streamWriter.WriteLine("using FMODUnity;\n\npublic enum " + vcaEnumName + "{");
            for (int i = 0; i < enumEntries.Length; i++)
            {
                int lastIndex = enumEntries[i].LastIndexOf("/") + 1;
                string name = enumEntries[i].Remove(0, lastIndex);
                name = name.Replace(" ", "");

                if (name.Length == 0)
                    continue;

                streamWriter.Write("\t" + name + " = " + i);

                if (i != enumEntries.Length - 1)
                    streamWriter.WriteLine(",");
            }
            streamWriter.WriteLine("\n}\n\t public static class AllVCAs {\n public static string[] VCAPaths = new string[]\n{ ");

            for (int i = 0; i < enumEntries.Length; i++)
            {

                streamWriter.Write("\n\"" + enumEntries[i] + "\"");
                if (i != enumEntries.Length - 1)
                    streamWriter.WriteLine(",");
            }

            streamWriter.WriteLine("};\n public static FMOD.Studio.VCA GetVCA(" + vcaEnumName + " vca) \n { \n" +
            "return RuntimeManager.GetVCA(VCAPaths[(int)vca]);\n" +
            "}\n}");
        }
    }

    ///<summary>
    ///Checks the files and makes sure that already assigned enums will keep their correct numberValue.
    ///</summary>
    static string[] GetEnumEntries(string filePath, List<string> names)
    {
        Dictionary<string, int> lastNumbers = new Dictionary<string, int>();
        int biggestNum = -1;

        if (File.Exists(filePath))
        {
            using (StreamReader streamReader = new StreamReader(filePath))
            {
                int i = 0;
                string line = "";
                bool found = false;
                while ((line = streamReader.ReadLine()) != null)
                {
                    if (!found)
                    {
                        if (line.Contains("string[]"))
                        {
                            found = true;
                        }
                    }
                    else
                    {
                        if (line.Contains("{") || line.Length == 0)
                            continue;

                        line = line.Replace("\t", "");
                        line = line.Replace("\"", "");
                        line = line.Replace(",", "");

                        string[] lines = line.Split('=');

                        if (names.Contains(lines[0]))
                        {
                            names.Remove(lines[0]);
                            int correctNum = i;

                            if (lines.Length > 1 && int.TryParse(lines[1], out int num))
                                correctNum = num;

                            lastNumbers.Add(lines[0], correctNum);
                            if (biggestNum < correctNum)
                                biggestNum = correctNum;
                        }
                        else if (line.Contains("}"))
                        {
                            break;
                        }

                        i++;
                    }
                }
            }
        }

        biggestNum++;

        if (biggestNum < lastNumbers.Count + names.Count)
            biggestNum = lastNumbers.Count + names.Count;

        string[] enumEntries = new string[biggestNum];

        foreach (string s in lastNumbers.Keys)
        {
            enumEntries[lastNumbers[s]] = s;
        }

        if (names.Count > 0)
        {
            int currentNum = 0;
            for (int i = 0; i < biggestNum; i++)
            {
                if (enumEntries[i] == null || enumEntries[i].Length == 0)
                {
                    enumEntries[i] = names[currentNum];
                    currentNum++;

                    if (currentNum >= names.Count)
                        break;
                }
            }
        }

        return enumEntries;
    }
}
#endif