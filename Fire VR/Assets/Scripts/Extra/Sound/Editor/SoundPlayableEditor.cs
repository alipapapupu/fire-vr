﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace MohaviCreative
{
    namespace Sounds
    {
        [CustomPropertyDrawer(typeof(SoundPlayer.SoundPlayable))]
        [CanEditMultipleObjects]
        public class SoundPlayableEditor : PropertyDrawer
        {
            SerializedProperty name = null;
            SerializedProperty number = null;
            SerializedProperty canPlay = null;
            SerializedProperty oneInstance = null;
            SerializedProperty playableEvent = null;
            SerializedProperty playAutomatically = null;
            SerializedProperty dontPlayOnSceneLoad = null;
            float amount = 0;


            public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                Rect tempRect = position;
                // Don't make child fields be indented
                var indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = 1;
                amount = 0;

                SetProperties(property);

                Draw(property, tempRect);
                if (property.isExpanded)
                {
                    EditorGUI.indentLevel = 2;
                    Draw(name, tempRect);

                    amount += EditorGUIUtility.singleLineHeight * 0.2f;
                    GUI.enabled = false;
                    Draw(number, tempRect);
                    GUI.enabled = true;

                    Draw(canPlay, tempRect);

                    Draw(oneInstance, tempRect);
                    Draw(playableEvent, tempRect);
                    Draw(playAutomatically, tempRect);

                    Draw(dontPlayOnSceneLoad, tempRect);
                }
            }
            void Draw(SerializedProperty s, VisualElement container)
            {
                container.Add(new PropertyField(s));
            }

            void Draw(SerializedProperty s, Rect r)
            {
                r.y += amount;
                r.height = EditorGUI.GetPropertyHeight(s, false);
                EditorGUI.PropertyField(r, s);
                amount += EditorGUI.GetPropertyHeight(s, false);
            }
            void Draw(SerializedProperty s)
            {
                EditorGUILayout.PropertyField(s);
            }

            public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
            {
                SetProperties(property);

                float amount = 0;

                amount += EditorGUI.GetPropertyHeight(property, false);
                if (property.isExpanded)
                {
                    EditorGUI.indentLevel = 2;
                    amount += EditorGUI.GetPropertyHeight(name, false);

                    amount += EditorGUI.GetPropertyHeight(number, false);

                    amount += EditorGUI.GetPropertyHeight(canPlay, false);

                    amount += EditorGUI.GetPropertyHeight(oneInstance, false);
                    amount += EditorGUI.GetPropertyHeight(playableEvent, false);
                    amount += EditorGUI.GetPropertyHeight(playAutomatically, false);

                    amount += EditorGUI.GetPropertyHeight(dontPlayOnSceneLoad, false);
                }

                return EditorGUI.GetPropertyHeight(property, false) + amount;
            }

            void SetProperties(SerializedProperty property)
            {
                name = property.FindPropertyRelative("name");
                number = property.FindPropertyRelative("number");
                canPlay = property.FindPropertyRelative("canPlay");
                oneInstance = property.FindPropertyRelative("oneInstance");
                playableEvent = property.FindPropertyRelative("playableEvent");
                playAutomatically = property.FindPropertyRelative("playAutomatically");
                dontPlayOnSceneLoad = property.FindPropertyRelative("dontPlayOnSceneLoad");
            }
        }
    }
}