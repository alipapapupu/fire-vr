﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace MohaviCreative
{
    namespace Sounds
    {
        [CustomPropertyDrawer(typeof(SceneSoundChanger.SceneInfo))]
        [CanEditMultipleObjects]
        public class SceneInfoEditor : PropertyDrawer
        {
            SerializedProperty sceneName = null;
            SerializedProperty sceneNumber = null;
            SerializedProperty changeParameter = null;
            SerializedProperty globalParameter = null;
            SerializedProperty parameterName = null;
            SerializedProperty parameterValue = null;
            SerializedProperty playSong = null;
            SerializedProperty eventName = null;
            SerializedProperty stopSongOnSceneChange = null;
            SerializedProperty instance = null;
            float amount = 0;


            public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
            {
                Rect tempRect = position;
                // Don't make child fields be indented
                var indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = 1;
                amount = 0;

                SetProperties(property);


                Draw(property, tempRect);
                if (property.isExpanded)
                {
                    EditorGUI.indentLevel = 2;
                    Draw(sceneName, tempRect);
                    amount += EditorGUIUtility.singleLineHeight * 0.2f;
                    Draw(sceneNumber, tempRect);

                    if (sceneNumber.intValue == -1 && sceneName.stringValue == "")
                        return;

                    if (!playSong.boolValue)
                    {
                        Draw(changeParameter, tempRect);

                        if (changeParameter.boolValue)
                        {
                            Draw(globalParameter, tempRect);
                            Draw(parameterName, tempRect);
                            Draw(parameterValue, tempRect);
                            return;
                        }
                    }
                    else
                    {
                        amount += EditorGUIUtility.singleLineHeight;
                    }

                    Draw(playSong, tempRect);


                    if (playSong.boolValue)
                    {
                        Draw(stopSongOnSceneChange, tempRect);
                        Draw(instance.FindPropertyRelative("canPlay"), tempRect);
                        Draw(instance.FindPropertyRelative("oneInstance"), tempRect);
                        Draw(instance.FindPropertyRelative("playableEvent"), tempRect);
                    }
                }
            }
            void Draw(SerializedProperty s, VisualElement container)
            {
                container.Add(new PropertyField(s));
            }

            void Draw(SerializedProperty s, Rect r)
            {
                r.y += amount;
                r.height = EditorGUI.GetPropertyHeight(s, false);
                EditorGUI.PropertyField(r, s);
                amount += EditorGUI.GetPropertyHeight(s, false);
            }
            void Draw(SerializedProperty s)
            {
                EditorGUILayout.PropertyField(s);
            }

            public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
            {
                SetProperties(property);

                float amount = 0;
                if (property.isExpanded)
                {
                    amount += EditorGUI.GetPropertyHeight(property, false);
                    amount += EditorGUI.GetPropertyHeight(sceneName, false);
                    amount += EditorGUI.GetPropertyHeight(sceneNumber, false);

                    if (sceneNumber.intValue == -1 && sceneName.stringValue == "")
                        return amount;

                    if (!playSong.boolValue)
                    {
                        amount += EditorGUI.GetPropertyHeight(changeParameter, false);

                        if (changeParameter.boolValue)
                        {
                            amount += EditorGUI.GetPropertyHeight(globalParameter, false);
                            amount += EditorGUI.GetPropertyHeight(parameterName, false);
                            amount += EditorGUI.GetPropertyHeight(parameterValue, false);
                            return amount;
                        }
                    }
                    else
                    {
                        amount += EditorGUIUtility.singleLineHeight;
                    }

                    amount += EditorGUI.GetPropertyHeight(playSong, false);


                    if (playSong.boolValue)
                    {
                        amount += EditorGUI.GetPropertyHeight(stopSongOnSceneChange, false);
                        amount += EditorGUI.GetPropertyHeight(instance.FindPropertyRelative("canPlay"), false);
                        amount += EditorGUI.GetPropertyHeight(instance.FindPropertyRelative("oneInstance"), false);
                        amount += EditorGUI.GetPropertyHeight(instance.FindPropertyRelative("playableEvent"), false);
                    }
                }

                return EditorGUI.GetPropertyHeight(property, false) + amount;
            }

            void SetProperties(SerializedProperty property)
            {
                sceneName = property.FindPropertyRelative("sceneName");
                sceneNumber = property.FindPropertyRelative("sceneNumber");
                changeParameter = property.FindPropertyRelative("changeParameter");
                globalParameter = property.FindPropertyRelative("globalParameter");
                parameterName = property.FindPropertyRelative("parameterName");
                parameterValue = property.FindPropertyRelative("parameterValue");
                playSong = property.FindPropertyRelative("playSong");
                eventName = property.FindPropertyRelative("eventName");
                stopSongOnSceneChange = property.FindPropertyRelative("stopSongOnSceneChange");
                instance = property.FindPropertyRelative("instance");
            }
        }
    }
}