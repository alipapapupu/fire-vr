﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public static class Raycaster
{
    static Camera mainCamera = null;
    static float time2D = 0;
    static float time3D = 0;
    static bool startedOnUI = false;

    static Raycaster()
    {
        if (Camera.main)
            mainCamera = Camera.main;
    }

    public static Vector3 Raycast()
    {
        return Raycast(Physics.DefaultRaycastLayers);
    }
    public static Vector3 Raycast(LayerMask mask)
    {
        return Raycast(mainCamera, mask);
    }
    public static Vector3 Raycast(Camera cam, LayerMask mask)
    {
        Vector3 position = ScreenPosition();

        if (position == Vector3.zero)
            return Vector3.zero;

        return Raycast(cam.ScreenPointToRay(position), mask);
    }

    public static Vector3 Raycast(Vector3 origin)
    {
        return Raycast(origin, mainCamera);
    }

    public static Vector3 Raycast(Vector3 origin, Camera cam)
    {
        return Raycast(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers);
    }

    public static Vector3 Raycast(Vector3 origin, Camera cam, LayerMask mask)
    {
        return Raycast(cam.ScreenPointToRay(origin), mask);
    }

    public static Vector3 Raycast(Vector3 origin, Vector3 direction)
    {
        return Raycast(origin, direction, Physics.DefaultRaycastLayers);
    }

    public static Vector3 Raycast(Vector3 origin, Vector3 direction, LayerMask mask)
    {
        return Raycast(new Ray(origin, direction), mask);
    }

    public static Vector3 Raycast(Ray ray, LayerMask mask)
    {

        if (Raycast(ray, out RaycastHit hit, mask))
        {
            return hit.point;
        }

        return Vector3.zero;
    }

    public static bool RaycastObject(out int num, params GameObject[] o)
    {
        return RaycastObject(Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastObject(Camera cam, out int num, params GameObject[] o)
    {
        return RaycastObject(cam, Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastObject(LayerMask mask, out int num, params GameObject[] o)
    {
        return RaycastObject(mainCamera, mask, out num, o);
    }

    public static bool RaycastObject(params GameObject[] o)
    {
        return RaycastObject(Physics.DefaultRaycastLayers, o);
    }

    public static bool RaycastObject(Camera cam, params GameObject[] o)
    {
        return RaycastObject(cam, Physics.DefaultRaycastLayers, o);
    }

    public static bool RaycastObject(LayerMask mask, params GameObject[] o)
    {
        return RaycastObject(mainCamera, mask, o);
    }

    public static bool RaycastObject(Camera cam, LayerMask mask, params GameObject[] o)
    {
        return RaycastObject(cam, mask, out int num, o);
    }

    public static bool RaycastObject(Vector3 origin, params GameObject[] o)
    {
        return RaycastObject(origin, mainCamera, o);
    }

    public static bool RaycastObject(Vector3 origin, Camera cam, params GameObject[] o)
    {
        return RaycastObject(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out int num, o);
    }

    public static bool RaycastObject(Vector3 origin, Camera cam, LayerMask mask, params GameObject[] o)
    {
        return RaycastObject(cam.ScreenPointToRay(origin), mask, out int num, o);
    }

    public static bool RaycastObject(Vector3 origin, Vector3 direction, params GameObject[] o)
    {
        return RaycastObject(origin, direction, Physics.DefaultRaycastLayers, o);
    }

    public static bool RaycastObject(Vector3 origin, Vector3 direction, LayerMask mask, params GameObject[] o)
    {
        return RaycastObject(new Ray(origin, direction), mask, out int num, o);
    }

    public static bool RaycastObject(Vector3 origin, out int num, params GameObject[] o)
    {
        return RaycastObject(origin, mainCamera, out num, o);
    }

    public static bool RaycastObject(Vector3 origin, Camera cam, out int num, params GameObject[] o)
    {
        return RaycastObject(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastObject(Vector3 origin, Camera cam, LayerMask mask, out int num, params GameObject[] o)
    {
        return RaycastObject(cam.ScreenPointToRay(origin), mask, out num, o);
    }

    public static bool RaycastObject(Vector3 origin, Vector3 direction, out int num, params GameObject[] o)
    {
        return RaycastObject(origin, direction, Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastObject(Vector3 origin, Vector3 direction, LayerMask mask, out int num, params GameObject[] o)
    {
        return RaycastObject(new Ray(origin, direction), mask, out num, o);
    }

    public static bool RaycastObject(Camera cam, LayerMask mask, out int num, params GameObject[] o)
    {
        num = -1;
        Vector3 position = ScreenPosition();
        if (position != Vector3.zero)
        {
            return RaycastObject(cam.ScreenPointToRay(position), mask, out num, o);
        }

        return false;
    }

    public static bool RaycastObject(Ray ray, LayerMask mask, out int num, params GameObject[] o)
    {
        num = -1;

        if (Raycast(ray, out RaycastHit hit, mask))
        {
            GameObject game = hit.collider.gameObject;
            for (int i = 0; i < o.Length; i++)
            {
                if (game == o[i])
                {
                    num = i;
                    return true;
                }
            }
        }


        return false;
    }


    public static bool RaycastCollider(out int num, params Collider[] o)
    {
        return RaycastCollider(Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastCollider(LayerMask mask, out int num, params Collider[] o)
    {
        return RaycastCollider(mainCamera, mask, out num, o);
    }

    public static bool RaycastCollider(params Collider[] o)
    {
        return RaycastCollider(Physics.DefaultRaycastLayers, o);
    }

    public static bool RaycastCollider(Camera cam, params Collider[] o)
    {
        return RaycastCollider(cam, Physics.DefaultRaycastLayers, o);
    }

    public static bool RaycastCollider(LayerMask mask, params Collider[] o)
    {
        return RaycastCollider(mainCamera, mask, o);
    }

    public static bool RaycastCollider(Camera cam, LayerMask mask, params Collider[] o)
    {
        return RaycastCollider(cam, mask, out int num, o);
    }

    public static bool RaycastCollider(Vector3 origin, params Collider[] o)
    {
        return RaycastCollider(origin, mainCamera, o);
    }

    public static bool RaycastCollider(Vector3 origin, Camera cam, params Collider[] o)
    {
        return RaycastCollider(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out int num, o);
    }

    public static bool RaycastCollider(Vector3 origin, Camera cam, LayerMask mask, params Collider[] o)
    {
        return RaycastCollider(cam.ScreenPointToRay(origin), mask, out int num, o);
    }

    public static bool RaycastCollider(Vector3 origin, Vector3 direction, params Collider[] o)
    {
        return RaycastCollider(origin, direction, Physics.DefaultRaycastLayers, o);
    }

    public static bool RaycastCollider(Vector3 origin, Vector3 direction, LayerMask mask, params Collider[] o)
    {
        return RaycastCollider(new Ray(origin, direction), mask, out int num, o);
    }

    public static bool RaycastCollider(Vector3 origin, out int num, params Collider[] o)
    {
        return RaycastCollider(origin, mainCamera, out num, o);
    }

    public static bool RaycastCollider(Vector3 origin, Camera cam, out int num, params Collider[] o)
    {
        return RaycastCollider(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastCollider(Vector3 origin, Camera cam, LayerMask mask, out int num, params Collider[] o)
    {
        return RaycastCollider(cam.ScreenPointToRay(origin), mask, out num, o);
    }

    public static bool RaycastCollider(Vector3 origin, Vector3 direction, out int num, params Collider[] o)
    {
        return RaycastCollider(origin, direction, Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastCollider(Vector3 origin, Vector3 direction, LayerMask mask, out int num, params Collider[] o)
    {
        return RaycastCollider(new Ray(origin, direction), mask, out num, o);
    }

    public static bool RaycastCollider(Camera cam, LayerMask mask, out int num, params Collider[] o)
    {
        num = -1;
        Vector3 position = ScreenPosition();
        if (position != Vector3.zero)
        {
            return RaycastCollider(cam.ScreenPointToRay(position), mask, out num, o);
        }

        return false;
    }

    public static bool RaycastCollider(Ray ray, LayerMask mask, out int num, params Collider[] o)
    {
        num = -1;

        if (Raycast(ray, out RaycastHit hit, mask))
        {
            Collider game = hit.collider;
            for (int i = 0; i < o.Length; i++)
            {
                if (game == o[i])
                {
                    num = i;
                    return true;
                }
            }
        }


        return false;
    }

    public static bool RaycastMono(out int num, params MonoBehaviour[] o)
    {
        return RaycastMono(Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastMono(LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        return RaycastMono(mainCamera, mask, out num, o);
    }

    public static bool RaycastMono(params MonoBehaviour[] o)
    {
        return RaycastMono(Physics.DefaultRaycastLayers, o);
    }

    public static bool RaycastMono(Camera cam, params MonoBehaviour[] o)
    {
        return RaycastMono(cam, Physics.DefaultRaycastLayers, o);
    }

    public static bool RaycastMono(LayerMask mask, params MonoBehaviour[] o)
    {
        return RaycastMono(mainCamera, mask, o);
    }

    public static bool RaycastMono(Camera cam, LayerMask mask, params MonoBehaviour[] o)
    {
        return RaycastMono(cam, mask, out int num, o);
    }

    public static bool RaycastMono(Vector3 origin, params MonoBehaviour[] o)
    {
        return RaycastMono(origin, mainCamera, o);
    }

    public static bool RaycastMono(Vector3 origin, Camera cam, params MonoBehaviour[] o)
    {
        return RaycastMono(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out int num, o);
    }

    public static bool RaycastMono(Vector3 origin, Camera cam, LayerMask mask, params MonoBehaviour[] o)
    {
        return RaycastMono(cam.ScreenPointToRay(origin), mask, out int num, o);
    }

    public static bool RaycastMono(Vector3 origin, Vector3 direction, params MonoBehaviour[] o)
    {
        return RaycastMono(origin, direction, Physics.DefaultRaycastLayers, o);
    }

    public static bool RaycastMono(Vector3 origin, Vector3 direction, LayerMask mask, params MonoBehaviour[] o)
    {
        return RaycastMono(new Ray(origin, direction), mask, out int num, o);
    }

    public static bool RaycastMono(Vector3 origin, out int num, params MonoBehaviour[] o)
    {
        return RaycastMono(origin, mainCamera, out num, o);
    }

    public static bool RaycastMono(Vector3 origin, Camera cam, out int num, params MonoBehaviour[] o)
    {
        return RaycastMono(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastMono(Vector3 origin, Camera cam, LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        return RaycastMono(cam.ScreenPointToRay(origin), mask, out num, o);
    }

    public static bool RaycastMono(Vector3 origin, Vector3 direction, out int num, params MonoBehaviour[] o)
    {
        return RaycastMono(origin, direction, Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool RaycastMono(Vector3 origin, Vector3 direction, LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        return RaycastMono(new Ray(origin, direction), mask, out num, o);
    }

    public static bool RaycastMono(Camera cam, LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        num = -1;
        Vector3 position = ScreenPosition();
        if (position != Vector3.zero)
        {
            return RaycastMono(cam.ScreenPointToRay(position), mask, out num, o);
        }

        return false;
    }

    public static bool RaycastMono(Ray ray, LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        num = -1;

        if (Raycast(ray, out RaycastHit hit, mask))
        {
            GameObject game = hit.collider.gameObject;

            for (int i = 0; i < o.Length; i++)
            {
                if (game == o[i].gameObject)
                {
                    num = i;
                    return true;
                }
            }
        }

        return false;
    }

    public static bool Raycast(Ray r, out RaycastHit hit, LayerMask mask)
    {
        if (!Physics.autoSimulation && !Mathf.Approximately(time3D, Time.time))
        {
            time3D = Time.time;
            Physics.Simulate(Time.fixedDeltaTime);
        }

        return Physics.Raycast(r, out hit, float.PositiveInfinity, mask);
    }

    public static Vector3 Raycast2D()
    {
        return Raycast2D(Physics.DefaultRaycastLayers);
    }
    public static Vector3 Raycast2D(LayerMask mask)
    {
        return Raycast2D(mainCamera, mask);
    }
    public static Vector3 Raycast2D(Camera cam, LayerMask mask)
    {
        Vector3 position = ScreenPosition();

        if (position == Vector3.zero)
            return Vector3.zero;

        return Raycast2D(cam.ScreenPointToRay(position), mask);
    }

    public static Vector3 Raycast2D(Vector3 origin)
    {
        return Raycast2D(origin, mainCamera);
    }

    public static Vector3 Raycast2D(Vector3 origin, Camera cam)
    {
        return Raycast2D(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers);
    }

    public static Vector3 Raycast2D(Vector3 origin, Camera cam, LayerMask mask)
    {
        return Raycast2D(cam.ScreenPointToRay(origin), mask);
    }

    public static Vector3 Raycast2D(Vector3 origin, Vector3 direction)
    {
        return Raycast2D(origin, direction, Physics.DefaultRaycastLayers);
    }

    public static Vector3 Raycast2D(Vector3 origin, Vector3 direction, LayerMask mask)
    {
        return Raycast2D(new Ray(origin, direction), mask);
    }

    public static Vector3 Raycast2D(Ray ray, LayerMask mask)
    {
        if (Raycast2D(ray, mask, out RaycastHit2D hit))
        {
            return hit.point;
        }

        return Vector3.zero;
    }

    public static bool Raycast2DObject(out int num, params GameObject[] o)
    {
        return Raycast2DObject(Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool Raycast2DObject(LayerMask mask, out int num, params GameObject[] o)
    {
        return Raycast2DObject(mainCamera, mask, out num, o);
    }

    public static bool Raycast2DObject(params GameObject[] o)
    {
        return Raycast2DObject(Physics.DefaultRaycastLayers, o);
    }

    public static bool Raycast2DObject(Camera cam, params GameObject[] o)
    {
        return Raycast2DObject(cam, Physics.DefaultRaycastLayers, o);
    }

    public static bool Raycast2DObject(LayerMask mask, params GameObject[] o)
    {
        return Raycast2DObject(mainCamera, mask, o);
    }

    public static bool Raycast2DObject(Camera cam, LayerMask mask, params GameObject[] o)
    {
        return Raycast2DObject(cam, mask, out int num, o);
    }

    public static bool Raycast2DObject(Vector3 origin, params GameObject[] o)
    {
        return Raycast2DObject(origin, mainCamera, o);
    }

    public static bool Raycast2DObject(Vector3 origin, Camera cam, params GameObject[] o)
    {
        return Raycast2DObject(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out int num, o);
    }

    public static bool Raycast2DObject(Vector3 origin, Camera cam, LayerMask mask, params GameObject[] o)
    {
        return Raycast2DObject(cam.ScreenPointToRay(origin), mask, out int num, o);
    }

    public static bool Raycast2DObject(Vector3 origin, Vector3 direction, params GameObject[] o)
    {
        return Raycast2DObject(origin, direction, Physics.DefaultRaycastLayers, o);
    }

    public static bool Raycast2DObject(Vector3 origin, Vector3 direction, LayerMask mask, params GameObject[] o)
    {
        return Raycast2DObject(new Ray(origin, direction), mask, out int num, o);
    }

    public static bool Raycast2DObject(Vector3 origin, out int num, params GameObject[] o)
    {
        return Raycast2DObject(origin, mainCamera, out num, o);
    }

    public static bool Raycast2DObject(Vector3 origin, Camera cam, out int num, params GameObject[] o)
    {
        return Raycast2DObject(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool Raycast2DObject(Vector3 origin, Camera cam, LayerMask mask, out int num, params GameObject[] o)
    {
        return Raycast2DObject(cam.ScreenPointToRay(origin), mask, out num, o);
    }

    public static bool Raycast2DObject(Vector3 origin, Vector3 direction, out int num, params GameObject[] o)
    {
        return Raycast2DObject(origin, direction, Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool Raycast2DObject(Vector3 origin, Vector3 direction, LayerMask mask, out int num, params GameObject[] o)
    {
        return Raycast2DObject(new Ray(origin, direction), mask, out num, o);
    }

    public static bool Raycast2DObject(Camera cam, LayerMask mask, out int num, params GameObject[] o)
    {
        Vector3 position = ScreenPosition();
        return Raycast2DObject(cam.ScreenPointToRay(position), mask, out num, o);
    }

    public static bool Raycast2DObject(Ray ray, LayerMask mask, out int num, params GameObject[] o)
    {
        num = -1;

        if (Raycast2D(ray, out RaycastHit2D hit))
        {
            GameObject game = hit.collider.gameObject;
            for (int i = 0; i < o.Length; i++)
            {
                if (game == o[i])
                {
                    num = i;
                    return true;
                }
            }
        }

        return false;
    }


    public static bool Raycast2DMono(out int num, params MonoBehaviour[] o)
    {
        return Raycast2DMono(Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool Raycast2DMono(LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        return Raycast2DMono(mainCamera, mask, out num, o);
    }
    public static bool Raycast2DMono(Camera cam, out int num, params MonoBehaviour[] o)
    {
        return Raycast2DMono(cam, Physics.DefaultRaycastLayers, out num, o);
    }


    public static bool Raycast2DMono(params MonoBehaviour[] o)
    {
        return Raycast2DMono(Physics.DefaultRaycastLayers, o);
    }

    public static bool Raycast2DMono(Camera cam, params MonoBehaviour[] o)
    {
        return Raycast2DMono(cam, Physics.DefaultRaycastLayers, o);
    }

    public static bool Raycast2DMono(LayerMask mask, params MonoBehaviour[] o)
    {
        return Raycast2DMono(mainCamera, mask, o);
    }

    public static bool Raycast2DMono(Camera cam, LayerMask mask, params MonoBehaviour[] o)
    {
        return Raycast2DMono(cam, mask, out int num, o);
    }

    public static bool Raycast2DMono(Vector3 origin, params MonoBehaviour[] o)
    {
        return Raycast2DMono(origin, mainCamera, o);
    }

    public static bool Raycast2DMono(Vector3 origin, Camera cam, params MonoBehaviour[] o)
    {
        return Raycast2DMono(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out int num, o);
    }

    public static bool Raycast2DMono(Vector3 origin, Camera cam, LayerMask mask, params MonoBehaviour[] o)
    {
        return Raycast2DMono(cam.ScreenPointToRay(origin), mask, out int num, o);
    }

    public static bool Raycast2DMono(Vector3 origin, Vector3 direction, params MonoBehaviour[] o)
    {
        return Raycast2DMono(origin, direction, Physics.DefaultRaycastLayers, o);
    }

    public static bool Raycast2DMono(Vector3 origin, Vector3 direction, LayerMask mask, params MonoBehaviour[] o)
    {
        return Raycast2DMono(new Ray(origin, direction), mask, out int num, o);
    }

    public static bool Raycast2DMono(Vector3 origin, out int num, params MonoBehaviour[] o)
    {
        return Raycast2DMono(origin, mainCamera, out num, o);
    }

    public static bool Raycast2DMono(Vector3 origin, Camera cam, out int num, params MonoBehaviour[] o)
    {
        return Raycast2DMono(cam.ScreenPointToRay(origin), Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool Raycast2DMono(Vector3 origin, Camera cam, LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        return Raycast2DMono(cam.ScreenPointToRay(origin), mask, out num, o);
    }

    public static bool Raycast2DMono(Vector3 origin, Vector3 direction, out int num, params MonoBehaviour[] o)
    {
        return Raycast2DMono(origin, direction, Physics.DefaultRaycastLayers, out num, o);
    }

    public static bool Raycast2DMono(Vector3 origin, Vector3 direction, LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        return Raycast2DMono(new Ray(origin, direction), mask, out num, o);
    }

    public static bool Raycast2DMono(Camera cam, LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        Vector3 position = ScreenPosition();

        if (position == Vector3.zero)
        {
            num = -1;
            return false;
        }
        position.z = cam.transform.position.z * -1;

        return Raycast2DMono(new Ray(cam.ScreenToWorldPoint(position), Vector3.forward), mask, out num, o);
    }

    public static bool Raycast2DMono(Ray ray, LayerMask mask, out int num, params MonoBehaviour[] o)
    {
        num = -1;

        if (Raycast2D(ray, out RaycastHit2D hit))
        {
            GameObject game = hit.collider.gameObject;
            for (int i = 0; i < o.Length; i++)
            {
                if (game == o[i].gameObject)
                {
                    num = i;
                    return true;
                }
            }
        }

        return false;
    }

    public static bool Raycast2D(Ray ray, out RaycastHit2D hit)
    {
        return Raycast2D(ray, Physics2D.DefaultRaycastLayers, out hit);
    }

    public static bool Raycast2D(Ray ray, LayerMask mask, out RaycastHit2D hit)
    {
        if (!Physics2D.autoSimulation && !Mathf.Approximately(time2D, Time.time))
        {
            time2D = Time.time;
            Physics2D.Simulate(Time.fixedDeltaTime);
        }

        hit = Physics2D.Raycast(ray.origin, ray.direction, float.PositiveInfinity, mask);
        return hit.collider != null;
    }


    public static Vector3 ScreenPosition()
    {
        Vector3 position = Vector3.zero;

        if (PointerIsDown())
        {
            startedOnUI = IsPointerOverUIElement();
        }

        if (Time.time == 0 || IsPointerOverUIElement() || startedOnUI)
            return position;

        if (Application.isEditor)
        {
            if (Input.GetMouseButtonUp(0))
            {
                position = Input.mousePosition;
            }
        }
        else
        {
            if (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended)
                position = Input.touches[0].position;
        }

        return position;
    }

    public static bool IsPointerOverUIElement()
    {
        var eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);
        return results.Count > 0;
    }

    public static bool PointerIsUp()
    {
        if (Application.isEditor)
        {
            return Input.GetMouseButtonUp(0);
        }
        else
        {
            return (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended);
        }
    }

    public static bool PointerIsHeld()
    {
        if (Application.isEditor)
        {
            return Input.GetMouseButton(0);
        }
        else
        {
            return Input.touches.Length > 0 &&
            (Input.touches[0].phase == TouchPhase.Moved ||
            Input.touches[0].phase == TouchPhase.Stationary);
        }
    }

    public static bool PointerIsDown()
    {

        if (Application.isEditor)
        {
            return Input.GetMouseButtonDown(0);
        }
        else
        {
            return (Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Began);
        }
    }
}
