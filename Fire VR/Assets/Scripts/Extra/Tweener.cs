﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Tweener : MonoBehaviour
{
    #region Enums
    public enum Mode
    {
        Position,
        Rotation,
        Scale,
        Color,
        RandomShake,
        Transform
    }
    public enum Loop
    {
        Restart,
        PingPong
    }
    enum Action
    {
        Play,
        Restart,
        RestartPlay,
        RestartBackward,
        RestartBackwardPlay,
        Stop
    }
    public enum ActionTime
    {
        Update,
        LateUpdate,
        FixedUpdate
    }
    #endregion

    #region Variables

    /// <summary>
    /// In which update should the animation be played.
    /// </summary>
    [Tooltip("In which update should the animation be played.")]
    public ActionTime actionTime = ActionTime.Update;

    /// <summary>
    /// Actions used in base methods.
    /// </summary>
    [Tooltip("Actions used in base methods.")]
    UnityEvent[] actions = new UnityEvent[] { new UnityEvent(), new UnityEvent(), new UnityEvent(), new UnityEvent(), new UnityEvent(), new UnityEvent() };

    /// <summary>
    /// Animation mode you want to use.
    /// </summary>
    [Tooltip("Animation mode you want to use.")]
    public Mode mode = Mode.Position;

    /// <summary>
    /// Loop mode you want to use.
    /// </summary>
    [Tooltip("Loop mode you want to use.")]
    public Loop loopType = Loop.Restart;

    /// <summary>
    /// Should the tweener play when enabled.
    /// </summary>
    [Tooltip("Should the tweener play when enabled.")]
    public bool autoPlay = false;

    /// <summary>
    /// Should the tweener restart when enabled.
    /// </summary>
    [Tooltip("Should the tweener restart when enabled.")]
    public bool autoRestart = false;

    /// <summary>
    /// Should the tweener play the next tweener on the object.
    /// </summary>
    [Tooltip("Should the tweener play the next tweener on the object.")]
    public bool playNext = false;

    /// <summary>
    /// Should the tweener restart the next tweener on the object.
    /// </summary>
    [Tooltip("Should the tweener restart the next tweener on the object.")]
    public bool restartNext = false;

    /// <summary>
    /// Duration of the tweener.
    /// </summary>
    [Tooltip("Duration of the tweener.")]
    public float duration = 1;

    /// <summary>
    /// Delay of the tweener.
    /// </summary>
    [Tooltip("Delay of the tweener.")]
    public float delay = 0;

    /// <summary>
    /// Does the tweener ignore timescale.
    /// </summary>
    [Tooltip("Does the tweener ignore timescale.")]
    public bool ignoreTimeScale = false;

    /// <summary>
    /// Curve how the animation plays.
    /// </summary>
    [Tooltip("Curve how the animation plays.")]
    public AnimationCurve curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

    /// <summary>
    /// How many loops the tweener does before stopping.
    /// </summary>
    [Tooltip("How many loops the tweener does before stopping.")]
    public int loops = 0;

    /// <summary>
    /// Will the tweener loop endlessly.
    /// </summary>
    [Tooltip("Will the tweener loop endlessly.")]
    public bool infiniteLoops = false;

    /// <summary>
    /// Id of the tweener.
    /// </summary>
    [Tooltip("Id of the tweener.")]
    public int id = 0;

    /// <summary>
    /// Priority id of the tweener. Prioritys with the same id affects each other.
    /// </summary>
    [Tooltip("Priority id of the tweener. Prioritys with the same id affects each other.")]
    public int priorityId = 0;

    /// <summary>
    /// Priority of the tweener.
    /// </summary>
    [Tooltip("Priority of the tweener.")]
    public int priority = 0;

    /// <summary>
    /// Should the tweener be stopped by other, higher priority tweener.
    /// </summary>
    [Tooltip("Should the tweener be stopped by other, higher priority tweener.")]
    public bool stopByHigher = false;

    #region StartValues
    /// <summary>
    /// Should tweener use assigned position or its own position as starting point.
    /// </summary>
    [Tooltip("Should tweener use assigned position or its own position as starting point.")]
    public bool useStart = false;

    /// <summary>
    /// Should tweener use assigned position or its own position as starting point.
    /// </summary>
    [Tooltip("Should tweener use more accurate Quaternions to calculate rotation.")]
    public bool useQuaternion = false;

    /// <summary>
    /// Starting position of the tweener.
    /// </summary>
    [Tooltip("Starting position of the tweener.")]
    public Vector3 startPos = Vector3.zero;

    /// <summary>
    /// Starting rotation of the tweener.
    /// </summary>
    [Tooltip("Starting rotation of the tweener.")]
    public Vector3 startRot = Vector3.zero;

    /// <summary>
    /// Starting rotation of the tweener.
    /// </summary>
    [Tooltip("Starting rotation of the tweener in quaternions.")]
    public Quaternion startRotQ;

    /// <summary>
    /// Starting scale of the tweener.
    /// </summary>
    [Tooltip("Starting scale of the tweener.")]
    public Vector3 startScale = Vector3.one;

    /// <summary>
    /// Starting color of the tweener.
    /// </summary>
    [Tooltip("Starting color of the tweener.")]
    public Color startColor = Color.white;

    /// <summary>
    /// Transform which the starting values are taken from.
    /// </summary>
    [Tooltip("Transform which the starting values are taken from.")]
    public Transform startTran = null;

    /// <summary>
    /// Starting position where the shake moves the object a little.
    /// </summary>
    [Tooltip("Starting position where the shake moves the object a little.")]
    public Vector3 startShake = Vector3.zero;
    #endregion

    #region EndValues
    /// <summary>
    /// Ending position of the tweener.
    /// </summary>
    [Tooltip("Ending position of the tweener.")]
    public Vector3 endPos = Vector3.zero;

    /// <summary>
    /// Ending rotation of the tweener.
    /// </summary>
    [Tooltip("Ending rotation of the tweener.")]
    public Vector3 endRot = Vector3.zero;

    /// <summary>
    /// Ending rotation of the tweener.
    /// </summary>
    [Tooltip("Ending rotation of the tweener in quaternion.")]
    public Quaternion endRotQ;

    /// <summary>
    /// Ending scale of the tweener.
    /// </summary>
    [Tooltip("Ending scale of the tweener.")]
    public Vector3 endScale = Vector3.one;

    /// <summary>
    /// Ending color of the tweener.
    /// </summary>
    [Tooltip("Ending color of the tweener.")]
    public Color endColor = Color.white;

    /// <summary>
    /// Transform which the ending values are taken from.
    /// </summary>
    [Tooltip("Transform which the ending values are taken from.")]
    public Transform endTran = null;

    /// <summary>
    /// How far from the initial position will the shake go.
    /// </summary>
    [Tooltip("How far from the initial position will the shake go.")]
    public float endShake = 0;
    #endregion

    #region FinalCalculatedValues

    /// <summary>
    /// Calculated, final start position.
    /// </summary>
    [Tooltip("Calculated, final start position.")]
    public Vector3 finalStartPos = Vector3.zero;

    /// <summary>
    /// Calculated, final end rotation.
    /// </summary>
    [Tooltip("Calculated, final start rotation.")]
    public Vector3 finalStartRot;

    /// <summary>
    /// Calculated, final end rotation.
    /// </summary>
    [Tooltip("Calculated, final start rotation.")]
    public Quaternion finalStartRotQ;

    /// <summary>
    /// Calculated, final start scale.
    /// </summary>
    [Tooltip("Calculated, final start scale.")]
    public Vector3 finalStartScale = Vector3.one;

    /// <summary>
    /// Calculated, final start color.
    /// </summary>
    [Tooltip("Calculated, final start color.")]
    public Color finalStartColor = Color.white;

    /// <summary>
    /// Calculated, final end position.
    /// </summary>
    [Tooltip("Calculated, final end position.")]
    public Vector3 finalEndPos = Vector3.zero;

    /// <summary>
    /// Calculated, final end rotation.
    /// </summary>
    [Tooltip("Calculated, final end rotation.")]
    public Vector3 finalEndRot;

    /// <summary>
    /// Calculated, final end rotation.
    /// </summary>
    [Tooltip("Calculated, final end rotation.")]
    public Quaternion finalEndRotQ;

    /// <summary>
    /// Calculated, final end scale.
    /// </summary>
    [Tooltip("Calculated, final end scale.")]
    public Vector3 finalEndScale = Vector3.one;

    /// <summary>
    /// Calculated, final end color.
    /// </summary>
    [Tooltip("Calculated, final end color.")]
    public Color finalEndColor = Color.white;

    #endregion

    /// <summary>
    /// Does the tweener use transform to get starting and ending values.
    /// </summary>
    [Tooltip("Does the tweener use transform to get starting and ending values.")]
    public bool useTran = false;

    /// <summary>
    /// Is the given value ending or starting point.
    /// </summary>
    [Tooltip("Is the given value ending or starting point.")]
    public bool from = false;

    /// <summary>
    /// Does the tweener animation happen in local space.
    /// </summary>
    [Tooltip("Does the tweener animation happen in local space.")]
    public bool localSpace = false;

    /// <summary>
    /// Is the given end value final, or relative to the starting value.
    /// </summary>
    [Tooltip("Is the given end value final, or relative to the starting value.")]
    public bool relative = false;

    #region ActionButtons

    /// <summary>
    /// Will anything happen when tweener starts playing.
    /// </summary>
    [Tooltip("Will anything happen when tweener starts playing.")]
    public bool useOnPlay = false;

    /// <summary>
    /// Will anything happen when tweener completes itself.
    /// </summary>
    [Tooltip("Will anything happen when tweener completes itself.")]
    public bool useOnComplete = false;

    /// <summary>
    /// Will anything happen when tweener starts playing backwards.
    /// </summary>
    [Tooltip("Will anything happen when tweener starts playing backwards.")]
    public bool useOnRewind = false;

    /// <summary>
    /// Will anything happen when tweener completes itself backwards.
    /// </summary>
    [Tooltip("Will anything happen when tweener completes itself backwards.")]
    public bool useOnRewindComplete = false;

    /// <summary>
    /// Will anything happen when tweener starts playing in any way.
    /// </summary>
    [Tooltip("Will anything happen when tweener starts playing in any way.")]
    public bool useOnAnyPlay = false;

    /// <summary>
    /// Will anything happen when tweener completes itself in any way.
    /// </summary>
    [Tooltip("Will anything happen when tweener completes itself in any way.")]
    public bool useOnAnyComplete = false;


    /// <summary>
    /// What methods will happen when the tweener starts playing.
    /// </summary>
    [Tooltip("What methods will happen when the tweener starts playing.")]
    public UnityEvent onPlay = null;

    /// <summary>
    /// What methods will happen when the tweener completes itself.
    /// </summary>
    [Tooltip("What methods will happen when the tweener completes itself.")]
    public UnityEvent onComplete = null;

    /// <summary>
    /// What methods will happen when the tweener starts playing backwards.
    /// </summary>
    [Tooltip("What methods will happen when the tweener starts playing backwards.")]
    public UnityEvent onRewind = null;

    /// <summary>
    /// What methods will happen when the tweemer completes itself backwards.
    /// </summary>
    [Tooltip("What methods will happen when the tweemer completes itself backwards.")]
    public UnityEvent onRewindComplete = null;

    /// <summary>
    /// What methods will happen when the tweener starts playing.
    /// </summary>
    [Tooltip("What methods will happen when the tweener starts playing in any way.")]
    public UnityEvent onAnyPlay = null;

    /// <summary>
    /// What methods will happen when the tweener completes itself.
    /// </summary>
    [Tooltip("What methods will happen when the tweener completes itself in any way.")]
    public UnityEvent onAnyComplete = null;

    #endregion

    /// <summary>
    /// How much is the speed multiplied.
    /// </summary>
    [Tooltip("How much is the speed multiplied.")]
    public float speedMultiplier = 1;

    /// <summary>
    /// Will physics affect tweener.
    /// </summary>
    [Tooltip("Will physics affect tweener.")]
    public bool usePhysics = false;

    /// <summary>
    /// What physics layers affects the physics.
    /// </summary>
    [Tooltip("What physics layers affects the physics")]
    public LayerMask physicsLayers = ~0;

    #region Ignores
    /// <summary>
    /// Which values should be ignored on value change.
    /// </summary>
    [Tooltip("Which values should be ignored on value change.")]
    public bool[] ignorePositions = new bool[3];
    /// <summary>
    /// Which values should be ignored on value change.
    /// </summary>
    [Tooltip("Which values should be ignored on value change.")]
    public bool[] ignoreRotations = new bool[3];
    /// <summary>
    /// Which values should be ignored on value change.
    /// </summary>
    [Tooltip("Which values should be ignored on value change.")]
    public bool[] ignoreScales = new bool[3];
    /// <summary>
    /// Which values should be ignored on value change.
    /// </summary>
    [Tooltip("Which values should be ignored on value change.")]
    public bool[] ignoreColors = new bool[4];
    /// <summary>
    /// Which values should be ignored on value change.
    /// </summary>
    [Tooltip("Which values shoud be ignore on value change.")]
    public bool[] ignoreShakes = new bool[3];
    /// <summary>
    /// Which values should be ignored on value change.
    /// </summary>
    [Tooltip("Which values shoud be ignore on value change.")]
    public bool[] ignoreTransforms = new bool[3];
    #endregion


    /// <summary>
    /// Is the tweener currently on.
    /// </summary>
    [Tooltip("Is the tweener currently on.")]
    public bool on = false;

    /// <summary>
    /// Is the tweener used on ui.
    /// </summary>
    [Tooltip("Is the tweener used on ui.")]
    private bool ui = false;

    /// <summary>
    /// Is the object textmeshpro.
    /// </summary>
    [Tooltip("Is the object textmeshpro.")]
    private bool textMeshUGUI = false;

    /// <summary>
    /// Is the tweener used on sprite.
    /// </summary>
    [Tooltip("Is the tweener used on sprite.")]
    private bool sprite = false;

    /// <summary>
    /// All materials the tweener is going to affect.
    /// </summary>
    [Tooltip("All materials the tweener is going to affect.")]
    private Material[] targetRend = null;

    /// <summary>
    /// The ui renderer the tweener is going to affect.
    /// </summary>
    [Tooltip("The ui renderer the tweener is going to affect.")]
    private Graphic uiTargetRend = null;

    /// <summary>
    /// The sprite renderer the tweener is going to affect.
    /// </summary>
    [Tooltip("The sprite renderer the tweener is going to affect.")]
    private SpriteRenderer spriteTargetRend = null;

    /// <summary>
    /// The TextMesh renderer the tweener is going to affect.
    /// </summary>
    [Tooltip("The TextMesh renderer the tweener is going to affect.")]
    private TextMesh textMeshTargetRend = null;

    /// <summary>
    /// Is the tweener playing forward.
    /// </summary>
    [Tooltip("Is the tweener playing forward.")]
    private bool forward = true;

    /// <summary>
    /// How many loops the tweener has finished.
    /// </summary>
    [Tooltip("How many loops the tweener has finished.")]
    private int currentLoop = 0;

    /// <summary>
    /// Does the tweener use character controller.
    /// </summary>
    [Tooltip("Does the tweener use character controller.")]
    private CharacterController controller = null;


    /// <summary>
    /// Timer calculating delay of the tween.
    /// </summary>
    [Tooltip("Timer calculating delay of the tween.")]
    private Timer delayer = null;

    /// <summary>
    /// Timer calculating the time of the tween.
    /// </summary>
    [Tooltip("Timer calculating the time of the tween.")]
    private Timer timer = null;


    /// <summary>
    /// List of all tweeners on the scene.
    /// </summary>
    [Tooltip("List of all tweeners on the scene.")]
    public static List<Tweener> allTweens;

    /// <summary>
    /// List of all tweeners on the same gameObject.
    /// </summary>
    [Tooltip("List of all tweeners on the scene.")]
    public List<Tweener> allObjectTweens;
    #endregion

    /// <summary>
    /// Bool to tell if all the needed things have already been assigned.
    ///</summary>
    bool assigned = false;

    /// <summary>
    /// If code recognizes the object as UI, assigns and uses recttrasform.
    ///</summary>
    RectTransform rectTransform = null;

    ///<summary>
    ///Is the tweeners timer done.
    ///</summary>
    bool TDone = false;

    ///<summary>
    ///Is the tweeners delay timer done.
    ///</summary>
    bool DDone = false;

    Vector3 actualPosition
    {
        get
        {
            if (ui)
                return rectTransform.anchoredPosition3D;
            else
            {
                if (localSpace)
                    return transform.localPosition;
                else
                    return transform.position;
            }
        }
        set
        {
            if (ui)
                rectTransform.anchoredPosition3D = value;
            else
            {
                if (localSpace)
                    transform.localPosition = value;
                else
                    transform.position = value;
            }
        }
    }
    private void Assign()
    {
        if (assigned)
            return;

        assigned = true;
        actions[0].AddListener(Play);
        actions[1].AddListener(Restart);
        actions[2].AddListener(RestartPlay);
        actions[3].AddListener(RestartBackward);
        actions[4].AddListener(RestartBackwardPlay);
        actions[5].AddListener(Stop);

        if (GetComponentInParent<Canvas>() && GetComponent<RectTransform>())
        {
            rectTransform = GetComponent<RectTransform>();
            ui = true;
        }

        CountObjectTweens(false);

        if (allTweens == null)
        {
            allTweens = new List<Tweener>();
        }

        if (!allTweens.Contains(this))
        {
            allTweens.Add(this);
        }

        if (gameObject.layer == LayerMask.GetMask("UI") || GetComponentInParent<Canvas>())
        {
            ui = true;

            if (gameObject.GetComponent<TextMesh>())
            {
                textMeshUGUI = true;
            }
        }
        else if (GetComponent<SpriteRenderer>())
        {
            sprite = true;
        }

        delayer = new Timer(delay, 1, !ignoreTimeScale, false);
        timer = new Timer(duration, 1, !ignoreTimeScale, false);
    }

    /// <summary>
    /// Reset, happens when the tweener is created. Assings all the inital values for the editor.
    /// </summary>
    private void Reset()
    {
        if (allTweens == null)
        {
            allTweens = new List<Tweener>();
        }

        if (!allTweens.Contains(this))
        {
            allTweens.Add(this);
        }

        for (int i = 0; i < allTweens.Count; i++)
        {
            if (allTweens[i] != this && id == allTweens[i].id)
            {
                id++;
                i = -1;
            }
        }

        CountObjectTweens(true);
    }

    /// <summary>
    /// Counts and saves list of all the tweeners in the same object.
    /// </summary>
    /// <param name="newTween">Boolean telling if the this tweener is new, and if the method should call other tweeners CounObjectTweens-method.</param>
    public void CountObjectTweens(bool newTween)
    {
        allObjectTweens = new List<Tweener>();
        foreach (Tweener t in GetComponents<Tweener>())
        {
            if (t != this)
            {
                allObjectTweens.Add(t);
                if (newTween)
                {
                    t.CountObjectTweens(false);
                }
            }
        }
    }

    /// <summary>
    /// Awake, happens when scene starts. Assings all the initial values.
    /// </summary>
    private void Awake()
    {
        Assign();
    }

    /// <summary>
    /// Happens when gameObject is enabled. If autoplay is on, the tweener is played right away when enabled.
    /// </summary>
    private void OnEnable()
    {
        Assign();
        if (autoRestart)
        {
            Restart(forward);
        }
        if (autoPlay)
        {
            StartCoroutine(StartAfterSceneLoad());
        }
    }

    IEnumerator StartAfterSceneLoad()
    {
        yield return null;
        Play();
    }

    #region Animations

    /// <summary>
    /// Update-method that actually keeps the tweener going.
    /// </summary>
    private void Update()
    {
        if (actionTime == ActionTime.Update)
        {
            Act();
        }
    }

    /// <summary>
    /// KateUpdate-method that actually keeps the tweener going.
    /// </summary>
    private void LateUpdate()
    {
        if (actionTime == ActionTime.LateUpdate)
        {
            Act();
        }
    }

    /// <summary>
    /// FixedUpdate-method that actually keeps the tweener going.
    /// </summary>
    private void FixedUpdate()
    {
        if (actionTime == ActionTime.FixedUpdate)
        {
            Act();
        }
    }

    private void Act()
    {
        if (on)
        {
            timer.speed = speedMultiplier;
            delayer.speed = speedMultiplier;
            if (!DDone)
            {
                if (delayer.NextBool())
                {
                    timer.ChangeTime(forward != from, delayer.overFloat);
                    DDone = true;
                }
            }
            else if (!TDone)
            {
                if (timer.NextBool(forward != from))
                {
                    TDone = true;
                }

                Animate(curve.Evaluate(timer.GetFloat()));

                if (TDone)
                {
                    if (currentLoop < loops || infiniteLoops)
                    {
                        DDone = false;
                        TDone = false;
                        Looper();
                    }
                    else
                    {
                        StartCoroutine(Finished());
                    }
                }
            }
        }
    }

    IEnumerator Finished()
    {
        yield return null;


        on = false;

        if (restartNext)
        {
            allObjectTweens[allObjectTweens.IndexOf(this) + 1].Restart(forward);
        }
        if (playNext)
        {
            allObjectTweens[allObjectTweens.IndexOf(this) + 1].Play();
        }

        if (useOnAnyComplete)
            onAnyComplete?.Invoke();

        if (forward)
        {
            if (useOnComplete)
            {
                onComplete?.Invoke();
            }
        }
        else
        {
            if (useOnRewindComplete)
            {
                onRewindComplete?.Invoke();
            }
        }
    }

    /// <summary>
    /// Animates the gameObjects wanted way.
    /// </summary>
    /// <param name="useFloat">Value between 0 and 1 that tells at which point of animation we are.</param>
    void Animate(float useFloat)
    {
        switch (mode)
        {
            case Mode.Color:
                AnimateColor(useFloat);
                break;

            case Mode.Position:
                AnimatePosition(useFloat, ignorePositions);
                break;

            case Mode.Rotation:
                AnimateRotation(useFloat, ignoreRotations);
                break;

            case Mode.Scale:
                AnimateScale(useFloat, ignoreScales);
                break;

            case Mode.RandomShake:
                AnimateRandomShake(useFloat);
                break;

            case Mode.Transform:
                AnimateTransform(useFloat);
                break;
        }
    }

    /// <summary>
    /// Animates the gameObjects color in the wanted way.
    /// </summary>
    /// <param name="useFloat">Value between 0 and 1 that tells at which point of animation we are.</param>
    public void AnimateColor(float useFloat)
    {
        Color c = Color.LerpUnclamped(startColor, finalEndColor, useFloat);


        if (ui)
        {
            if (textMeshUGUI)
            {
                textMeshTargetRend.color = IgnoreColors(c, textMeshTargetRend.color);
            }
            else
            {
                uiTargetRend.color = IgnoreColors(c, uiTargetRend.color);
            }
        }
        else if (sprite)
        {
            spriteTargetRend.color = IgnoreColors(c, spriteTargetRend.color);
        }
        else
        {
            foreach (Material m in targetRend)
            {
                m.color = IgnoreColors(c, m.color); ;
            }
        }
    }

    /// <summary>
    /// Animates the gameObjects position in the wanted way.
    /// </summary>
    /// <param name="useFloat">Value between 0 and 1 that tells at which point of animation we are.</param>
    public void AnimatePosition(float useFloat, bool[] ignores)
    {
        Vector3 newPos = Vector3.LerpUnclamped(finalStartPos, finalEndPos, useFloat);

        for (int i = 0; i < ignores.Length; i++)
        {
            newPos[i] = ignores[i] ? actualPosition[i] : newPos[i];
        }

        if (usePhysics)
        {
            Vector3 direction = (newPos - actualPosition).normalized;
            if (Physics.Raycast(actualPosition, direction, out RaycastHit hit, Vector3.Distance(newPos, actualPosition), physicsLayers))
            {
                actualPosition = hit.point - direction * 0.4f;
                finalEndPos -= newPos - actualPosition;
                return;
            }
        }

        if (controller)
        {
            Vector3 mover = newPos - actualPosition;
            if (Mathf.Approximately(finalStartPos.y, finalEndPos.y))
            {
                mover += Vector3.down;
            }
            controller.Move(mover);
        }
        else
        {
            actualPosition = newPos;
        }
    }

    /// <summary>
    /// Animates the gameObjects rotation in the wanted way.
    /// </summary>
    /// <param name="useFloat">Value between 0 and 1 that tells at which point of animation we are.</param>
    public void AnimateRotation(float useFloat, bool[] ignores)
    {
        Vector3 newRot = Vector3.LerpUnclamped(finalStartRot, finalEndRot, useFloat);

        if (useQuaternion)
            newRot = Quaternion.LerpUnclamped(finalStartRotQ, finalEndRotQ, useFloat).eulerAngles;

        for (int i = 0; i < ignores.Length; i++)
        {
            newRot[i] = ignores[i] ?
                (localSpace ? transform.localEulerAngles[i] : transform.eulerAngles[i]) :
                newRot[i];
        }

        if (localSpace)
        {
            if (useQuaternion)
                transform.localRotation = Quaternion.Euler(newRot);
            else
                transform.localEulerAngles = newRot;
        }
        else
        {
            if (useQuaternion)
                transform.rotation = Quaternion.Euler(newRot);
            else
                transform.eulerAngles = newRot;
        }
    }

    /// <summary>
    /// Animates the gameObjects scale in the wanted way.
    /// </summary>
    /// <param name="useFloat">Value between 0 and 1 that tells at which point of animation we are.</param>
    public void AnimateScale(float useFloat, bool[] ignores)
    {
        Vector3 newScale = Vector3.LerpUnclamped(finalStartScale, finalEndScale, useFloat);
        for (int i = 0; i < ignores.Length; i++)
        {
            newScale[i] = ignores[i] ? (localSpace ? transform.localScale[i] : transform.lossyScale[i]) : newScale[i];
        }

        if (localSpace)
        {
            transform.localScale = newScale;
        }
        else
        {
            SetGlobalScale(newScale);
        }
    }

    /// <summary>
    /// Animates the gameObjects shake in the wanted way.
    /// </summary>
    /// <param name="useFloat">Value between 0 and 1 that tells at which point of animation we are.</param>
    public void AnimateRandomShake(float useFloat)
    {
        actualPosition = startShake;
        if (useFloat != 1 && useFloat != 0)
        {
            actualPosition += new Vector3(
                ignoreShakes[0] ? 0 : Random.Range(-endShake, endShake),
                ignoreShakes[1] ? 0 : Random.Range(-endShake, endShake),
                ignoreShakes[2] ? 0 : Random.Range(-endShake, endShake)
                );
        }
    }

    /// <summary>
    /// Animates the gameObjects transform in the wanted way.
    /// </summary>
    /// <param name="useFloat">Value between 0 and 1 that tells at which point of animation we are.</param>
    public void AnimateTransform(float useFloat)
    {
        if (!ignoreTransforms[0])
            AnimatePosition(useFloat, new bool[3]);
        if (!ignoreTransforms[1])
            AnimateRotation(useFloat, new bool[3]);
        if (!ignoreTransforms[2])
            AnimateScale(useFloat, new bool[3]);
    }

    Color IgnoreColors(Color returnedColor, Color baseColor)
    {
        for (int i = 0; i < ignoreColors.Length; i++)
        {
            returnedColor[i] = ignoreColors[i] ? baseColor[i] : returnedColor[i];
        }
        return returnedColor;
    }

    /// <summary>
    /// Counts how many time we have looped so far, and if we should keep on loopin.
    /// </summary>
    void Looper()
    {
        currentLoop++;
        switch (loopType)
        {
            case Loop.PingPong:
                forward = !forward;
                timer.RestartWithOver(forward != from);
                break;

            case Loop.Restart:
                timer.RestartWithOver(forward != from);
                break;
        }
    }

    /// <summary>
    /// Method to set global scale of the object.
    /// </summary>
    /// <param name="globalScale">Scale we want the object to be.</param>
    public void SetGlobalScale(Vector3 globalScale)
    {
        transform.localScale = Vector3.one;
        Vector3 newScale = Vector3.zero;
        for (int i = 0; i < 3; i++)
        {
            newScale[i] = globalScale[i] / transform.lossyScale[i];
        }

        transform.localScale = newScale;
    }
    #endregion

    #region BaseMethods

    /// <summary>
    /// Base action, that invokes the given action.
    /// </summary>
    /// <param name="ac">Enum value of the given action.</param>
    void Act(Action ac)
    {
        actions[(int)ac].Invoke();
    }

    /// <summary>
    /// Base method for doing something with all the tweeners in gameObject.
    /// </summary>
    /// <param name="ac">Enum value of the given action.</param>
    private void BaseAllInObject(Action ac)
    {
        foreach (Tweener tween in GetComponents<Tweener>())
        {
            if (tween.enabled)
            {
                tween.Act(ac);
            }
        }
    }

    /// <summary>
    /// Base method for doing something with all the tweeners in gameObject with right id.
    /// </summary>
    /// <param name="ac">Enum value of the given action.</param>
    /// <param name="i">Id of the tweeners we want to play.</param>
    private void BaseAllInObjectById(Action ac, int i)
    {
        foreach (Tweener tween in GetComponents<Tweener>())
        {
            if (tween.enabled && tween.id == i)
            {
                tween.Act(ac);
            }
        }
    }

    /// <summary>
    /// Base method for doing something with all the tweeners in scene.
    /// </summary>
    /// <param name="ac">Enum value of the given action.</param>
    private static void BaseAllInScene(Action ac)
    {
        foreach (Tweener tween in allTweens)
        {
            if (tween.gameObject.activeSelf)
            {
                tween.Act(ac);
            }
        }
    }

    /// <summary>
    /// Base method for doing something with all the tweeners in scene with right id.
    /// </summary>
    /// <param name="ac">Enum value of the given action.</param>
    /// <param name="i">Id of the tweener we want to play.</param>
    private static void BaseAllById(Action ac, int i)
    {
        foreach (Tweener tween in allTweens)
        {
            if (tween.id == i)
            {
                tween.Act(ac);
            }
        }
    }

    /// <summary>
    /// Base method for doing something with all the tweeners of the gameObjects children with right id.
    /// </summary>
    /// <param name="ac">Enum value of the given action.</param>
    /// <param name="i">Id of the tweener we want to play.</param>
    private void BaseAllChildrenById(Action ac, int i)
    {
        foreach (Tweener tween in GetComponentsInChildren<Tweener>())
        {
            if (tween.id == i)
            {
                tween.Act(ac);
            }
        }
    }
    #endregion

    #region PlayMethods

    /// <summary>
    /// Plays the tweener.
    /// </summary>
    public void Play()
    {
        Assign();
        foreach (Tweener t in allObjectTweens)
        {
            PriorityCheck(priorityId, priority);
        }

        delayer.Restart();
        timer.Restart(forward != from);
        DDone = false;
        TDone = false;
        on = true;
        Animate(forward != from ? 0 : 1);

        if (useOnAnyPlay)
            onAnyPlay?.Invoke();

        if (forward)
        {
            if (useOnPlay)
            {
                onPlay?.Invoke();
            }
        }
        else
        {
            if (useOnRewind)
            {
                onRewind?.Invoke();
            }
        }
    }

    /// <summary>
    /// Plays all the tweeners in gameObject.
    /// </summary>
    public void PlayAllInObject()
    {
        BaseAllInObject(Action.Play);
    }

    /// <summary>
    /// Plays all the tweeners in gameObject with right id.
    /// </summary>
    /// <param name="i">Id of the tweeners we want to play.</param>
    public void PlayAllInObjectById(int i)
    {
        BaseAllInObjectById(Action.Play, i);
    }

    /// <summary>
    /// Plays all the tweeners in scene.
    /// </summary>
    public static void PlayAllInScene()
    {
        BaseAllInScene(Action.Play);
    }

    /// <summary>
    /// Plays all the tweeners in scene with right id.
    /// </summary>
    /// <param name="i">Id of the tweener we want to play.</param>
    public static void PlayAllById(int i)
    {
        BaseAllById(Action.Play, i);
    }

    /// <summary>
    /// Plays all the tweeners of the gameObjects children with right id.
    /// </summary>
    /// <param name="i">Id of the tweener we want to play.</param>
    public void PlayAllChildrenById(int i)
    {
        BaseAllChildrenById(Action.Play, i);
    }
    #endregion

    #region RestartMethods


    /// <summary>
    /// Restarts the tweener.
    /// </summary>
    public void Restart()
    {
        Restart(true);
    }

    /// <summary>
    /// Recalculates all values of the tweener.
    /// </summary>
    /// <param name="direction">Is the tweener restarting forward of backward.</param>
    public void Restart(bool direction)
    {
        Assign();
        forward = direction;
        on = false;
        controller = GetComponent<CharacterController>();

        delayer.maxTime = delay;
        timer.maxTime = duration;
        delayer.RestartForward();
        timer.Restart(forward != from);
        currentLoop = 0;

        switch (mode)
        {
            case Mode.Color:
                RestartColor();
                break;

            case Mode.Position:
                RestartPosition();
                break;

            case Mode.Rotation:
                RestartRotation();
                break;

            case Mode.Scale:
                RestartScale();
                break;

            case Mode.RandomShake:
                RestartShake();
                break;

            case Mode.Transform:
                RestartTransform();
                break;
        }

        Animate((forward != from) ? 0 : 1);
    }

    /// <summary>
    /// Recalculates the color values of the tweener.
    /// </summary>
    public void RestartColor()
    {
        if (ui)
        {
            if (textMeshUGUI)
            {
                textMeshTargetRend = GetComponent<TextMesh>();
                if (!useStart)
                {
                    startColor = textMeshTargetRend.color;
                }
            }
            else
            {
                uiTargetRend = GetComponent<Graphic>();
                if (!useStart)
                {
                    startColor = uiTargetRend.color;
                }
            }
        }
        else if (sprite)
        {
            spriteTargetRend = GetComponent<SpriteRenderer>();
            if (!useStart)
            {
                startColor = spriteTargetRend.color;
            }
        }
        else
        {
            targetRend = GetComponent<Renderer>().materials;
            if (!useStart)
            {
                startColor = targetRend[0].color;
            }
        }
        finalEndColor = endColor;
    }

    /// <summary>
    /// Recalculates the position values of the tweener.
    /// </summary>
    public void RestartPosition()
    {
        if (useTran || mode == Mode.Transform)
        {
            if (!useStart)
                startTran = transform;

            startPos = localSpace ? startTran.localPosition : startTran.position;
            endPos = localSpace ? endTran.localPosition : endTran.position;
            finalStartPos = startPos;
            finalEndPos = endPos;
        }
        else
        {
            Vector3 currentPos = actualPosition;
            if (!useStart)
            {
                startPos = currentPos;
            }
            finalStartPos = startPos;
            finalEndPos = endPos;
            if (relative)
            {
                finalEndPos += currentPos;
                if (useStart)
                {
                    finalStartPos += currentPos;
                }
            }
        }
    }

    /// <summary>
    /// Recalculates the rotation values of the tweener.
    /// </summary>
    public void RestartRotation()
    {
        if (useTran || mode == Mode.Transform)
        {
            startRot = localSpace ? startTran.localEulerAngles : startTran.eulerAngles;
            endRot = localSpace ? endTran.localEulerAngles : endTran.eulerAngles;

            finalStartRotQ = Quaternion.Euler(startRot);
            finalEndRotQ = Quaternion.Euler(endRot);
            finalStartRot = startRot;
            finalEndRot = endRot;
        }
        else
        {
            Vector3 currentRot = localSpace ? transform.localEulerAngles : transform.eulerAngles;
            if (!useStart)
            {
                startRot = currentRot;
            }

            Vector3 tempStart = startRot;
            Vector3 tempEnd = endRot;

            if (relative)
            {
                tempEnd += currentRot;
                if (useStart)
                {
                    tempStart += currentRot;
                }
            }

            finalStartRotQ = Quaternion.Euler(tempStart);
            finalEndRotQ = Quaternion.Euler(tempEnd);
            finalStartRot = startRot;
            finalEndRot = endRot;
        }
    }

    /// <summary>
    /// Recalculates the scale values of the tweener.
    /// </summary>
    public void RestartScale()
    {
        if (useTran || mode == Mode.Transform)
        {
            startScale = localSpace ? startTran.localScale : startTran.lossyScale;
            endScale = localSpace ? endTran.localScale : endTran.lossyScale;
            finalStartScale = startScale;
            finalEndScale = endScale;
        }
        else
        {
            Vector3 currentScale = localSpace ? transform.localScale : transform.lossyScale;
            if (!useStart)
            {
                startScale = currentScale;
            }
            finalStartScale = startScale;
            finalEndScale = endScale;
            if (relative)
            {
                finalEndScale += currentScale;
                if (useStart)
                {
                    finalStartScale = currentScale;
                }
            }
        }
    }

    /// <summary>
    /// Recalculates the transform values of the tweener.
    /// </summary>
    public void RestartShake()
    {
        startShake = transform.position;
    }

    /// <summary>
    /// Recalculates the transform values of the tweener.
    /// </summary>
    public void RestartTransform()
    {
        RestartPosition();
        RestartRotation();
        RestartScale();
    }

    /// <summary>
    /// Restarts all the tweeners in gameObject.
    /// </summary>
    public void RestartAllInObject()
    {
        BaseAllInObject(Action.Restart);
    }

    /// <summary>
    /// Restarts all the tweeners in gameObject with right id.
    /// </summary>
    /// <param name="i">Id of the tweeners we want to play.</param>
    public void RestartAllInObjectById(int i)
    {
        BaseAllInObjectById(Action.Restart, i);
    }

    /// <summary>
    /// Restarts all the tweeners in scene.
    /// </summary>
    public static void RestartAllInScene()
    {
        BaseAllInScene(Action.Restart);
    }

    /// <summary>
    /// Restarts all the tweeners in scene with right id.
    /// </summary>
    /// <param name="i">Id of the tweener we want to play.</param>
    public static void RestartAllById(int i)
    {
        BaseAllById(Action.Restart, i);
    }

    /// <summary>
    /// Restarts all the tweeners of the gameObjects children with right id.
    /// </summary>
    /// <param name="i">Id of the tweener we want to play.</param>
    public void RestartAllChildrenById(int i)
    {
        BaseAllChildrenById(Action.Restart, i);
    }

    #endregion

    #region RestartBackwardMethods


    /// <summary>
    /// Recalculates all values of the tweener to work backwards. In other words, start from end value, and end to start value.
    /// </summary>
    public void RestartBackward()
    {
        Restart(false);
    }

    public void RestartBackwardAllInObject()
    {
        BaseAllInObject(Action.RestartBackward);
    }

    /// <summary>
    /// Recalculates all values of the tweener with correct id in object to work backwards, and then plays the tweener. In other words, start from end value, and end to start value.
    /// </summary>
    /// <param name="i">Id of the tweeners we want to play.</param>
    public void RestartBackwardAllInObjectById(int i)
    {
        BaseAllInObjectById(Action.RestartBackward, i);
    }

    /// <summary>
    /// Recalculates all values of the tweeners in scene to work backwards, and then plays the tweener. In other words, start from end value, and end to start value.
    /// </summary>
    public static void RestartBackwardAllInScene()
    {
        BaseAllInScene(Action.RestartBackward);
    }

    /// <summary>
    /// Recalculates all values of the tweener with correct id to work backwards, and then plays the tweener. In other words, start from end value, and end to start value.
    /// </summary>
    /// <param name="i">Id of the tweener we want to play.</param>
    public static void RestartBackwardAllById(int i)
    {
        BaseAllById(Action.RestartBackward, i);
    }

    /// <summary>
    /// Plays all the tweeners of the gameObjects children with right id.
    /// </summary>
    /// <param name="i">Id of the tweener we want to play.</param>
    public void RestartBackwardAllChildrenById(int i)
    {
        BaseAllChildrenById(Action.RestartBackward, i);
    }
    #endregion


    /// <summary>
    /// Continue the tweener from where it left.
    /// </summary>
    public void Continue()
    {
        on = true;
    }

    #region StopMethods

    /// <summary>
    /// Stops the tweener.
    /// </summary>
    public void Stop()
    {
        on = false;
    }

    /// <summary>
    /// Stops all tweeners in gameObject.
    /// </summary>
    public void StopAllInObject()
    {
        BaseAllInObject(Action.Stop);
    }

    /// <summary>
    /// Stops all tweeners in gameObject with correct id.
    /// </summary>
    /// <param name="i">Id of the tweener we want to stop.</param>
    public void StopAllInObjectById(int i)
    {
        BaseAllInObjectById(Action.Stop, i);
    }

    /// <summary>
    /// Stops all tweens in scene.
    /// </summary>
    public static void StopAllInScene()
    {
        BaseAllInScene(Action.Stop);
    }

    /// <summary>
    /// Stops all tweens in scene with right id.
    /// </summary>
    /// <param name="i">Id of the tweeners we want to stop.</param>
    public static void StopAllById(int i)
    {
        BaseAllById(Action.Stop, i);
    }

    /// <summary>
    /// Stops all tweens in children with right id.
    /// </summary>
    /// <param name="i">Id of the tweeners we want to stop.</param>
    public void StopAllChildrenById(int i)
    {
        BaseAllChildrenById(Action.Stop, i);
    }
    #endregion

    #region RestartPlayMethods
    /// <summary>
    /// Restarts and plays the tweener.
    /// </summary>
    public void RestartPlay()
    {
        Restart();
        Play();
    }

    /// <summary>
    /// Restarts and plays all the tweeners in gameObject.
    /// </summary>
    public void RestartPlayAllInObject()
    {
        BaseAllInObject(Action.RestartPlay);
    }

    /// <summary>
    /// Restarts and plays all tweeners with right id.
    /// </summary>
    /// <param name="i">Id of the tweener we want to restart.</param>
    public void RestartPlayAllById(int i)
    {
        BaseAllById(Action.RestartPlay, i);
    }

    /// <summary>
    /// Restarts and plays all the tweeners in gameObject with right id.
    /// </summary>
    /// <param name="i">Id of the tweeners we want to play.</param>
    public void RestartPlayAllInObjectById(int i)
    {
        BaseAllInObjectById(Action.RestartPlay, i);
    }

    /// <summary>
    /// Restarts and plays all the tweeners in scene.
    /// </summary>
    public static void RestartPlayAllInScene()
    {
        BaseAllInScene(Action.RestartPlay);
    }

    #endregion

    #region RestartBackwardPlayMethods

    /// <summary>
    /// Recalculates all values of the tweener to work backwards, and then plays the tweener. In other words, start from end value, and end to start value.
    /// </summary>
    public void RestartBackwardPlay()
    {
        RestartBackward();
        Play();
    }

    /// <summary>
    /// Recalculates all values of the tweener in the object to work backwards, and then plays the tweener. In other words, start from end value, and end to start value.
    /// </summary>
    public void RestartBackwardPlayAllInObject()
    {
        BaseAllInObject(Action.RestartBackwardPlay);
    }

    /// <summary>
    /// Recalculates all values of the tweener with correct id to work backwards, and then plays the tweener. In other words, start from end value, and end to start value.
    /// </summary>
    /// <param name="i">Id of the tweener we want to restart.</param>
    public void RestartBackwardPlayAllById(int i)
    {
        BaseAllById(Action.RestartBackwardPlay, i);
    }

    /// <summary>
    /// Recalculates all values of the tweener in object with correct id to work backwards, and then plays the tweener. In other words, start from end value, and end to start value.
    /// </summary>
    /// <param name="i">Id of the tweeners we want to play.</param>
    public void RestartBackwardPlayAllInObjectById(int i)
    {
        BaseAllInObjectById(Action.RestartBackwardPlay, i);
    }

    /// <summary>
    /// Recalculates all values of the tweener in scene to work backwards, and then plays the tweener. In other words, start from end value, and end to start value.
    /// </summary>
    public static void RestartBackwardPlayAllInScene()
    {
        BaseAllInScene(Action.RestartBackwardPlay);
    }
    #endregion

    /// <summary>
    /// If Tweener is destroyed, remove it from allTweens-list.
    /// </summary>
    private void OnDestroy()
    {
        if (allTweens.Contains(this))
        {
            allTweens.Remove(this);
        }

        if (allObjectTweens.Contains(this))
        {
            allObjectTweens.Remove(this);
        }

        foreach (Tweener t in allObjectTweens)
        {
            t.CountObjectTweens(false);
        }
    }

    /// <summary>
    /// Checks the priority, and if the caller has same priority Id, and bigger or priority, or if the callers priority is higher and the tween is marked to be stopped by higher priority, the tweener is stopped. 
    /// </summary>
    /// <param name="i">The callers priority ID.</param>
    /// <param name="pri">The callers priority.</param>
    public void PriorityCheck(int i, int pri)
    {
        if (on && enabled)
        {
            if (i == priorityId && pri >= priority)
            {
                Stop();
            }
            else if (pri > priority && stopByHigher)
            {
                Stop();
            }
        }
    }

    /// <summary>
    /// Finds and returns tweener with the right id from GameObject. Tweener has to be enabled for this to work.
    /// </summary>
    /// <param name="i">Id of the tweener we want.</param>
    /// <returns></returns>
    public Tweener GetTweenerById(int i)
    {
        foreach (Tweener tween in GetComponents<Tweener>())
        {
            if (tween.enabled && tween.id == i)
            {
                return tween;
            }
        }
        Debug.LogWarning("Couldn't find plausible tweener.");
        return null;
    }

    /// <summary>
    /// Finds and returns tweener with the right id from the GameObjects children. Tweener has to be enabled for this to work.
    /// </summary>
    /// <param name="i">Id of the tweener we want.</param>
    /// <returns></returns>
    public Tweener GetTweenerInChildrenById(int i)
    {
        foreach (Tweener tween in GetComponentsInChildren<Tweener>())
        {
            if (tween.enabled && tween.id == i)
            {
                return tween;
            }
        }
        Debug.LogWarning("Couldn't find plausible tweener.");
        return null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="desiredNamespace"></param>
    /// <returns></returns>
    public bool NamespaceExists(string desiredNamespace)
    {
        foreach (System.Reflection.Assembly assembly in System.AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach (System.Type type in assembly.GetTypes())
            {
                if (type.Namespace == desiredNamespace)
                    return true;
            }
        }
        return false;
    }
    public void ChangeStart(Transform t)
    {
        endTran = t;
    }

    public void ChangeEnd(Transform t)
    {
        endTran = t;
    }

    public void ChangeFromTo(bool b)
    {
        from = b;
    }
}
