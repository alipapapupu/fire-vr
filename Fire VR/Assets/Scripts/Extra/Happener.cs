﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Happener : MonoBehaviour
{
    public enum Time
    {
        OnCall,
        Awake,
        Start,
        Update,
        FixedUpdate,
        LateUpdate,
        OnEnable,
        OnDisable,
        OnDestroy,
        OnCollisionEnter,
        OnCollisionStay,
        OnCollisionExit,
        OnCollisionEnter2D,
        OnCollisionStay2D,
        OnCollisionExit2D,
        OnTriggerEnter,
        OnTriggerStay,
        OnTriggerExit,
        OnTriggerEnter2D,
        OnTriggerStay2D,
        OnTriggerExit2D
    }

    [Tooltip("What action makes the thing happen.")]
    public Time happeningTime = 0;
    [Tooltip("Will the action happen right away when possible, or will it wait for this bool to turn true before happening.")]
    public bool canHappen = true;
    [Tooltip("Will the canHappen turn off when the ded is done.")]
    public bool turnHappenOff = false;
    [Tooltip("The thing that will happen when happens.")]
    public UnityEvent happeningEvent = null;

    public void ChangeHappen(bool b)
    {
        canHappen = b;
    }

    public void Happen()
    {
        if (canHappen)
        {
            happeningEvent.Invoke();

            if (turnHappenOff)
            {
                canHappen = false;
            }
        }
    }

    void Awake()
    {
        if (happeningTime == Time.Awake)
            Happen();
    }
    void Start()
    {
        if (happeningTime == Time.Start)
            Happen();
    }
    void Update()
    {
        if (happeningTime == Time.Update)
            Happen();
    }
    void FixedUpdate()
    {
        if (happeningTime == Time.FixedUpdate)
            Happen();
    }
    void LateUpdate()
    {
        if (happeningTime == Time.LateUpdate)
            Happen();
    }
    void OnEnable()
    {
        if (happeningTime == Time.OnEnable)
            Happen();
    }
    void OnDisable()
    {
        if (happeningTime == Time.OnDisable)
            Happen();
    }
    void OnDestroy()
    {
        if (happeningTime == Time.OnDestroy)
            Happen();
    }
    void OnCollisionEnter(Collision col)
    {
        if (happeningTime == Time.OnCollisionEnter)
            Happen();
    }
    void OnCollisionStay(Collision col)
    {
        if (happeningTime == Time.OnCollisionStay)
            Happen();
    }
    void OnCollisionExit(Collision col)
    {
        if (happeningTime == Time.OnCollisionExit)
            Happen();
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (happeningTime == Time.OnCollisionEnter2D)
            Happen();
    }
    void OnCollisionStay2D(Collision2D col)
    {
        if (happeningTime == Time.OnCollisionStay2D)
            Happen();
    }
    void OnCollisionExit2D(Collision2D col)
    {
        if (happeningTime == Time.OnCollisionExit2D)
            Happen();
    }
    void OnTriggerEnter(Collider col)
    {
        if (happeningTime == Time.OnTriggerEnter)
            Happen();
    }
    void OnTriggerStay(Collider col)
    {
        if (happeningTime == Time.OnTriggerStay)
            Happen();
    }
    void OnTriggerExit(Collider col)
    {
        if (happeningTime == Time.OnTriggerExit)
            Happen();
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (happeningTime == Time.OnTriggerEnter2D)
            Happen();
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (happeningTime == Time.OnTriggerStay2D)
            Happen();
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (happeningTime == Time.OnTriggerExit2D)
            Happen();
    }
    public void DestroyThisObject()
    {
        Destroy(gameObject);
    }

    public void DestroyObject(GameObject gam)
    {
        Destroy(gam);
    }

    public void InstantiateObject(GameObject gam)
    {
        GameObject g = Instantiate(gam);
        g.transform.position = transform.position;
        Debug.DrawRay(transform.position, Vector3.up, Color.red, 1000);
    }

    public void CopyTransform(Transform t)
    {
        transform.position = t.position;
        transform.rotation = t.rotation;
    }

    public void CopyPosition(Transform t)
    {
        SetPosition(t.position);
    }

    public void SetPosition(Vector3 v)
    {
        transform.position = v;
    }

    public void CopyRotation(Transform t)
    {
        SetRotation(t.eulerAngles);
    }

    public void SetRotation(Vector3 r)
    {
        transform.eulerAngles = r;
    }

    public void CopyScale(Transform t)
    {
        SetScale(t.lossyScale);
    }

    public void SetScale(Vector3 v)
    {
        GlobalScale(v);
    }

    void GlobalScale(Vector3 globalScale)
    {
        transform.localScale = Vector3.one;
        Vector3 newScale = Vector3.zero;
        for (int i = 0; i < 3; i++)
        {
            newScale[i] = globalScale[i] * transform.lossyScale[i];
        }

        transform.localScale = newScale;
    }

    public void SetParent(Object t)
    {
        if (t as GameObject != null)
            transform.SetParent((t as GameObject).transform);
        else if (t as Transform != null)
            transform.SetParent(t as Transform);
        else if (t == null)
            transform.SetParent(null);
    }
}
