﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gyroscope : MonoBehaviour
{
    public static Gyroscope instance = null;
    bool gyroEnabled = false;
    Quaternion rot = new Quaternion(0, 0, 1, 0);
    Quaternion gyroRot = Quaternion.identity;
    UnityEngine.Gyroscope gyro = null;
    Transform cameraContainer = null;
    [SerializeField]
    float smoother = 0.5f;
    public static bool assigned = false;
    public IEnumerator SetGyroscope()
    {
        instance = this;

        cameraContainer = new GameObject("Camera Container").transform;
        cameraContainer.transform.SetParent(transform.parent);
        cameraContainer.transform.localPosition = Vector3.zero;
        cameraContainer.transform.localRotation = Quaternion.Euler(90, 90, 0);
        transform.SetParent(cameraContainer.transform);
        EnableGyro(true);
        yield return new WaitUntil(() => Input.gyro.attitude != new Quaternion(0, 0, 0, 0));
        RestartRotation();
        assigned = true;
    }

    public void EnableGyro(bool b)
    {
        b = SystemInfo.supportsGyroscope ? b : false;

        gyroEnabled = b;
        Input.gyro.enabled = b;
        gyro = Input.gyro;

        if (b)
        {
            cameraContainer.transform.localRotation = Quaternion.Euler(90, 90, 0);
        }
        else
        {
            cameraContainer.transform.localRotation = Quaternion.identity;
            transform.localRotation = Quaternion.identity;
        }
    }
    void Update()
    {
        if (gyroEnabled)
            UseGyroscope();
    }

    void UseGyroscope()
    {
        transform.localRotation = Quaternion.Slerp(transform.localRotation, gyro.attitude * rot, smoother);
    }

    public void RestartRotation()
    {
        cameraContainer.localRotation = Quaternion.Inverse(transform.localRotation);
    }
}
