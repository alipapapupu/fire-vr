﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MohaviCreative
{
    public abstract class Collection<T> : MonoBehaviour where T : MonoBehaviour
    {
        T instance = null;
        public static List<T> instances = null;

        protected virtual void OnEnable()
        {
            CheckValues();
            if (!instances.Contains(instance))
            {
                instances.Add(instance);
            }
        }

        /// <summary>
        /// This function is called when the behaviour becomes disabled or inactive.
        /// </summary>
        protected virtual void OnDisable()
        {
            CheckValues();
            if (instances.Contains(instance))
            {
                instances.Remove(instance);
            }
        }

        void CheckValues()
        {
            if (!instance)
                instance = this as T;
            if (instances == null)
                instances = new List<T>();
        }
    }
}
