﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimTrigger : MonoBehaviour
{
    [SerializeField]
    protected Animator ar = null;
    protected SimpleAnimation sa = null;
    public string tempName = "";
    [Tooltip("What the temp bool is currently. Temp bool is used to play the animation correct direction.")]
    public bool tempBool = false;
    [Tooltip("Should temp bool be used. If not, the animation is always play to same direction.")]
    public bool useTempBoolInSimple = false;
    bool first = true;
    bool animationStarted = false;
    [Tooltip("Should the animation be triggered by simple click. Good for hiding ui on click.")]
    public bool triggerOnClick = false;
    // Start is called before the first frame update
    protected virtual void Awake()
    {
        if (!ar)
        {
            ar = GetComponent<Animator>();
        }

        if (!sa)
        {
            sa = GetComponent<SimpleAnimation>();
        }

        if (!ar)
        {
            Debug.LogWarning("Animator has not been assigned. Animations wont work without it.\n" + transform.name);
        }
    }

    void LateUpdate()
    {
        if (triggerOnClick && Input.GetMouseButtonUp(0) && tempBool && !animationStarted)
        {
            PlaySimple();
        }
        animationStarted = false;
    }

    public void SetBool(string useName, bool value)
    {
        if (!ar)
            return;
        ar.SetBool(useName, value);
    }

    public void SetBool(bool value)
    {
        SetBool(tempName, value);
    }

    public void SetBool()
    {
        SetBool(tempBool);
    }

    public void SetBool(string useName)
    {
        SetBool(useName, tempBool);
    }

    public bool GetBool(string useName)
    {
        return ar.GetBool(useName);
    }

    public void SetTrigger(string useName)
    {
        if (!ar)
            return;
        ar.SetTrigger(useName);
    }

    public void ResetTrigger(string useName)
    {
        if (!ar)
            return;
        ar.ResetTrigger(useName);
    }

    public void SetFloat(string useName, float value)
    {
        if (!ar)
            return;
        ar.SetFloat(useName, value);
    }

    public void SetFloat(float value)
    {
        SetFloat(tempName, value);
    }

    public float GetFloat(string useName)
    {
        return ar.GetFloat(useName);
    }

    public void SetInt(string useName, int value)
    {
        if (!ar)
            return;
        ar.SetInteger(useName, value);
    }

    public void SetInt(int value)
    {
        SetInt(tempName, value);
    }

    public int GetInt(string useName)
    {
        return ar.GetInteger(useName);
    }

    public bool HasState(string useName)
    {
        return ar.HasState(0, Animator.StringToHash(useName));
    }

    public void SetSpeed(float speed)
    {
        ar.speed = speed;
    }

    public bool IsCurrentState(string useName)
    {
        return ar.GetCurrentAnimatorStateInfo(0).IsName(useName);
    }

    public void SetTempName(string useName)
    {
        tempName = useName;
    }

    ///<summary>
    ///Plays the simple animation.
    ///</summary>
    public void PlaySimple()
    {
        PlaySimple("Default");
    }

    ///<summary>
    ///Plays the simple animation.
    ///</summary>
    /// <param name="forward">Should the animation be played forward or backward.</param>
    public void PlaySimple(bool forward)
    {
        PlaySimple(forward, "Default");
    }

    ///<summary>
    ///Plays the simple animation.
    ///</summary>
    /// <param name="forward">Should the animation be played forward or backward.</param>
    /// <param name="useName">Name of the animation which you want to play.</param>
    public void PlaySimple(bool forward, string useName)
    {
        if (!sa)
        {
            Debug.LogWarning("GameObject doesnt have SimpleAnimation.");
            return;
        }
        else if (StateExists(useName, out SimpleAnimation.State state))
        {
            if ((forward && state.speed < 0) ||
            (!forward && state.speed > 0))
            {
                SetSimpleSpeed(useName, -1, true);
            }

            if (forward)
                sa.Rewind(useName);

            sa.Play(useName);
            animationStarted = true;
        }
    }


    ///<summary>
    ///Plays the simple animation.
    ///</summary>
    /// <param name="useName">Name of the animation which you want to play.</param>
    public void PlaySimple(string useName)
    {
        if (!sa)
            return;

        if (useTempBoolInSimple)
        {
            if (!tempBool)
                sa.Rewind(useName);

            if (!first)
                SetSimpleSpeed(useName, -1, true);
            else
                first = false;

            tempBool = !tempBool;
        }
        sa.Play(useName);
        animationStarted = true;
    }

    ///<summary>
    ///Sets the speed of simple animation.
    ///</summary>
    /// <param name="useName">Name of the animation which speed you want to stop.</param>
    /// <param name="speed">The speed value you want to use.</param>
    /// <param name="multiplier">Should the speed value be used as multiplier, or exact value.</param>
    public void SetSimpleSpeed(string useName, float speed, bool multiplier)
    {
        if (StateExists(useName, out SimpleAnimation.State state))
        {
            float useSpeed = multiplier ? state.speed * speed : speed;
            state.speed = useSpeed;
        }
        else
        {
            Debug.LogWarning("State not found.");
        }
    }
    public bool StateExists(string useName)
    {
        SimpleAnimation.State state = sa.GetState(useName);

        if (state == null)
        {
            Debug.LogWarning("State not found.");
        }

        return state != null;
    }

    public bool StateExists(string useName, out SimpleAnimation.State state)
    {
        state = sa.GetState(useName);

        if (state == null)
        {
            Debug.LogWarning("State not found.");
        }

        return state != null;
    }
}
