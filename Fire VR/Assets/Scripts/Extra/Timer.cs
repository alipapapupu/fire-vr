﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer
{
    /// <summary>
    /// Starting time of the timer.
    /// </summary>
    float startTime = 0;
    /// <summary>
    /// Ending time of the timer.
    /// </summary>
    public float maxTime = 0;
    /// <summary>
    /// Timers current value.
    /// </summary>
    float timer = 0;
    /// <summary>
    /// Speed of the timer.
    /// </summary>
    public float speed = 1;
    /// <summary>
    /// Should the timer autorestart when completed.
    /// </summary>
    bool autoRestart = false;
    /// <summary>
    /// Does the timer use loops.
    /// </summary>
    bool useLoop = false;
    /// <summary>
    /// How many loops the timer uses.
    /// </summary>
    int maxLoop = 0;
    /// <summary>
    /// On which loop are we.
    /// </summary>
    int loop = 0;
    /// <summary>
    /// Minimum value the timer returns.
    /// </summary>
    float minValue = 0;
    /// <summary>
    /// Maximum value the timer returns.
    /// </summary>
    float maxValue = 1;
    /// <summary>
    /// Does the timer use deltatime.
    /// </summary>
    bool useDeltaTime = true;
    /// <summary>
    /// Is the timer done.
    /// </summary>
    public bool done = false;
    /// <summary>
    /// How much was left over from last timer.
    /// </summary.
    public float overFloat = 0f;


    /// <summary>
    /// Creates normal timer.
    /// </summary>
    /// <param name="maxTime">How long the timer is.</param>
    public Timer(float maxTime)
    {
        startTime = 0;
        this.maxTime = maxTime;
        speed = 1;
        useDeltaTime = true;
        autoRestart = false;
    }

    /// <summary>
    /// Creates normal timer.
    /// </summary>
    /// <param name="maxTime">How long the timer is.</param>
    /// <param name="speed">How fast the timer is.</param>
    /// <param name="autoRestart">Will the timer restart itself after completion.</param>
    public Timer(float maxTime, float speed, bool autoRestart)
    {
        startTime = 0;
        this.maxTime = maxTime;
        this.speed = speed;
        useDeltaTime = true;
        this.autoRestart = autoRestart;
    }

    /// <summary>
    /// Creates normal timer.
    /// </summary>
    /// <param name="maxTime">How long the timer is.</param>
    /// <param name="speed">How fast the timer is.</param>
    /// <param name="useDeltaTime">Should the timer be used with Time.deltaTime.</param>
    /// <param name="autoRestart">Will the timer restart itself after completion.</param>
    public Timer(float maxTime, float speed, bool useDeltaTime, bool autoRestart)
    {
        startTime = 0;
        this.maxTime = maxTime;
        this.speed = speed;
        this.useDeltaTime = useDeltaTime;
        this.autoRestart = autoRestart;
    }

    /// <summary>
    /// Creates normal timer with different startingtime.
    /// </summary>
    /// <param name="startTime">At what will the timer start.</param>
    /// <param name="maxTime">How long the timer is.</param>
    /// <param name="speed">How fast the timer is.</param>
    /// <param name="useDeltaTime">Should the timer be used with Time.deltaTime.</param>
    /// <param name="autoRestart">Will the timer restart itself after completion.</param>
    public Timer(float startTime, float maxTime, float speed, bool useDeltaTime, bool autoRestart)
    {
        this.maxTime = maxTime;
        this.speed = speed;
        this.useDeltaTime = useDeltaTime;
        this.autoRestart = autoRestart;
        this.startTime = startTime;
    }

    /// <summary>
    /// Creates normal timer which getfloat-methods minimum and maximum values are different.
    /// </summary>
    /// <param name="maxTime">How long the timer is.</param>
    /// <param name="speed">How fast the timer is.</param>
    /// <param name="useDeltaTime">Should the timer be used with Time.deltaTime.</param>
    /// <param name="autoRestart">Will the timer restart itself after completion.</param>
    /// <param name="minValue">Minimum value the getfloat-method will return.</param>
    /// <param name="maxValue">Maximum value the getfloat-method will return.</param>
    public Timer(float maxTime, float speed, bool useDeltaTime, bool autoRestart, float minValue, float maxValue)
    {
        startTime = 0;
        this.maxTime = maxTime;
        this.speed = speed;
        this.useDeltaTime = useDeltaTime;
        useLoop = true;
        autoRestart = true;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    /// <summary>
    /// Creates normal timer which will loop predetermined times.
    /// </summary>
    /// <param name="maxTime">How long the timer is.</param>
    /// <param name="speed">How fast the timer is.</param>
    /// <param name="useDeltaTime">Should the timer be used with Time.deltaTime.</param>
    /// <param name="maxLoop">How many times the loop will play before stopping completely.</param>
    public Timer(float maxTime, float speed, bool useDeltaTime, int maxLoop)
    {
        startTime = 0;
        this.maxTime = maxTime;
        this.speed = speed;
        this.useDeltaTime = useDeltaTime;
        this.maxLoop = maxLoop;
        useLoop = true;
        autoRestart = true;
    }

    /// <summary>
    /// Checks if the timer will be restarted.
    /// </summary>
    /// <param name="forward">Which way the timer is going</param>
    void AutoRestart(bool forward)
    {
        overFloat = forward ? timer - maxTime : startTime - timer;
        if (autoRestart)
        {
            timer = forward ? startTime : maxTime;
        }
        else
        {
            timer = forward ? maxTime : startTime;
            done = true;
        }
    }

    /// <summary>
    /// Checks if the timer uses deltatime, and returns correct time.
    /// </summary>
    /// <returns>Correct time stamp.</returns>
    float DeltaTime()
    {
        return (useDeltaTime ? Time.deltaTime : Time.unscaledDeltaTime);
    }

    /// <summary>
    /// Increases or decreases the timer.
    /// </summary>
    /// <param name="direction">Will the timer be increased or decresed. True if increasing.</param>
    /// <returns>Returns true if the timer completes itself.</returns>
    public bool NextBool(bool direction)
    {
        if (done)
        {
            return true;
        }

        timer += speed * DeltaTime() * (direction ? 1 : -1);

        if (direction ? timer >= maxTime : timer <= startTime)
        {
            AutoRestart(direction);
            return Looping();
        }

        return false;
    }

    /// <summary>
    /// Increases the timer.
    /// </summary>
    /// <returns>Returns true if the timer completes itself.</returns>
    public bool NextBool()
    {
        return NextBool(true);
    }

    /// <summary>
    /// Decreases the timer.
    /// </summary>
    /// <returns>Returns true if the timer completes itself.</returns>
    public bool BackBool()
    {
        return NextBool(false);
    }

    /// <summary>
    /// Increases the timer.
    /// </summary>
    /// <returns>Return true the first time the timer reaches the target.</returns>
    public bool OnceBool(bool forward)
    {
        if (!done)
        {
            return NextBool(forward);
        }
        return false;
    }

    /// <summary>
    /// Increases or decreases the timer.
    /// </summary>
    /// <param name="direction">Will the timer be increased or decresed. True if increasing.</param>
    /// <returns>Returns float between min and max value.</returns>
    public float NextFloat(bool direction)
    {
        if (done)
        {
            return direction ? 1 : 0;
        }

        NextBool(direction);

        return timer / maxTime * (maxValue - minValue) + minValue;
    }

    /// <summary>
    /// Increases the timer.
    /// </summary>
    /// <returns>Returns float between min and max value.</returns>
    public float NextFloat()
    {
        return NextFloat(true);
    }

    /// <summary>
    /// Decreases the timer.
    /// </summary>
    /// <returns>Returns float between min and max value.</returns>
    public float BackFloat()
    {
        return NextFloat(false);
    }


    /// <summary>
    /// Increases the timer.
    /// </summary>
    /// <returns>Returns current time.</returns>
    public float NextTime()
    {
        NextFloat(true);
        return timer;
    }

    /// <summary>
    /// Decreases the timer.
    /// </summary>
    /// <returns>Returns current time.</returns>
    public float BackTime()
    {
        NextFloat(false);
        return timer;
    }


    /// <summary>
    /// Checks if the timer completed itself.
    /// </summary>
    /// <param name="forward">Checks the wanted direction of timer. True is forward.</param>
    /// <returns>Returns true if the timer has completed itself.</returns>
    public bool GetBool(bool forward)
    {
        if (forward ? timer >= maxTime : timer <= startTime)
        {
            return Looping();
        }
        return false;
    }

    /// <summary>
    /// Gives the current value of timer between 0 and 1.
    /// </summary>
    /// <returns>Returns value between 0 and 1.</returns>
    public float GetFloat()
    {
        return (GetFloat(true));
    }

    /// <summary>
    /// Gives the current value of timer between 0 and 1.
    /// </summary>
    /// <param name="forward">Checks the wanted direction of timer. True is forward.</param>
    /// <returns>Returns value between 0 and 1.</returns>
    public float GetFloat(bool forward)
    {
        if (forward ? timer >= maxTime : timer <= startTime)
        {
            return forward ? maxValue : minValue;
        }
        return Mathf.Abs((forward ? 0 : 1) - (timer / maxTime * (maxValue - minValue) + minValue));
    }

    /// <summary>
    /// Checks if the timer should loop.
    /// </summary>
    /// <returns>True if the timer is truly over. False if it will still loop.</returns>
    bool Looping()
    {
        if (useLoop)
        {
            if (loop == maxLoop)
            {
                loop = 0;
                done = true;
                return true;
            }
            return false;
        }
        else
        {
            done = true;
            return true;
        }
    }

    /// <summary>
    /// Sets the timers time. Will clamp the value between start and max time.
    /// </summary>
    /// <param name="time">Time you want the timer to be.</param>
    public void SetTime(float time)
    {
        timer = Mathf.Clamp(time, startTime, maxTime);
    }

    /// <summary>
    /// Gives the current time without any changes.
    /// </summary>
    /// <returns>Untouched current time.</returns>
    public float GetTime()
    {
        return timer;
    }

    /// <summary>
    /// Restarts the timer. 
    /// </summary>
    public void Restart()
    {
        RestartForward();
    }

    /// <summary>
    /// Restarts the timer. 
    /// </summary>
    /// <param name="forward">Direction you are restarting. True is forward, aka normal restart.</param>
    public void Restart(bool forward)
    {
        if (forward)
        {
            RestartForward();
        }
        else
        {
            RestartBackward();
        }
    }

    /// <summary>
    /// Restarts the timer forward.
    /// </summary>
    public void RestartForward()
    {
        done = false;
        timer = startTime;
    }

    /// <summary>
    /// Restarts the timer backwards.
    /// </summary>
    public void RestartBackward()
    {
        done = false;
        timer = maxTime;
    }

    /// <summary>
    /// Restart the timer forward with the leftover float it had.
    /// </summary>
    public void RestartForwardWithOver()
    {
        done = false;
        timer = overFloat;

        if (timer >= maxTime)
        {
            overFloat = timer - maxTime;
            done = true;
        }
    }

    /// <summary>
    /// Restart the timer with the leftover float it had.
    /// </summary>
    /// <param name="forward">Direction to restart.</param>
    public void RestartWithOver(bool forward)
    {
        done = false;
        timer = forward ? startTime + overFloat : maxTime - overFloat;

        if (forward ? timer >= maxTime : timer <= startTime)
        {
            overFloat = forward ? timer - maxTime : startTime - timer;
            done = true;
        }
    }


    /// <summary>
    /// Increases the timers time with wanted value.
    /// </summary>
    /// <param name="time">How much do you want to increase the timers time.</param>
    public void IncreaseTime(float time)
    {
        done = false;
        SetTime(timer + time);
        if (timer >= maxTime)
        {
            overFloat = timer - maxTime;
            done = true;
        }
    }

    /// <summary>
    /// Decreases the timers time with wanted value.
    /// </summary>
    /// <param name="time">How much do you want to decrease the timers time.</param>
    public void DecreaseTime(float time)
    {
        done = false;
        SetTime(timer - time);
        if (timer <= startTime)
        {
            overFloat = startTime - timer;
            done = true;
        }
    }

    public void ChangeTime(bool forward, float time)
    {
        if (forward)
        {
            IncreaseTime(time);
        }
        else
        {
            DecreaseTime(time);
        }
    }
}
