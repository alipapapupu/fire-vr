﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeDrawer : MonoBehaviour
{
    [SerializeField]
    Color color = Color.white;

    [SerializeField]
    Shapes shape = 0;

    [SerializeField]
    Mesh mesh = null;

    public enum Shapes
    {
        Box,
        Sphere,
        Mesh,
        WireBox,
        WireSphere,
        WireMesh
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = color;
        Gizmos.matrix = transform.localToWorldMatrix;

        switch (shape)
        {
            case Shapes.Box:
                Gizmos.DrawCube(Vector3.zero, Vector3.one);
                break;
            case Shapes.Sphere:
                Gizmos.DrawSphere(Vector3.zero, 1);
                break;
            case Shapes.Mesh:
                Gizmos.DrawMesh(mesh, 0, Vector3.zero, Quaternion.identity, Vector3.one);
                break;
            case Shapes.WireBox:
                Gizmos.DrawWireCube(Vector3.zero, Vector3.one); ;
                break;
            case Shapes.WireSphere:
                Gizmos.DrawWireSphere(Vector3.zero, 1);
                break;
            case Shapes.WireMesh:
                Gizmos.DrawWireMesh(mesh, 0, Vector3.zero, Quaternion.identity, Vector3.one);
                break;
        }

        Gizmos.color = Color.white;
        Gizmos.matrix = Matrix4x4.identity;
    }
}
