﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [Tooltip("The object to be created.")]
    public GameObject pooledObject = null;
    [Tooltip("GameObject who will parent when pooled item is taken from pool")]
    [SerializeField]
    Transform poolParent = null;
    [Tooltip("How many object should be created to the pool on the start of the game")]
    [SerializeField]
    int amount = 10;
    [Tooltip("Is the object UI element. Affects the default parent.")]
    [SerializeField]
    bool UIObject = false;

    [Tooltip("Id of the pooler script")]
    public int poolerId = 0;

    [Tooltip("Name of the pooler script")]
    public string poolerName = "";

    Transform parent;

    [HideInInspector]
    public GameObject[] pooling;

    public static List<Pool> allPools;
    public static GameObject canvas = null;

    private void Reset()
    {
        poolerId = FindObjectsOfType<Pool>().Length - 1;
        poolerName = "Pooler " + poolerId;
    }

    private void Awake()
    {
        if (!pooledObject)
        {
            Destroy(this);
            return;
        }

        if (allPools == null || allPools[0] == null)
        {
            allPools = new List<Pool>();
        }

        if (!allPools.Contains(this))
        {
            allPools.Add(this);
        }

        parent = new GameObject().transform;
        parent.SetParent(transform);
        parent.name = pooledObject.name + "s";

        pooling = new GameObject[amount];

        for (int i = 0; i < amount; i++)
        {
            pooling[i] = Create();
        }

        if (poolParent == null)
        {
            if (UIObject)
            {
                if (!canvas)
                {
                    GameObject can = new GameObject().AddComponent<Canvas>().gameObject;
                }
                poolParent = canvas.transform;
            }
            else
            {
                poolParent = transform;
            }
        }
    }

    public GameObject CreateObject()
    {
        return CreateObject(poolParent);
    }

    public GameObject CreateObject(Transform parent)
    {
        GameObject poolObject = null;
        for (int i = 0; i < pooling.Length; i++)
        {
            if (!pooling[i].activeSelf)
            {
                poolObject = pooling[i];
            }
        }

        if (!poolObject)
        {

            GameObject[] copy1 = new GameObject[10];
            GameObject[] copy2 = pooling.Clone() as GameObject[];

            for (int i = 0; i < 10; i++)
            {
                copy1[i] = Create();
            }

            poolObject = copy1[0];

            pooling = new GameObject[copy1.Length + copy2.Length];
            copy1.CopyTo(pooling, 0);
            copy2.CopyTo(pooling, copy1.Length);
        }

        poolObject.SetActive(true);
        poolObject.transform.SetParent(poolParent);

        return poolObject;
    }

    GameObject Create()
    {
        GameObject newObject = Instantiate(pooledObject, parent);
        newObject.SetActive(false);

        if (newObject.GetComponent<PooledItem>())
        {
            newObject.GetComponent<PooledItem>().Create(parent, this);
        }
        else
        {
            newObject.AddComponent<PooledItem>().Create(parent, this);
        }

        return newObject;
    }

    public static GameObject Create(int id)
    {
        Pool pool = GetPool(id);

        if (pool)
        {
            return pool.CreateObject();
        }
        return null;
    }

    public static GameObject Create(string id)
    {
        Pool pool = GetPool(id);

        if (pool)
        {
            return pool.CreateObject();
        }
        return null;
    }

    public static Pool GetPool(int id)
    {
        foreach (Pool po in allPools)
        {
            if (po.poolerId == id)
            {
                return po;
            }
        }
        return null;
    }

    public static Pool GetPool(string Name)
    {
        foreach (Pool po in allPools)
        {
            if (po.poolerName == Name)
            {
                return po;
            }
        }
        return null;
    }

    public static Pool GetPoolByObject(string Name)
    {
        foreach (Pool po in allPools)
        {
            if (po.pooledObject.name == Name)
            {
                return po;
            }
        }
        return null;
    }

    public static void ResetPools()
    {
        if (allPools == null) return;

        foreach (Pool po in allPools)
        {
            foreach (GameObject go in po.pooling)
            {
                go.SetActive(false);
            }
        }
    }

    public void Return(Transform child, Transform parent)
    {
        if (gameObject.activeSelf)
            StartCoroutine(Returner(child, parent));
    }

    public IEnumerator Returner(Transform child, Transform parent)
    {
        yield return null;
        child.parent = parent;
    }
}
