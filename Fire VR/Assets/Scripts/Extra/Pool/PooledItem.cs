﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledItem : MonoBehaviour
{
    bool inPool = true;
    Transform parent;
    Pool pool = null;

    public void Create(Transform parent, Pool pool)
    {
        this.parent = parent;
        this.pool = pool;
    }

    public void Return()
    {
        if (pool)
            pool.Return(transform, parent);

        inPool = false;
        gameObject.SetActive(false);
    }

    public IEnumerator ReturnLate()
    {
        yield return null;
        Return();
    }

    void OnDisable()
    {
        if (inPool)
        {
            Return();
        }
    }

    void OnEnable()
    {
        inPool = true;
    }
}
