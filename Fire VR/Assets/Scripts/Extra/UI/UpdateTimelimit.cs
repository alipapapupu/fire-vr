﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpdateTimelimit : MonoBehaviour
{

    public void NewTime(float f)
    {
        NewTime(f.ToString());
    }

    public void NewTime(string s)
    {
        GetComponent<TextMeshPro>().text = s;
    }
}
