﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TextSizeGroup : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI[] texts = null;
    float fontSize = float.PositiveInfinity;
    Vector2Int resolution = Vector2Int.zero;
    bool firstTime = true;

    IEnumerator Start(){
        yield return null;
        StartCoroutine(Calculate(true));
    }
    public IEnumerator Calculate(bool wait = false)
    {
        resolution = new Vector2Int(Screen.width, Screen.height);

        foreach (TextMeshProUGUI t in texts)
        {
            t.enableAutoSizing = true;
            t.ForceMeshUpdate();
        }

        if (firstTime || wait)
        {
            firstTime = false;
            yield return null;
        }

        foreach (TextMeshProUGUI t in texts)
        {
            t.ForceMeshUpdate();
            if (t.fontSize < fontSize)
            {
                fontSize = t.fontSize;
            }
            t.enableAutoSizing = false;
            t.ForceMeshUpdate();
        }

        foreach (TextMeshProUGUI t in texts)
        {
            t.fontSize = fontSize;
            t.ForceMeshUpdate();
        }
    }

    void LateUpdate()
    {
        if (resolution != new Vector2Int(Screen.width, Screen.height))
        {
            StartCoroutine(Calculate());
        }
    }

    public void PCalculate(){
        StartCoroutine(Calculate());
    }
}
