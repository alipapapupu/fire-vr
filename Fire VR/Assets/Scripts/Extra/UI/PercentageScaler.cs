﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[ExecuteAlways]
public class PercentageScaler : UIPercentage
{

    /// <summary>
    /// Should the code preserve the objects aspect ratio. To use this, the gameobject has to have image component with a sprite. Both useX and useY cant be on if this is on.
    /// </summary>
    [Tooltip("Should the code preserve the objects aspect ratio. To use this, the gameobject has to have image component with a sprite. Both useX and useY cant be on if this is on.")]
    [SerializeField]
    bool preserveAspect = false;

    /// <summary>
    /// If imageFitter is on, will adjust sizes so the image will always be inside the new size.
    /// </summary>
    [Tooltip("If imageFitter is on, will adjust sizes so the image will always be inside the new size.")]
    [SerializeField]
    bool imageFitter = false;

    /// <summary>
    /// Objects image-component.
    /// </summary>
    [Tooltip("Objects image-component.")]
    MaskableGraphic image = null;

    Texture texture = null;

    public bool debug = false;

    override protected bool use
    {
        get { return base.use || imageFitter; }
    }

    override protected void Update()
    {
        base.Update();

        if (image && texture != image.mainTexture)
        {
            Calculate();
        }
    }

    override protected void Calculate()
    {
        Vector2 size = fromParent ? parent.rect.size : new Vector2(Screen.width, Screen.height);

        Vector2 newSize = rectTransform.sizeDelta;

        //Calculating percentages.
        if (useX || imageFitter)
            newSize.x = size.x * percentage.x / 100;
        if (useY || imageFitter)
            newSize.y = size.y * percentage.y / 100;

        //Calculating the aspects. If both useX and useY are on, useY is ignored.
        if (preserveAspect)
        {
            //Checking it he aspect can even be used.
            bool canUseAspect = true;
            if (!image || !image.mainTexture || !texture || texture != image.mainTexture)
            {
                image = GetComponent<MaskableGraphic>();
                if (!image)
                {
                    canUseAspect = false;
                    Debug.LogWarning("GameObject has no image, cant use aspect preserving.", gameObject);
                }
                else if (!image.mainTexture)
                {
                    canUseAspect = false;
                    Debug.LogWarning("GameObject has image, but the image doesnt have a sprite. cant use aspect preserving", gameObject);
                }
                else
                {
                    texture = image.mainTexture;
                }
            }


            //Will calculate the aspect only if possible.
            if (canUseAspect)
            {
                Vector2 imageSize = new Vector2(texture.width, texture.height);
                Vector2 aspectRatio = new Vector2(imageSize.y / imageSize.x, imageSize.x / imageSize.y);
                Vector2 aspectSize = newSize;

                if (useX || imageFitter)
                    aspectSize.y = newSize.x * aspectRatio.x;
                if ((!useX && useY) || imageFitter)
                    aspectSize.x = newSize.y * aspectRatio.y;

                if (imageFitter)
                {
                    if (aspectSize.x > newSize.x)
                    {
                        newSize.y = aspectSize.y;
                    }
                    else if (aspectSize.y > newSize.y)
                    {
                        newSize.x = aspectSize.x;
                    }
                }
                else
                {
                    newSize = aspectSize;
                }
            }
        }

        //Checks if the calculated size is already set to make sure ui isnt update unnecessary.
        if (rectTransform.sizeDelta != newSize)
            rectTransform.sizeDelta = newSize;

    }
}
