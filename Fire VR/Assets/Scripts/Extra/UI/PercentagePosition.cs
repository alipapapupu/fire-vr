﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PercentagePosition : UIPercentage
{
    /// <summary>
    /// Where is the x position calculated. Left if true, right if false. 
    /// </summary>
    [Tooltip("Where is the x position calculated. Left if true, right if false. ")]
    [SerializeField]
    bool startFromLeft = true;
    /// <summary>
    /// Where is the y position calculated. Bottom if true, top if false. 
    /// </summary>
    [Tooltip("Where is the y position calculated. Bottom if true, top if false. ")]
    [SerializeField]
    bool startFromBottom = true;
    /// <summary>
    /// Will the objects pivot point be calculated with the position. 
    /// </summary>
    [Tooltip("Will the objects pivot point be calculated with the position. ")]
    [SerializeField]
    bool dynamicPivot = false;
    override protected void Calculate()
    {
        Vector4 positions = fromParent ? Rect(parent.rect.min, parent.rect.max) : Rect(new Vector2(Screen.width, Screen.height));
        Vector2 size = fromParent ? new Vector2(positions.y - positions.x, positions.w - positions.z) : new Vector2(Screen.width, Screen.height);

        Vector2 normalizedSize = new Vector2(size.x * percentage.x, size.y * percentage.y) / 100;
        Vector2 normalizedSomething = normalizedSize / size;
        Vector3 newPosition = fromParent ? rectTransform.localPosition : rectTransform.position;
        if (useX)
        {
            newPosition.x = normalizedSize.x * (startFromLeft ? 1 : -1);
            newPosition.x += startFromLeft ? positions.x : positions.y;
        }
        if (useY)
        {
            newPosition.y = normalizedSize.y * (startFromBottom ? 1 : -1);
            newPosition.y += startFromBottom ? positions.z : positions.w;
        }

        if (dynamicPivot)
        {
            Vector2 newPivot = new Vector2(
            useX ? (normalizedSomething.x - (startFromLeft ? 0 : 1)) : rectTransform.pivot.x,
            useY ? (normalizedSomething.y - (startFromBottom ? 0 : 1)) : rectTransform.pivot.y
            );

            rectTransform.pivot = newPivot;
        }

        if ((fromParent ? rectTransform.localPosition : rectTransform.position) != newPosition)
        {
            if (fromParent)
                rectTransform.localPosition = newPosition;
            else
                rectTransform.position = newPosition;
        }
    }

    Vector4 Rect(Vector2 max)
    {
        return Rect(new Vector2(0, 0), max);
    }
    Vector4 Rect(Vector2 x, Vector2 y)
    {
        return new Vector4(x.x, y.x, x.y, y.y);
    }
}
