﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[ExecuteAlways]
public abstract class UIPercentage : MonoBehaviour
{
    /// <summary>
    /// Object which size is used for percentage. Automatically sets the objects own parent to be the parent object. 
    /// </summary>
    [Tooltip("Object which size is used for percentage. Automatically sets the objects own parent to be the parent object.")]
    public RectTransform parent = null;
    /// <summary>
    /// Percentages to use from the parent objects size or global size. 
    /// </summary>
    [Tooltip("Percentages to use from the parent objects size or global size. ")]
    [SerializeField]
    protected Vector2 percentage = Vector2.one * 100;
    /// <summary>
    /// Which percentages should be used, x,y or both. Choosing neither wont do anything. If using preserving aspect ratio, you cant choose both.
    /// </summary>
    [Tooltip("Which percentages should be used, x,y or both. Choosing neither wont do anything. If using preserving aspect ratio, you cant choose both.")]
    [SerializeField]
    protected bool useX = false, useY = false;
    bool variablesSet = false;
    protected virtual bool use
    {
        get { return useX || useY; }
    }

    /// <summary>
    /// Objects rect trasfrom. 
    /// </summary>
    [Tooltip("Objects rect trasfrom.")]
    protected RectTransform rectTransform = null;

    /// <summary>
    /// Will the code use the parent objects size or the global size. 
    /// </summary>
    [Tooltip("Will the code use the parent objects variables or the global variables. ")]
    [SerializeField]
    protected bool fromParent = true;
    Vector2 currentSize = Vector2.zero;

    void Start()
    {
        variablesSet = false;
    }

    protected virtual void Update()
    {
        Set();

        if (use)
        {
            if ((fromParent && parent && currentSize != parent.rect.size) ||
            (!fromParent && currentSize != new Vector2(Screen.width, Screen.height)))
            {
                currentSize = fromParent ? parent.rect.size : new Vector2(Screen.width, Screen.height);
                Calculate();
            }
        }
    }

    void OnEnable()
    {
        variablesSet = false;
        Set();
        Calculate();
    }

    void Set()
    {
        if (!variablesSet)
        {
            variablesSet = true;
            //Setting basic variables.
            if (!rectTransform)
                rectTransform = GetComponent<RectTransform>();
            if (!parent)
            {
                if (transform.parent)
                    parent = transform.parent.GetComponent<RectTransform>();
            }
        }
    }

    protected abstract void Calculate();

    public static Vector2 Percentages(Vector2 wantedPercentage)
    {
        return Percentages(wantedPercentage, new Vector2(Screen.width, Screen.height));
    }

    public static Vector2 Percentages(Vector2 wantedPercentage, Vector2 origin)
    {
        wantedPercentage /= 100;
        Vector2 pos = new Vector2(origin.x * wantedPercentage.x, origin.y * wantedPercentage.y);
        pos.x -= origin.x / 2;
        pos.y -= origin.y / 2;
        return pos;
    }

    public Vector2 Percentages(Vector2 wantedPercentage, bool fromParent)
    {
        return Percentages(wantedPercentage, fromParent ? parent.rect.size : new Vector2(Screen.width, Screen.height));
    }
}
