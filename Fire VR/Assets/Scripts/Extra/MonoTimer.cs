﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MonoTimer : MonoBehaviour
{
    [System.Serializable]
    class TimeCall : UnityEvent<float> { };
    [System.Serializable]
    class TimeCallString : UnityEvent<string> { };
    [SerializeField]
    float maxTime = 1;
    Timer t = null;
    [SerializeField, Tooltip("How many numbers should be shown after comma.")]
    int num = 0;
    [SerializeField, Tooltip("Which direction should the timer go?")]
    bool reversed = false;
    [SerializeField]
    TimeCall timeUpdate = null;
    [SerializeField]
    TimeCallString timeUpdateString = null;
    [SerializeField]
    UnityEvent finished = null;
    string s = "";
    float f = 0;
    // Start is called before the first frame update
    void Start()
    {
        t = new Timer(maxTime);
        t.Restart(!reversed);
    }

    // Update is called once per frame
    void Update()
    {
        t.NextFloat(!reversed);

        s = t.GetTime().ToString("f" + num);
        f = float.Parse(s);
        timeUpdate?.Invoke(t.GetTime());
        timeUpdateString?.Invoke(t.GetTime().ToString("f" + num));

        if (t.GetBool(!reversed))
        {
            finished?.Invoke();
        }
    }


}
