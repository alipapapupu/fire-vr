﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class RemoveObiCollider : EditorWindow
{
    [MenuItem("Tools/Remove Obi Colliders")]
    static void CreateRemoveObiCollider()
    {
        EditorWindow.GetWindow<RemoveObiCollider>();
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Remove Obi Colliders"))
        {
            var selection = Selection.gameObjects;

            for (var i = selection.Length - 1; i >= 0; --i)
            {
                Obi.ObiCollider[] colliders = selection[i].GetComponentsInChildren<Obi.ObiCollider>();
                for (int o = 0; o < colliders.Length; o++)
                {
                    Undo.RegisterCreatedObjectUndo(selection[i], "Remove Obi Collider");
                    DestroyImmediate(colliders[o]);
                }
                Obi.ObiRigidbody[] rigidbodies = selection[i].GetComponentsInChildren<Obi.ObiRigidbody>();
                for (int o = 0; o < rigidbodies.Length; o++)
                {
                    Undo.RegisterCreatedObjectUndo(selection[i], "Remove Obi Rigidbody");
                    DestroyImmediate(rigidbodies[o]);
                }
            }
        }

        GUI.enabled = false;
        EditorGUILayout.LabelField("Selection count: " + Selection.objects.Length);
    }
}