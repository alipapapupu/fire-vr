﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class AddObiCollider : EditorWindow
{
    [MenuItem("Tools/Add Obi Colliders")]
    static void CreateAddObiCollider()
    {
        EditorWindow.GetWindow<AddObiCollider>();
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Add Obi Colliders"))
        {
            var selection = Selection.gameObjects;

            for (var i = selection.Length - 1; i >= 0; --i)
            {
                foreach (Collider c in selection[i].GetComponentsInChildren<Collider>())
                {
                    List<Obi.ObiCollider> oc = new List<Obi.ObiCollider>(c.GetComponents<Obi.ObiCollider>());
                    int colliderAmount = c.GetComponents<Collider>().Length;

                    if (oc.Count == colliderAmount)
                    {
                        continue;
                    }

                    Undo.RegisterCreatedObjectUndo(selection[i], "Add Obi Collider");

                    while (oc.Count < colliderAmount)
                    {
                        oc.Add(c.gameObject.AddComponent<Obi.ObiCollider>());
                    }
                    while (oc.Count > colliderAmount)
                    {
                        Destroy(oc[oc.Count - 1]);
                        oc.RemoveAt(oc.Count - 1);
                    }

                    for (int o = 0; o < oc.Count; o++)
                    {
                        oc[o].colliderNum = o;
                    }
                }
            }
        }

        GUI.enabled = false;
        EditorGUILayout.LabelField("Selection count: " + Selection.objects.Length);
    }
}