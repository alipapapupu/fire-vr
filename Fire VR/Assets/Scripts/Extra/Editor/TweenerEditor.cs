﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Tweener))]
[CanEditMultipleObjects]
public class TweenerEditor : Editor
{
    SerializedProperty mode;
    SerializedProperty actionTime;
    SerializedProperty autoPlay;
    SerializedProperty autoRestart;
    SerializedProperty playNext;
    SerializedProperty restartNext;
    SerializedProperty duration;
    SerializedProperty delay;
    SerializedProperty ignoreTimeScale;
    SerializedProperty loops;
    SerializedProperty loopType;
    SerializedProperty infiniteLoops;
    SerializedProperty id;
    SerializedProperty priorityId;
    SerializedProperty priority;
    SerializedProperty stopByHigher;
    SerializedProperty curve;
    SerializedProperty useStart;
    SerializedProperty startPos;
    SerializedProperty startRot;
    SerializedProperty startScale;
    SerializedProperty startTran;
    SerializedProperty startColor;
    SerializedProperty startShake;
    SerializedProperty endPos;
    SerializedProperty endRot;
    SerializedProperty endScale;
    SerializedProperty endTran;
    SerializedProperty endColor;
    SerializedProperty endShake;
    SerializedProperty useTran;
    SerializedProperty from;
    SerializedProperty localSpace;
    SerializedProperty relative;
    SerializedProperty useOnPlay;
    SerializedProperty useOnComplete;
    SerializedProperty useOnRewind;
    SerializedProperty useOnRewindComplete;
    SerializedProperty useOnAnyPlay;
    SerializedProperty useOnAnyComplete;
    SerializedProperty onPlay;
    SerializedProperty onComplete;
    SerializedProperty onRewind;
    SerializedProperty onRewindComplete;
    SerializedProperty onAnyPlay;
    SerializedProperty onAnyComplete;
    SerializedProperty usePhysics;
    SerializedProperty physicsLayer;
    SerializedProperty ignorePositions;
    SerializedProperty ignoreRotations;
    SerializedProperty ignoreScales;
    SerializedProperty ignoreColors;
    SerializedProperty ignoreShakes;
    SerializedProperty ignoreTransforms;
    SerializedProperty[] ignores;
    SerializedProperty useQuaternion;
    Vector3 position;
    string[] v = new string[] { "x", "y", "z" };
    string[] c = new string[] { "r", "b", "g", "a" };
    string[] t = new string[] { "position", "rotation", "scale" };
    string buttonName = "";

    void OnEnable()
    {
        mode = serializedObject.FindProperty("mode");
        actionTime = serializedObject.FindProperty("actionTime");
        autoPlay = serializedObject.FindProperty("autoPlay");
        playNext = serializedObject.FindProperty("playNext");
        autoRestart = serializedObject.FindProperty("autoRestart");
        restartNext = serializedObject.FindProperty("restartNext");
        duration = serializedObject.FindProperty("duration");
        delay = serializedObject.FindProperty("delay");
        ignoreTimeScale = serializedObject.FindProperty("ignoreTimeScale");
        loops = serializedObject.FindProperty("loops");
        loopType = serializedObject.FindProperty("loopType");
        infiniteLoops = serializedObject.FindProperty("infiniteLoops");
        id = serializedObject.FindProperty("id");
        priorityId = serializedObject.FindProperty("priorityId");
        priority = serializedObject.FindProperty("priority");
        stopByHigher = serializedObject.FindProperty("stopByHigher");
        curve = serializedObject.FindProperty("curve");
        useStart = serializedObject.FindProperty("useStart");
        startPos = serializedObject.FindProperty("startPos");
        startRot = serializedObject.FindProperty("startRot");
        startScale = serializedObject.FindProperty("startScale");
        startTran = serializedObject.FindProperty("startTran");
        startColor = serializedObject.FindProperty("startColor");
        startShake = serializedObject.FindProperty("startShake");
        endPos = serializedObject.FindProperty("endPos");
        endRot = serializedObject.FindProperty("endRot");
        endScale = serializedObject.FindProperty("endScale");
        endTran = serializedObject.FindProperty("endTran");
        endColor = serializedObject.FindProperty("endColor");
        endShake = serializedObject.FindProperty("endShake");
        useTran = serializedObject.FindProperty("useTran");
        from = serializedObject.FindProperty("from");
        localSpace = serializedObject.FindProperty("localSpace");
        relative = serializedObject.FindProperty("relative");
        useOnPlay = serializedObject.FindProperty("useOnPlay");
        useOnComplete = serializedObject.FindProperty("useOnComplete");
        useOnRewind = serializedObject.FindProperty("useOnRewind");
        useOnRewindComplete = serializedObject.FindProperty("useOnRewindComplete");
        useOnAnyPlay = serializedObject.FindProperty("useOnAnyPlay");
        useOnAnyComplete = serializedObject.FindProperty("useOnAnyComplete");
        onPlay = serializedObject.FindProperty("onPlay");
        onComplete = serializedObject.FindProperty("onComplete");
        onRewind = serializedObject.FindProperty("onRewind");
        onRewindComplete = serializedObject.FindProperty("onRewindComplete");
        onAnyPlay = serializedObject.FindProperty("onAnyPlay");
        onAnyComplete = serializedObject.FindProperty("onAnyComplete");
        usePhysics = serializedObject.FindProperty("usePhysics");
        physicsLayer = serializedObject.FindProperty("physicsLayers");
        ignorePositions = serializedObject.FindProperty("ignorePositions");
        ignoreRotations = serializedObject.FindProperty("ignoreRotations");
        ignoreScales = serializedObject.FindProperty("ignoreScales");
        ignoreColors = serializedObject.FindProperty("ignoreColors");
        ignoreShakes = serializedObject.FindProperty("ignoreShakes");
        ignoreTransforms = serializedObject.FindProperty("ignoreTransforms");
        useQuaternion = serializedObject.FindProperty("useQuaternion");

        if ((ignores == null))
        {
            ignores = new SerializedProperty[] { ignorePositions, ignoreRotations, ignoreScales, ignoreColors, ignoreShakes, ignoreTransforms };
        }
    }



    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        /*
        string playButtonName = (target as Tweener).on ? "Stop" : "Play";

        if (GUILayout.Button(playButtonName))
        {
            (target as Tweener).on = !(target as Tweener).on;

            if((target as Tweener).on)
            {
                position = (target as Tweener).transform.position;
            }
            else
            {
                (target as Tweener).transform.position = position;
            }
        }*/

        EditorGUIUtility.labelWidth = EditorGUIUtility.currentViewWidth * 0.3f;
        EditorGUIUtility.fieldWidth = EditorGUIUtility.currentViewWidth * 0.1f;
        GUILayoutOption width = GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.84f);

        GUILayoutOption halfWidth = GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.86f / 2);

        GUILayout.BeginHorizontal(width);
        EditorGUILayout.PropertyField(id);
        EditorGUILayout.PropertyField(priorityId);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal(width);
        EditorGUILayout.PropertyField(priority);
        EditorGUILayout.PropertyField(stopByHigher);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal(width);
        EditorGUILayout.PropertyField(mode);
        EditorGUILayout.PropertyField(actionTime);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal(width);
        EditorGUILayout.PropertyField(autoRestart);
        EditorGUILayout.PropertyField(restartNext);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal(width);
        EditorGUILayout.PropertyField(autoPlay);
        EditorGUILayout.PropertyField(playNext);
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal(width);
        EditorGUILayout.PropertyField(duration);
        EditorGUILayout.PropertyField(delay);
        GUILayout.EndHorizontal();



        EditorGUILayout.PropertyField(ignoreTimeScale, new GUIContent("Ignore Time"));

        if (mode.enumValueIndex == (int)Tweener.Mode.Position)
        {
            GUILayout.BeginHorizontal(width);
            EditorGUILayout.PropertyField(usePhysics);

            if (usePhysics.boolValue)
            {
                EditorGUILayout.PropertyField(physicsLayer);
            }

            GUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal(width);
        EditorGUILayout.PropertyField(infiniteLoops);
        if (!infiniteLoops.boolValue)
        {
            EditorGUILayout.PropertyField(loops);
        }
        GUILayout.EndHorizontal();

        if (loops.intValue > 0 || infiniteLoops.boolValue)
        {
            EditorGUILayout.PropertyField(loopType);
        }

        if (mode.enumValueIndex != (int)Tweener.Mode.RandomShake)
        {
            EditorGUILayout.PropertyField(curve);

            if (mode.enumValueIndex != (int)Tweener.Mode.Color)
            {
                EditorGUILayout.PropertyField(relative);
                EditorGUILayout.PropertyField(localSpace);

                if (mode.enumValueIndex != (int)Tweener.Mode.Transform)
                    EditorGUILayout.PropertyField(useTran);
            }

            if (mode.enumValueIndex == (int)Tweener.Mode.Rotation || mode.enumValueIndex == (int)Tweener.Mode.Transform)
                EditorGUILayout.PropertyField(useQuaternion);

            EditorGUILayout.PropertyField(useStart);

            buttonName = from.boolValue ? "From" : "To";


            GUILayoutOption op = GUILayout.Width(50);

            if (!useStart.boolValue)
            {
                GUILayout.BeginHorizontal();
                if (GUILayout.Button(buttonName))
                {
                    from.boolValue = !from.boolValue;
                }
            }
            else
            {
                if (GUILayout.Button(buttonName))
                {
                    from.boolValue = !from.boolValue;
                }
                GUILayout.Label(from.boolValue ? "End" : "Start");
                if (mode.enumValueIndex == (int)Tweener.Mode.Color)
                {
                    EditorGUILayout.PropertyField(startColor, GUIContent.none);
                }
                else
                {
                    if (useTran.boolValue || mode.enumValueIndex == (int)Tweener.Mode.Transform)
                    {
                        EditorGUILayout.PropertyField(startTran, GUIContent.none);
                    }
                    else if (mode.enumValueIndex == (int)Tweener.Mode.Position)
                    {
                        EditorGUILayout.PropertyField(startPos, GUIContent.none);
                    }
                    else if (mode.enumValueIndex == (int)Tweener.Mode.Rotation)
                    {
                        EditorGUILayout.PropertyField(startRot, GUIContent.none);
                    }
                    else if (mode.enumValueIndex == (int)Tweener.Mode.Scale)
                    {
                        EditorGUILayout.PropertyField(startScale, GUIContent.none);
                    }
                }
            }

            if (useStart.boolValue)
                GUILayout.Label(from.boolValue ? "Start" : "End");



            if (mode.enumValueIndex == (int)Tweener.Mode.Color)
            {
                EditorGUILayout.PropertyField(endColor, GUIContent.none);

                if (!useStart.boolValue)
                    GUILayout.EndHorizontal();
            }
            else
            {
                if (useTran.boolValue || mode.enumValueIndex == (int)Tweener.Mode.Transform)
                {
                    EditorGUILayout.PropertyField(endTran, GUIContent.none);
                }
                else if (mode.enumValueIndex == (int)Tweener.Mode.Position)
                {
                    EditorGUILayout.PropertyField(endPos, GUIContent.none);
                }
                else if (mode.enumValueIndex == (int)Tweener.Mode.Rotation)
                {
                    EditorGUILayout.PropertyField(endRot, GUIContent.none);
                }
                else if (mode.enumValueIndex == (int)Tweener.Mode.Scale)
                {
                    EditorGUILayout.PropertyField(endScale, GUIContent.none);
                }

                if (!useStart.boolValue)
                    GUILayout.EndHorizontal();
            }
        }
        else
        {
            EditorGUILayout.PropertyField(startShake);
            EditorGUILayout.PropertyField(endShake);
        }

        string[] used = v;

        switch (mode.enumValueIndex)
        {
            case (int)Tweener.Mode.Color:
                used = c;
                break;
            case (int)Tweener.Mode.Transform:
                used = t;
                break;
        }
        SerializedProperty sp = ignores[mode.enumValueIndex];

        GUILayout.Label("Ignore Values");
        GUILayout.BeginHorizontal(width);

        for (int i = 0; i < used.Length; i++)
        {
            SerializedProperty value = sp.GetArrayElementAtIndex(i);
            GUI.backgroundColor = value.boolValue ? Color.yellow : Color.white;

            if (GUILayout.Button(used[i], GUILayout.Width(EditorGUIUtility.currentViewWidth * 0.84f / used.Length)))
            {
                value.boolValue = !value.boolValue;
            }
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal(width);

        GUI.backgroundColor = useOnPlay.boolValue ? Color.yellow : Color.white;
        if (GUILayout.Button("OnPlay", halfWidth))
        {
            useOnPlay.boolValue = !useOnPlay.boolValue;
        }

        GUI.backgroundColor = useOnComplete.boolValue ? Color.yellow : Color.white;
        if (GUILayout.Button("OnComplete", halfWidth))
        {
            useOnComplete.boolValue = !useOnComplete.boolValue;
        }

        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();

        GUI.backgroundColor = useOnRewind.boolValue ? Color.yellow : Color.white;
        if (GUILayout.Button("OnRewind", halfWidth))
        {
            useOnRewind.boolValue = !useOnRewind.boolValue;
        }

        GUI.backgroundColor = useOnRewindComplete.boolValue ? Color.yellow : Color.white;
        if (GUILayout.Button("OnRewindcomplete", halfWidth))
        {
            useOnRewindComplete.boolValue = !useOnRewindComplete.boolValue;
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal(width);

        GUI.backgroundColor = useOnAnyPlay.boolValue ? Color.yellow : Color.white;
        if (GUILayout.Button("OnAnyPlay", halfWidth))
        {
            useOnAnyPlay.boolValue = !useOnAnyPlay.boolValue;
        }

        GUI.backgroundColor = useOnAnyComplete.boolValue ? Color.yellow : Color.white;
        if (GUILayout.Button("OnAnyComplete", halfWidth))
        {
            useOnAnyComplete.boolValue = !useOnAnyComplete.boolValue;
        }

        GUILayout.EndHorizontal();

        GUI.backgroundColor = Color.white;

        if (useOnPlay.boolValue)
        {
            EditorGUILayout.PropertyField(onPlay);
        }
        if (useOnComplete.boolValue)
        {
            EditorGUILayout.PropertyField(onComplete);
        }
        if (useOnRewind.boolValue)
        {
            EditorGUILayout.PropertyField(onRewind);
        }
        if (useOnRewindComplete.boolValue)
        {
            EditorGUILayout.PropertyField(onRewindComplete);
        }
        if (useOnAnyPlay.boolValue)
        {
            EditorGUILayout.PropertyField(onAnyPlay);
        }
        if (useOnAnyComplete.boolValue)
        {
            EditorGUILayout.PropertyField(onAnyComplete);
        }

        serializedObject.ApplyModifiedProperties();
    }
}
