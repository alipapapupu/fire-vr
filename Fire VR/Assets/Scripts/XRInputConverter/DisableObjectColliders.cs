﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class DisableObjectColliders : MonoBehaviour
{
    [SerializeField]
    InteractorController ic = null;
    [SerializeField]
    TeleportationProvider teleporter = null;
    Vector3 difference;

    void Awake()
    {
        teleporter.startLocomotion += DisableCollisions;
        teleporter.endLocomotion += EnableCollisions;
    }
    public void DisableCollisions(LocomotionSystem ls)
    {
        if (ic.processedInteractable)
        {
            difference = ic.processedInteractable.transform.position - transform.position;
            ic.processedInteractable.transform.GetComponent<ColliderDisabler>().ToggleColliders(false);
        }
    }

    public void EnableCollisions(LocomotionSystem ls)
    {
        if (ic.processedInteractable)
        {
            ic.processedInteractable.transform.position = transform.position + difference;
        }
        
        StartCoroutine(EnableCollisionsLate());
    }

    IEnumerator EnableCollisionsLate()
    {
        yield return null;
        if (ic.processedInteractable)
        {
            ic.processedInteractable.transform.position = transform.position + difference;
            ic.processedInteractable.transform.GetComponent<ColliderDisabler>().ToggleColliders(true);
        }
    }
}
