﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using static InputHelpers;

public class XRInputConverter
{
    public static InputFeatureUsage<bool> ButtonToInputFeatureUsage(Button button)
    {
        InputFeatureUsage<bool> result;
        switch (button)
        {
            case Button.Grip: result = CommonUsages.gripButton; break;
            case Button.Trigger: result = CommonUsages.triggerButton; break;
            case Button.PrimaryButton: result = CommonUsages.primaryButton; break;
            case Button.SecondaryButton: result = CommonUsages.secondaryButton; break;
            case Button.MenuButton: result = CommonUsages.menuButton; break;
            case Button.Primary2DAxisClick: result = CommonUsages.primary2DAxisClick; break;
        }

        return result;
    }
}
