﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Mirror))]
public class MirrorEditor : Editor
{
    SerializedProperty instantiateMirrorObject;

    public void OnEnable()
    {
        instantiateMirrorObject = serializedObject.FindProperty("InstantiateMirrorObject");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Mirror mirror = (Mirror)target;

        if (GUILayout.Button("Instantiate mirrored objects"))
        {
            mirror.InstantiateMirrorObjects();
        }
    }
}
