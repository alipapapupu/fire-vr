﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MohaviCreative.Sounds;

public class FireAlarm : MonoBehaviour
{
    bool turnedOn = false;
    Timer timer = new Timer(0.5f);
    [SerializeField]
    Material materialOn = null;
    [SerializeField]
    Material materialOff = null;
    bool lightOn = false;
    [SerializeField]
    new Renderer light = null;
    public void TurnOn()
    {
        if (turnedOn)
            return;

        turnedOn = true;
        GetComponent<SoundPlayer>().PlaySoundInstances(0);
        LevelManager.instance.fireAlarmPulled = true;
    }

    void Update()
    {
        if (turnedOn && timer.NextBool(true))
        {
            lightOn = !lightOn;
            light.material = lightOn ? materialOff : materialOn;
            timer.Restart();
        }
    }
}