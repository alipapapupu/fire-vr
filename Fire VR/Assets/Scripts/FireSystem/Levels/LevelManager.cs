using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
class Lose : UnityEvent<string>
{
}

public enum Defeat
{
    Fire,
    Electricity,
    Smoke,
    TooSlow,
    GreaseFireExplosion
}

public class LevelManager : MonoBehaviour
{
    [SerializeField]
    LevelData levelData = null;
    [SerializeField]
    int dataNumber = 0;
    [HideInInspector]
    public LevelData.Data currentData = null;

    public static LevelManager instance = null;
    [SerializeField]
    Lose winEvent = null;
    [SerializeField]
    Lose loseEvent = null;
    public static bool gameEnded = false;
    Dictionary<LevelData.ExtraCondition, bool> didExtra = new Dictionary<LevelData.ExtraCondition, bool>();
    [HideInInspector]
    public bool fireAlarmPulled = false, callEmergencyNumber = false;

    void Awake()
    {
        if (instance)
        {
            Debug.LogWarning("2 level instances in one level.");
            return;
        }

        instance = this;

        if (dataNumber > -1)
        {
            currentData = levelData.Levels[dataNumber];
        }

        Time.timeScale = 1;

        levelData.currentLevelIndex = dataNumber;

        gameEnded = false;
    }

    public static void WinS(LevelData.WinCondition w)
    {
        instance.Win(w);
    }

    public void Win(int i)
    {
        Win((LevelData.WinCondition)i);
    }

    public void Win(LevelData.WinCondition w)
    {
        if (gameEnded)
            return;


        if (w == currentData.winCondition)
        {
            Debug.Log("WIIIN");
            winEvent?.Invoke(currentData.levelName);

            gameEnded = true;
        }
    }

    public static void LoseS(Defeat d)
    {
        instance.Lose(d);
    }


    public void Lose(Defeat d)
    {
        if (gameEnded)
            return;

        string loseReason = d.ToString();

        Debug.Log(loseReason);
        loseEvent?.Invoke(loseReason);

        gameEnded = true;
    }

    public bool CheckExtraCondition(LevelData.ExtraCondition c)
    {
        switch (c)
        {
            case LevelData.ExtraCondition.Call112:
                return callEmergencyNumber;
            case LevelData.ExtraCondition.NoDamage:
                return PlayerHealth.instance.FullHealth();
            case LevelData.ExtraCondition.PullFireAlarm:
                return fireAlarmPulled;
            default:
                return true;
        }
    }
}
