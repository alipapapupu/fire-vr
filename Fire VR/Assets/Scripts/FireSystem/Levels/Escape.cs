﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Escape : MonoBehaviour
{
    [SerializeField]
    LayerMask mask = 0;
    Bounds bounds;
    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        bounds = GetComponent<BoxCollider>().bounds;
    }
    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        Collider[] colliders = Physics.OverlapBox(bounds.center, bounds.extents, transform.rotation, mask);

        foreach (Collider col in colliders)
        {
            if (col.tag == "Player")
            {
                LevelManager.WinS(LevelData.WinCondition.Escape);
                return;
            }
        }
    }

}
