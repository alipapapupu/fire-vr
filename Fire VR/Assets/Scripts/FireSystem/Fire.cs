﻿using UnityEngine;
using System.Collections.Generic;
using MohaviCreative.Sounds;

[RequireComponent(typeof(ParticleSystem))]
public class Fire : BaseFire
{
    delegate void PointProcessor(Vector2Int point, Color c);
    HeatMap[,] heatMap = null;
    Vector2 corners = Vector2.zero;
    Vector2 pointSize = Vector2.zero;
    Vector2 newMultiplier = Vector2.zero;
    Vector2Int amount = Vector2Int.zero;
    [Tooltip("Will the fire spread at all.")]
    public bool fireSpread = true;
    [Tooltip("How much the fire will increase itself.")]
    public float heatValueIncrease = 0;
    ParticleSystem ps = null;
    ParticleSystem.Particle[] particles = null;
    [Tooltip("What are the lowest and highest points of the heat")]
    public Vector2 heatClamp;
    [Tooltip("Changes the spread time. The script will take a time randomly between these values, and wait before trying to spread.")]
    public Vector2 spreadTimeClamp = Vector2.zero;
    [Tooltip("min and max sizes of the particles")]
    public Vector2 particleSizeClamp = Vector2.zero;
    [Tooltip("Size of the rectangles.")]
    public float baseSize = 0.5f;
    [Tooltip("Changes the size of particles")]
    public float sizeMultiplier = 0.01f;
    [Tooltip("Changes the time of particles")]
    public float timeMultiplier = 0;
    [Tooltip("Max distance to particles that are allowed to edit.")]
    public float maxDistance = 0;
    [Tooltip("Which points on the area are lit")]
    [SerializeField]
    Vector2Int[] litPoints = null;
    [Tooltip("Which areas on the whole area are lit")]
    [SerializeField]
    Area[] litAreas = null;
    [SerializeField]
    Area[] ignoreAreas = null;
    float currentTime = 0;
    float maxTime = 0;
    List<HeatMap> litHeatMaps = new List<HeatMap>();
    [Tooltip("What all objects can the fire start burning.")]
    [SerializeField]
    LayerMask fireable = 0;
    [Tooltip("Will the fire allow visual changes to the particles")]
    [SerializeField]
    bool visualChanges = true;
    [Tooltip("How much damage does the fire cause to the player.")]
    [SerializeField]
    float damageMultiplier = 1;

    void Start()
    {
        if (ps == null)
        {
            ps = GetComponent<ParticleSystem>();
        }

        if (particles == null || particles.Length < ps.main.maxParticles)
            particles = new ParticleSystem.Particle[ps.main.maxParticles];

        SetValues();

        heatMap = new HeatMap[(int)amount.x, (int)amount.y];

        EveryHeatMap(HeatmapInitialSet);

        CalculatePos(SetFire);
    }

    override protected void OnDisable()
    {
        base.OnDisable();
        SoundManager.SetGlobalValue("FireVolume", 0);
    }

    void Update()
    {
        if (PlayerHealth.instance)
        {
            Vector2Int pos = GetPoint(PlayerHealth.instance.transform.position, true);

            if (pos != Vector2Int.zero)
            {
                Vector2 fireDistanceClamp = new Vector2(1, 20);
                float distance = Vector3.Distance(PlayerHealth.instance.transform.position, heatMap[pos.x, pos.y].position3D);
                distance = Mathf.Clamp(distance, fireDistanceClamp.x, fireDistanceClamp.y);

                distance = 1 - ((distance / fireDistanceClamp.y) - fireDistanceClamp.x / fireDistanceClamp.y);


                SoundManager.SetGlobalValue("FireVolume", distance);
            }
        }

        if (!fireSpread)
            return;

        currentTime += Time.deltaTime;

        if (currentTime > maxTime)
        {
            currentTime -= maxTime;
            maxTime = Random.Range(spreadTimeClamp.x, spreadTimeClamp.y);
            for (int i = 0; i < litHeatMaps.Count; i++)
            {
                ChangeFire(litHeatMaps[i], heatValueIncrease);
                SpreadFire(litHeatMaps[i]);
                AddSmoke();
            }
        }
    }

    private void LateUpdate()
    {

        // GetParticles is allocation free because we reuse the particles buffer between updates
        int numParticlesAlive = ps.GetParticles(particles);

        // Change only the particles that are alive
        for (int i = 0; i < numParticlesAlive; i++)
        {
            EditParticle(i);
        }

        // Apply the particle changes to the Particle System
        ps.SetParticles(particles, numParticlesAlive);
    }

    Vector3 IncludeTransform(Vector3 v)
    {
        return transform.TransformPoint(v);
    }

    Vector3 IncludeTransform(Vector2 v)
    {
        return transform.TransformPoint(new Vector3(v.x, 0, v.y));
    }


    void SetFire(Vector2Int point, Color c)
    {
        ChangeFire(point, 1);
    }

    public void ChangeFire(Vector3 position, int distance, float change)
    {
        Vector2Int point = GetPoint(position);

        for (int x = point.x - distance; x <= point.x + distance; x++)
        {
            if (x < 0 || x >= amount.x)
                continue;

            for (int y = point.y - distance; y <= point.y + distance; y++)
            {
                if (y < 0 || y >= amount.y)
                    continue;

                ChangeFire(new Vector2Int(x, y), change);
            }
        }
    }

    void ChangeFire(Vector2Int point, float change)
    {
        ChangeFire(heatMap[point.x, point.y], change);
    }
    void ChangeFire(HeatMap h, float change)
    {
        h.heatValue = Mathf.Clamp(h.heatValue + change, heatClamp.x, heatClamp.y);

        Debug.DrawRay(h.position3D, Vector3.up, Color.black, 100);

        if (!h.lit && h.heatValue > 0)
        {
            litHeatMaps.Add(h);
            h.lit = true;

            if (!ps.isPlaying)
            {
                AddFire();
                ps.Play();
            }
        }
        else if (h.lit && h.heatValue <= 0)
        {
            litHeatMaps.Remove(h);
            h.lit = false;

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    Vector2Int point = new Vector2Int(x, y) + h.point;
                    point.x = Mathf.Clamp(point.x, 0, amount.x - 1);
                    point.y = Mathf.Clamp(point.y, 0, amount.y - 1);
                    heatMap[point.x, point.y].surrounded = false;
                }
            }

            if (litHeatMaps.Count == 0 && ps.isPlaying)
            {
                RemoveFire();
                ps.Stop();
            }
        }
    }

    override public void ChangeFire(Vector3 pos, float change, float distance, bool water)
    {
        foreach (HeatMap h in heatMap)
        {
            if (Distance2D(pos, h.position) < distance)
            {
                ChangeFire(h, change);
            }
        }
    }

    void EditParticle(int num)
    {
        if (!visualChanges ||
        (particles[num].startLifetime - Time.deltaTime) > particles[num].remainingLifetime)
            return;

        if (particles[num].position.y != 0)
            particles[num].position -= Vector3.up * particles[num].position.y;

        bool destroy = true;
        float heatValue = 0;
        float distance = maxDistance;

        Vector3 position = IncludeTransform(particles[num].position);

        List<HeatMap> tempLit = litHeatMaps.FindAll((h) => h.heatValue > 0 && Distance2D(h.position, position) < maxDistance);

        foreach (HeatMap h in tempLit)
        {
            if (h.heatValue <= 0)
                continue;

            float tempDistance = Distance2D(h.position, position);

            if (tempDistance < distance)
            {
                heatValue = h.heatValue;
                destroy = false;
                distance = tempDistance;
            }
        }

        if (destroy)
        {
            if (litHeatMaps.Count > 0)
            {
                Vector3 change = new Vector3(Random.Range(-maxDistance * 0.8f, maxDistance * 0.8f), 0, Random.Range(-maxDistance * 0.8f, maxDistance * 0.8f));
                Vector3 newPos = transform.worldToLocalMatrix * litHeatMaps[Random.Range(0, litHeatMaps.Count)].position;
                newPos += change;
                newPos.y = particles[num].position.y;
                particles[num].position = newPos;
                distance = change.magnitude;
            }
            else
            {
                particles[num].startLifetime = 0;
                ps.Stop();
            }
        }


        distance = (maxDistance - distance) / maxDistance;
        particles[num].startSize = Mathf.Clamp((heatValue * sizeMultiplier) * distance, particleSizeClamp.x, particleSizeClamp.y);
        particles[num].startLifetime = (heatValue * timeMultiplier) * distance;
    }

    void SpreadFire(HeatMap h)
    {
        if (h.heatValue < 0.5f)
            return;

        RaycastHit[] col = Physics.BoxCastAll(h.position3D, transform.TransformVector(new Vector3(pointSize.x, 0.1f, pointSize.y)), Vector3.up, transform.rotation, 10, fireable);

        foreach (RaycastHit hit in col)
        {
            FireObject fo = hit.transform.GetComponentInChildren<FireObject>();
            if (fo)
            {
                fo.StartBurn();
            }

            if (hit.transform.tag == "Player")
            {
                if (PlayerHealth.instance)
                {
                    PlayerHealth.instance.TakeDamage(h.heatValue * damageMultiplier * Time.deltaTime, Defeat.Fire);
                }
            }
        }

        if (h.surrounded)
            return;

        int lit = 0;
        for (int x = -1; x <= 1; x++)
        {
            if (h.point.x + x < 0 || h.point.x + x >= amount.x)
                continue;

            for (int y = -1; y <= 1; y++)
            {
                if (h.point.y + y < 0 || h.point.y + y >= amount.y)
                    continue;

                Vector2Int point = new Vector2Int(x, y) + h.point;
                point.x = Mathf.Clamp(point.x, 0, amount.x - 1);
                point.y = Mathf.Clamp(point.y, 0, amount.y - 1);
                HeatMap h2 = heatMap[point.x, point.y];
                float rValue = Random.value;

                if (h != h2 && rValue > 0.95f)
                    ChangeFire(h2, heatValueIncrease);

                if (h2.lit)
                    lit++;
            }
        }

        if (lit >= 6)
            h.surrounded = true;
    }

    void CalculatePos(PointProcessor pp)
    {
        for (int i = 0; i < litPoints.Length; i++)
        {
            Vector2Int point = new Vector2Int(Mathf.Clamp(litPoints[i].x, 0, amount.x - 1),
            Mathf.Clamp(litPoints[i].y, 0, amount.y - 1));

            pp.Invoke(point, Color.red);
        }

        DrawAreaGizmos(pp, litAreas, Color.red);
        DrawAreaGizmos(pp, ignoreAreas, Color.black);
    }

    void DrawAreaGizmos(PointProcessor pp, Area[] area, Color c)
    {
        for (int i = 0; i < litAreas.Length; i++)
        {
            int xDirection = litAreas[i].startPoint.x > litAreas[i].endPoint.x ? -1 : 1;
            int actualEndX = litAreas[i].endPoint.x + xDirection;

            for (int x = litAreas[i].startPoint.x; x != actualEndX; x += xDirection)
            {
                if (x < 0 || x >= amount.x)
                    continue;

                int yDirection = litAreas[i].startPoint.y > litAreas[i].endPoint.y ? -1 : 1;
                int actualEndY = litAreas[i].endPoint.y + yDirection;

                for (int y = litAreas[i].startPoint.y; y != actualEndY; y += yDirection)
                {
                    if (y < 0 || y >= amount.y)
                        continue;

                    Vector2Int point = new Vector2Int(x, y);

                    pp.Invoke(point, c);
                }
            }
        }
    }

    void HeatmapInitialSet(Vector2Int point, Color c)
    {
        HeatMap h = new HeatMap();
        heatMap[point.x, point.y] = h;
        h.point = point;
        Vector2 pos = new Vector2(corners.x * -1 + pointSize.x * point.x,
        corners.y * -1 + pointSize.y * point.y);
        pos += pointSize / 2;
        h.position3D = IncludeTransform(new Vector3(pos.x, 0, pos.y));
        h.position = new Vector2(h.position3D.x, h.position3D.z);
    }

    void EveryHeatMap(PointProcessor pp)
    {
        for (int x = 0; x < amount.x; x++)
        {
            for (int y = 0; y < amount.y; y++)
            {
                pp.Invoke(new Vector2Int(x, y), Color.red);
            }
        }
    }

    void DrawCube(Vector2Int point, Color c)
    {
        Color col = c;
        float heatValue = 1;

        if (heatMap != null && heatMap[point.x, point.y] != null)
        {
            heatValue = heatMap[point.x, point.y].heatValue;
            col = heatValue > 0 ? Color.red : Color.blue;
        }
        col.a = 0.5f;

        Gizmos.color = Color.Lerp(col - new Color(0, 0, 0, 1), col, Mathf.Abs(heatValue));
        Vector2 pos = new Vector2(corners.x * -1 + pointSize.x * point.x,
        corners.y * -1 + pointSize.y * point.y);
        pos += pointSize / 2;
        Gizmos.DrawCube(IncludeTransform(pos), transform.TransformVector(new Vector3(pointSize.x, 0.1f, pointSize.y) - Vector3.one * 0.01f));
    }

    void SetValues()
    {
        corners = Vector2.one / 2;

        Vector3Int tempAmount = Vector3Int.RoundToInt(transform.lossyScale / baseSize);
        amount = new Vector2Int(tempAmount.x, tempAmount.z);

        newMultiplier = new Vector2(1f / amount.x, 1f / amount.y);

        pointSize = new Vector2(newMultiplier.x, newMultiplier.y);
    }

    Vector2Int GetPoint(Vector3 position, bool onFire = false)
    {
        float distance = float.PositiveInfinity;
        Vector2Int point = Vector2Int.zero;

        foreach (HeatMap h in heatMap)
        {
            if (onFire && !h.lit)
                continue;

            float tempDistance = Distance2D(position, h.position);

            if (tempDistance < distance)
            {
                distance = tempDistance;
                point = h.point;
            }
        }

        return point;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        if (baseSize == 0)
            return;

        SetValues();

        Gizmos.DrawLine(IncludeTransform(new Vector2(corners.x * -1, corners.y)),
        IncludeTransform(new Vector2(corners.x, corners.y)));
        Gizmos.DrawLine(IncludeTransform(new Vector2(corners.x, corners.y * -1)),
        IncludeTransform(new Vector2(corners.x, corners.y)));
        Gizmos.DrawLine(IncludeTransform(new Vector2(corners.x * -1, corners.y * -1)),
        IncludeTransform(new Vector2(corners.x * -1, corners.y)));
        Gizmos.DrawLine(IncludeTransform(new Vector2(corners.x * -1, corners.y * -1)),
        IncludeTransform(new Vector2(corners.x, corners.y * -1)));

        for (int x = 1; x < amount.x; x++)
        {
            float xValue = corners.x * -1 + newMultiplier.x * x;
            Vector2 point1 = new Vector2(xValue, corners.y * -1);

            Vector2 point2 = new Vector2(xValue, corners.y);

            Gizmos.DrawLine(IncludeTransform(point1), IncludeTransform(point2));
        }

        for (int y = 1; y < amount.y; y++)
        {
            float yValue = corners.y * -1 + newMultiplier.y * y;
            Vector2 point1 = new Vector2(corners.x * -1, yValue);

            Vector2 point2 = new Vector2(corners.x, yValue);

            Gizmos.DrawLine(IncludeTransform(point1), IncludeTransform(point2));
        }

        if (Application.isPlaying)
        {
            EveryHeatMap(DrawCube);
        }
        else
        {
            CalculatePos(DrawCube);
        }
    }

    float Distance2D(Vector2 point1, Vector3 point2)
    {
        return Distance2D(new Vector3(point1.x, 0, point1.y), point2);
    }

    float Distance2D(Vector3 point1, Vector2 point2)
    {
        return Distance2D(point1, new Vector3(point2.x, 0, point2.y));
    }

    float Distance2D(Vector3 point1, Vector3 point2)
    {
        return Vector2.Distance(new Vector2(point1.x, point1.z), new Vector2(point2.x, point2.z));
    }

    Vector3 Multiply3D(Vector3 point1, Vector3 point2)
    {
        return new Vector3(point1.x * point2.x, point1.y * point2.y, point1.z * point2.z);
    }

    [System.Serializable]
    class Area
    {
        public Vector2Int startPoint = Vector2Int.zero;
        public Vector2Int endPoint = Vector2Int.zero;
    }

    class HeatMap
    {
        public Vector2Int point = Vector2Int.zero;
        public Vector2 position = Vector2.zero;
        public Vector3 position3D = Vector3.zero;
        public float heatValue = 0;
        public bool lit = false;
        public bool surrounded = false;
    }
}