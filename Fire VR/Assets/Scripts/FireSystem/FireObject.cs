﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MohaviCreative.Sounds;

public class FireObject : BaseFire
{
    public float burnTime = 1;
    public bool burning = false;
    Material[] materials = null;
    Timer timer = null;
    Color[] startColor;
    SkinnedMeshRenderer skinned = null;
    public bool burned = false;
    new ParticleSystem particleSystem = null;
    string previousTag = "";
    Renderer rend = null;

    void Start()
    {
        previousTag = transform.parent.tag;
        skinned = transform.parent.GetComponentInChildren<SkinnedMeshRenderer>();
        timer = new Timer(burnTime);

        rend = transform.parent.GetComponentInChildren<Renderer>();

        if (rend)
            materials = rend.materials;

        if (materials != null && materials.Length > 0)
        {
            startColor = new Color[materials.Length];

            for (int i = 0; i < materials.Length; i++)
            {
                //material[i] = Instantiate(material[i]);
                startColor[i] = materials[i].color;
            }
        }

        if (burning)
        {
            burning = false;
            StartBurn();
        }
    }

    void Update()
    {
        if (burning)
        {
            AddSmoke();
            timer.NextBool(true);

            if (materials != null)
            {
                for (int i = 0; i < materials.Length; i++)
                {
                    materials[i].color = Color.Lerp(startColor[i], Color.black, timer.GetFloat(true));
                }

                rend.materials = materials;
            }

            if (skinned)
            {
                skinned.SetBlendShapeWeight(0, timer.GetFloat(true) * 100);
            }
        }
    }

    public void StartBurn()
    {
        if (burning)
            return;

        AddFire();
        transform.parent.tag = "Fire";
        burning = true;

        GameObject gam = Pool.Create(0);

        gam.transform.parent = transform;
        gam.transform.localPosition = Vector3.zero;
        gam.transform.localEulerAngles = Vector3.zero;
        gam.transform.localScale = Vector3.one;

        particleSystem = gam.GetComponent<ParticleSystem>();
        ParticleSystem.ShapeModule module = particleSystem.shape;

        if (skinned)
        {
            module.shapeType = ParticleSystemShapeType.SkinnedMeshRenderer;
            module.skinnedMeshRenderer = skinned;
        }
        else
        {
            module.shapeType = ParticleSystemShapeType.MeshRenderer;
            module.meshRenderer = transform.parent.GetComponentInChildren<MeshRenderer>();
        }

        particleSystem.Play(true);
    }

    public void StopBurn()
    {
        if (!burning)
            return;

        //SoundManager.StaticStop(burningSound);
        RemoveFire();
        transform.parent.tag = previousTag;
        burning = false;
        particleSystem.Stop();
    }

    override public void ChangeFire(Vector3 pos, float change, float distance, bool water)
    {
        if (change > 0)
            StartBurn();
        else if (change < 0)
            StopBurn();
    }
}
