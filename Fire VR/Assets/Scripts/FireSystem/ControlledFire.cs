﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlledFire : BaseFire
{
    enum FireType
    {
        normal,
        grease,
        electric
    }

    [SerializeField]
    ParticleSystem greaseExplosion = null;
    [SerializeField]
    float explosionRange = 1;
    bool exploded = false;
    [SerializeField]
    FireType fireType = 0;
    [SerializeField]
    [Tooltip("How long it takes before the fire will start to spread")]
    float spreadTime = 1;
    [SerializeField]
    [Tooltip("Will the level fail if the fire spreads")]
    bool loseOnSpread = false;
    [SerializeField]
    [Tooltip("Will the level fail if the player causes grease explosion")]
    bool loseOnExplosion = false;
    Timer spreadTimer = null;
    ParticleSystem firePS = null;
    [SerializeField]
    bool on = true;

    void Start()
    {
        firePS = GetComponentInChildren<ParticleSystem>();
        spreadTimer = new Timer(spreadTime);

        if (on)
        {
            AddFire();
            firePS.Play();
        }
        else
            firePS.Stop();
    }

    public override void ChangeFire(Vector3 pos, float change, float distance, bool water)
    {
        if (!on && change < 0)
            return;

        if (water && fireType != FireType.normal)
        {
            SteamExplosion(pos);
            return;
        }
        else if (fireType == FireType.electric)
        {
            return;
        }
        ForceChangeFire(pos, change, distance, water);
    }

    public void ForceChangeFire(Vector3 pos, float change, float distance, bool water)
    {
        if (on && change < 0)
        {
            RemoveFire();
            firePS.Stop();
            on = false;
        }
        else if (!on && change > 0)
        {
            AddFire();
            firePS.Play();
            on = true;
        }
    }

    void Update()
    {
        if (on)
            AddSmoke();

        if (spreadTimer.OnceBool(true))
        {
            if (loseOnSpread)
            {
                LevelManager.LoseS(Defeat.TooSlow);
            }
        }
    }


    void SteamExplosion(Vector3 position)
    {
        if (exploded)
            return;

        exploded = true;
        ParticleSystem ps = Instantiate(greaseExplosion.gameObject).GetComponent<ParticleSystem>();
        ps.transform.position = position;

        soundPlayer.PlaySoundInstances(1);


        if (loseOnExplosion)
        {
            LevelManager.LoseS(Defeat.GreaseFireExplosion);
        }
    }

    void SpreadFire(Vector3 position, float range)
    {
        Collider[] colliders = Physics.OverlapSphere(position, range);

        foreach (Collider c in colliders)
        {
            FireObject fo = c.GetComponent<FireObject>();

            if (fo)
            {
                fo.StartBurn();
            }

            Fire f = c.GetComponent<Fire>();

            if (f)
            {
                f.ChangeFire(c.ClosestPoint(position), 5, 1);
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (fireType != FireType.electric || !on)
            return;

        if (col.tag == "LeftHand" || col.tag == "RightHand")
        {
            LevelManager.instance.Lose(Defeat.Electricity);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red - Color.black * 0.5f;

        Gizmos.DrawSphere(transform.position, explosionRange);
    }
}
