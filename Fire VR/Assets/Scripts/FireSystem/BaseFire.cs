﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MohaviCreative.Sounds;
using MohaviCreative;

[RequireComponent(typeof(SoundPlayer))]
public abstract class BaseFire : MonoBehaviour
{
    protected SoundPlayer soundPlayer = null;
    public abstract void ChangeFire(Vector3 pos, float change, float distance, bool water);
    public static List<BaseFire> fires = null;
    [Tooltip("How much smoke does the fire make every second")]
    [SerializeField]
    float smokeAmount = 1;

    protected virtual void Awake()
    {
        soundPlayer = GetComponent<SoundPlayer>();
    }

    protected void AddSmoke()
    {
        if (Smoke.smokes == null)
            return;

        foreach (Smoke s in Smoke.smokes)
        {
            s.AddSmoke(smokeAmount);
        }
    }

    protected virtual void OnDisable()
    {
        if (fires != null && fires.Contains(this))
        {
            fires.Remove(this);
            if (soundPlayer)
                soundPlayer.Stop(0);
        }
    }

    public void AddFire()
    {
        if (fires == null)
            fires = new List<BaseFire>();

        fires.Add(this);

        if (soundPlayer)
        {
            soundPlayer.PlaySoundInstances(0);
        }
    }

    public void RemoveFire()
    {
        fires.Remove(this);

        for (int i = 0; i < fires.Count; i++)
        {
            if (fires[i] == null)
            {
                fires.RemoveAt(i);
                i--;
            }
        }

        if (fires.Count == 0)
        {
            LevelManager.WinS(LevelData.WinCondition.PutOutFire);
        }

        if (soundPlayer)
            soundPlayer.Stop(0);
    }
}
