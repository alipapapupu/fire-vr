﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smoke : MonoBehaviour
{
    [SerializeField]
    bool changeSize = false;
    [SerializeField]
    ParticleSystem smoke = null;
    [Tooltip("How much fires affect amount")]
    [SerializeField]
    float amountMultiplier = 1;
    [Tooltip("How amount affect amount fof particles")]
    [SerializeField]
    float particleMultiplier = 10;
    [Tooltip("MinMax of the particle amount")]
    [SerializeField]
    Vector2 particleClamp = Vector2.zero;
    [SerializeField]
    float heightMultiplier = 10;
    [Tooltip("MinMax of the height")]
    [SerializeField]
    float maxAmount = 2;
    [SerializeField]
    float amount = 0;
    [SerializeField]
    float maxHeight = 1;
    public static List<Smoke> smokes = null;
    [SerializeField]
    LayerMask playerLayer = 0;
    [SerializeField]
    float damage = 0.01f;
    bool doubleSpeed = false;

    void Update()
    {
        if (amount > 1)
            CollideWithPlayer();
    }

    /// <summary>
    /// Callback to draw gizmos only if the object is selected.
    /// </summary>
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawWireCube(transform.position, transform.lossyScale);
    }

    void CollideWithPlayer()
    {
        Collider[] colliders = Physics.OverlapBox(transform.position, transform.lossyScale / 2, transform.rotation, playerLayer);

        foreach (Collider c in colliders)
        {
            if (c.tag == "Player")
            {
                PlayerHealth.instance.TakeDamage(damage * Time.deltaTime, Defeat.Smoke);
            }
        }
    }

    void OnEnable()
    {
        if (smokes == null)
            smokes = new List<Smoke>();

        if (!smokes.Contains(this))
            smokes.Add(this);
    }

    void OnDisable()
    {
        if (smokes == null)
            smokes = new List<Smoke>();

        if (smokes.Contains(this))
            smokes.Remove(this);
    }

    void OnValidate()
    {
        AddSmoke(0);
    }

    public void AddSmoke(float amount)
    {
        if (!smoke)
            return;

        if (Application.isPlaying && amount > 0)
            smoke.Play();

        float addAmount = amount * amountMultiplier * Time.deltaTime;

        if (doubleSpeed)
            addAmount *= 10;

        this.amount += addAmount;
        this.amount = Mathf.Min(this.amount, maxAmount);
        Vector3 smokePosition = transform.localPosition;

        if (changeSize)
            smokePosition = Vector3.zero;

        smokePosition.y = 0;
        smoke.transform.localPosition = smokePosition + Vector3.up * maxHeight;
        smoke.transform.localPosition += Vector3.down * this.amount * heightMultiplier / 2;
        Vector3 smokeSize = transform.localScale;

        if (changeSize)
            smokeSize = Vector3.one;

        smokeSize.y = 0;
        smoke.transform.localScale = smokeSize + Vector3.up * this.amount * heightMultiplier;
        ParticleSystem.EmissionModule emission = smoke.emission;
        emission.rateOverTime = Mathf.Clamp(this.amount * particleMultiplier, particleClamp.x, particleClamp.y);
    }

    public void DoubleSpeed(bool b)
    {
        doubleSpeed = b;
    }
}
