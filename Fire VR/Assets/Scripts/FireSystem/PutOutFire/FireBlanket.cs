﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBlanket : MonoBehaviour
{
    SphereCollider[,] spheres = null;
    List<ClothSphereColliderPair> pairs = new List<ClothSphereColliderPair>();
    int amount = 5;
    Vector3 topLeft = new Vector3(-1f, -1f, 0);
    Vector3 bottomRight = new Vector3(1f, 1f, 0);
    [SerializeField]
    float radius = 0.5f;
    Cloth cloth = null;
    Transform sphereParent = null;
    [SerializeField]
    LayerMask mask = 0;
    int[,,] points = null;
    [SerializeField]
    float effectiveness = 0;
    [SerializeField]
    float effectiveDistance = 0;
    int wallAmount = 2;
    GameObject[] wall = null;
    [SerializeField]
    float wallRadius = 0;
    [SerializeField]
    float wallThickness = 0;
    float wallHeight = 3;
    Collider[] pastWalls = null;
    [SerializeField]
    Transform center = null;
    [SerializeField]
    Transform[] handles = null;
    int[] handlePoints = null;
    Vector3[] handleDifference = null;
    int[] clothDistances = null;
    int amountOfPointsToChange = 9;
    float moveAmount = 0.5f;
    bool firstGrab = true;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        //Creating basic sphere to use
        sphereParent = new GameObject("sphereParent").transform;
        cloth = GetComponent<Cloth>();
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(sphere.GetComponent<MeshRenderer>());
        Destroy(sphere.GetComponent<MeshFilter>());
        sphere.layer = LayerMask.NameToLayer("Blanket");
        sphere.GetComponent<SphereCollider>().radius = radius * 0.5f;
        points = new int[amount, amount, 1];

        yield return null;

        //Create and place capsule colliders for walls
        float wallDistance = 1f;
        float wallWidth = 3;
        List<CapsuleCollider> capsules = new List<CapsuleCollider>();
        wall = new GameObject[wallAmount];
        handlePoints = new int[handles.Length];
        handleDifference = new Vector3[handles.Length];

        for (int i = 0; i < wallAmount; i++)
        {
            wall[i] = new GameObject("wall " + i);
            wall[i].layer = LayerMask.NameToLayer("Blanket");
            for (int o = 0; o < wallWidth; o++)
            {
                CapsuleCollider cc = wall[i].AddComponent<CapsuleCollider>();
                cc.radius = wallThickness;
                cc.height = wallHeight;
                Vector3 pos = Vector3.Lerp(Vector3.right * -1 * wallDistance,
                Vector3.right * wallDistance,
                (float)o / (wallWidth - 1));
                pos.y = wallHeight / 2;
                cc.center = pos;
                capsules.Add(cc);
            }
        }

        //Create and place sphere colliders for floor
        cloth.capsuleColliders = capsules.ToArray();

        spheres = new SphereCollider[amount, amount];

        for (int x = 0; x < amount; x++)
        {
            for (int y = 0; y < amount; y++)
            {
                GameObject gam = Instantiate(sphere, center);

                gam.transform.localPosition = Vector3.right * Mathf.Lerp(topLeft.x, bottomRight.x, (float)x / (amount - 1)) +
                Vector3.up * Mathf.Lerp(topLeft.y, bottomRight.y, (float)y / (amount - 1));

                spheres[x, y] = gam.GetComponent<SphereCollider>();

                if (x != 0)
                {
                    pairs.Add(new ClothSphereColliderPair(spheres[x, y], spheres[x - 1, y]));
                }
                if (y != 0)
                {
                    pairs.Add(new ClothSphereColliderPair(spheres[x, y], spheres[x, y - 1]));
                }
                if (x != 0 && y != 0)
                {
                    // pairs.Add(new ClothSphereColliderPair(spheres[x, y], spheres[x - 1, y - 1]));
                }

                gam.transform.SetParent(sphereParent);

                gam.transform.position -= Vector3.up * 2;
                gam.transform.localScale = Vector3.one;

                float distance = float.PositiveInfinity;

                for (int i = 0; i < cloth.vertices.Length; i++)
                {
                    Vector3 v = transform.TransformPoint(cloth.vertices[i]);

                    float tempDistance = Vector3.Distance(gam.transform.position, v);
                    if (tempDistance < distance)
                    {
                        points[x, y, 0] = i;
                        distance = tempDistance;
                    }
                }
            }
        }

        //Setting points for handles, and the locking points on cloth

        clothDistances = new int[amountOfPointsToChange * handles.Length];

        for (int o = 0; o < handles.Length; o++)
        {
            float[] floats = new float[amountOfPointsToChange];
            int[] closestPoints = new int[amountOfPointsToChange];
            float distance = float.PositiveInfinity;
            for (int i = 0; i < floats.Length; i++)
            {
                floats[i] = float.PositiveInfinity;
            }
            for (int i = 0; i < cloth.vertices.Length; i++)
            {
                Vector3 v = transform.TransformPoint(cloth.vertices[i]);

                float tempDistance = Vector3.Distance(handles[o].position, v);
                if (tempDistance < distance)
                {
                    handlePoints[o] = i;
                    handleDifference[o] = handles[o].position - v;
                    distance = tempDistance;
                }

                int tester = i;

                for (int a = 0; a < floats.Length; a++)
                {

                    if (tempDistance < floats[a])
                    {
                        float temp = floats[a];
                        int t = closestPoints[a];
                        floats[a] = tempDistance;
                        closestPoints[a] = tester;
                        tempDistance = temp;
                        tester = t;
                        a = -1;
                    }
                }
            }

            for (int i = 0; i < closestPoints.Length; i++)
            {
                clothDistances[o * amountOfPointsToChange + i] = closestPoints[i];
            }
        }


        cloth.sphereColliders = pairs.ToArray();
        Destroy(sphere);

        cloth.enabled = false;
        GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 100);
    }

    // Update is called once per frame
    void Update()
    {
        CalculatePosition();

        CalculateCollider();

        CalculateWall();
    }

    void CalculatePosition()
    {
        Vector3 sum = Vector3.zero;
        foreach (Vector3 v in cloth.vertices)
        {
            sum += v;
            Vector3 v2 = center.rotation * v;
            //Debug.DrawRay(v2 + transform.position, Vector3.up, Color.green);
        }

        sum /= cloth.vertices.Length;
        sum = center.rotation * sum;
        sum.y += 0.4f;

        sphereParent.position = Vector3.Slerp(sphereParent.position, transform.position + sum, moveAmount);

        if (handlePoints != null)
        {
            for (int i = 0; i < handles.Length; i++)
            {
                handles[i].position = transform.TransformPoint(cloth.vertices[handlePoints[i]]) + handleDifference[i];
                handles[i].transform.up = transform.TransformDirection(cloth.normals[handlePoints[i]]);
            }
        }
    }

    void CalculateCollider()
    {
        for (int x = 0; x < amount; x++)
        {
            for (int y = 0; y < amount; y++)
            {
                if (spheres == null || !spheres[x, y])
                    break;

                Vector3 pos = spheres[x, y].transform.position;
                Vector3 origin = PointPos(x, y);

                RaycastHit[] cols = Physics.SphereCastAll(origin, radius, Vector3.down, radius * 3, mask);
                float distance = float.PositiveInfinity;

                if (cols.Length > 0)
                {
                    for (int i = 0; i < cols.Length; i++)
                    {
                        RaycastHit hit = cols[i];

                        if (hit.distance < distance)
                        {
                            distance = hit.distance;
                            //Debug.DrawLine(PointPos(x, y), hit.point, Color.red);
                            pos.y = origin.y - hit.distance - (radius * 1.5f);
                        }

                        if (hit.distance < radius * 2)
                        {
                            if (hit.collider.GetComponent<BaseFire>())
                            {
                                hit.collider.GetComponent<BaseFire>().ChangeFire(hit.point, -effectiveness, effectiveDistance, false);
                            }
                        }
                    }
                }
                else
                {
                    pos.y = sphereParent.position.y - radius * 2;
                }

                spheres[x, y].transform.position = Vector3.Slerp(spheres[x, y].transform.position, pos, moveAmount);
            }
        }
    }

    void CalculateWall()
    {
        Collider[] col = new Collider[wallAmount];
        for (int i = 0; i < wallAmount; i++)
        {
            if (wall == null || !wall[i])
                continue;

            float distance = float.PositiveInfinity;
            Vector3 contactPoint = Vector3.zero;
            bool cols = false;

            foreach (Collider c in Physics.OverlapSphere(sphereParent.position, wallRadius, LayerMask.GetMask("Wall")))
            {
                bool walled = false;

                for (int o = 0; o < wallAmount; o++)
                {
                    if (i != o && (pastWalls[o] != null && c == pastWalls[o]) ||
                    (col[o] != null && c == col[o]))
                    {
                        walled = true;
                        break;
                    }
                }

                if (walled)
                    continue;

                Vector3 tempContactPoint = c.ClosestPoint(sphereParent.position);
                float tempDistance = Vector3.Distance(tempContactPoint, sphereParent.position);

                if (tempDistance < distance)
                {
                    cols = true;
                    distance = tempDistance;
                    contactPoint = tempContactPoint;
                    col[i] = c;
                }
            }

            if (cols)
            {
                Vector3 normal = (contactPoint - sphereParent.position).normalized;

                if (Physics.Raycast(sphereParent.position, normal,
                out RaycastHit hit, wallRadius + 0.5f, LayerMask.GetMask("Wall")))
                {
                    normal.y = 0;
                    normal.Normalize();
                    Vector3 newPos = hit.point + normal * wallThickness;
                    newPos.y = col[i].bounds.min.y;
                    wall[i].transform.position = newPos;
                    wall[i].transform.forward = normal;
                    Vector3 scale = wall[i].transform.localScale;
                    scale.y = col[i].bounds.size.y / wallHeight;
                    wall[i].transform.localScale = scale;
                }
            }
            else
            {
                wall[i].transform.position = sphereParent.position - Vector3.up * 100;
            }
        }

        pastWalls = col;
    }

    public void Grab(bool b)
    {
        if (firstGrab)
        {
            firstGrab = false;
            GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, 0);
            cloth.enabled = true;
        }

        ClothSkinningCoefficient[] c = cloth.coefficients;

        for (int i = 0; i < clothDistances.Length; i++)
        {
            c[clothDistances[i]].maxDistance = b ? 0 : 10;
        }

        cloth.coefficients = c;

        //StartCoroutine(Test(b));
    }

    Vector3 PointPos(Vector2Int point)
    {
        Vector3 position = transform.rotation * cloth.vertices[points[point.x, point.y, 0]] + transform.position;
        position += Vector3.up * (radius * 2);
        return position;
    }

    Vector3 PointPos(int x, int y)
    {
        return PointPos(new Vector2Int(x, y));
    }
    /*
        IEnumerator Test(bool b)
        {
            float[] c1 = b ? clothDistances1 : clothDistances2;
            float[] c2 = !b ? clothDistances1 : clothDistances2;
            for (float i = 0; i <= 2; i += Time.deltaTime)
            {
                yield return null;
                for (int o = 0; o < c1.Length; o++)
                {
                    clothDistances3[o].maxDistance = Mathf.Lerp(c1[o], c2[o], i / 10);
                }
                cloth.coefficients = clothDistances3;
            }
        }
    */

    void OnDrawGizmosSelected()
    {
        if (sphereParent)
        {
            Gizmos.DrawSphere(sphereParent.position, 0.4f);
            Gizmos.color = Color.black;
            Gizmos.DrawWireSphere(sphereParent.position, wallRadius);
        }
    }
}
