﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleFireOff : MonoBehaviour
{
    public float changeValue = 0;
    public float changeDistance = 0;
    public bool liquid = true;

    void Start()
    {
        ParticleSystem[] ps = GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem p in ps)
        {
            p.gameObject.AddComponent<LilParticleOff>().parent = this;
        }
    }
}
