﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateHose : MonoBehaviour
{
    // Start is called before the first frame update
    List<ConfigurableJoint> joints = null;
    public float springDamp = 0;
    public float spring = 0;
    public float limit = 0;
    public float bounciness = 0;
    public float contactDistance = 0;
    public float projectionDistance = 0;
    public Transform grabbable = null;
    List<Transform> parents = null;
    List<float> magnitudes = null;
    float maxMagnitude = 0;
    List<Vector3> localPos = null;
    bool grabbed = false;
    public Rigidbody hose = null;
    IEnumerator Start()
    {
        parents = new List<Transform>();
        magnitudes = new List<float>();
        localPos = new List<Vector3>();
        Vector3 direction = Vector3.zero;

        joints = new List<ConfigurableJoint>();
        int i = 0;
        foreach (Transform t in GetComponentsInChildren<Transform>())
        {
            i++;
            if (t == transform)
            {
                t.gameObject.AddComponent<Rigidbody>().isKinematic = true;
            }
            else
            {
                localPos.Add(t.localPosition);
                parents.Add(t.parent);
                float magnitude = (t.position - t.parent.position).magnitude*0.9f;
                magnitudes.Add(magnitude);
                maxMagnitude += magnitude;

                if (i > 4 && t.GetComponent<Collider>() == null)
                    t.gameObject.AddComponent<BoxCollider>().size = new Vector3(0.01f, 0.015f, 0.01f);

                if (t == hose.transform)
                {
                    direction = t.position - transform.position;
                    break;
                }
            }
        }

        yield return null;
        foreach (Transform t in GetComponentsInChildren<Transform>())
        {
            if (t != transform)
            {
                ConfigurableJoint cj = t.gameObject.AddComponent<ConfigurableJoint>();
                cj.axis = Vector3.up;
                cj.secondaryAxis = Vector3.right;
                cj.connectedBody = t.parent.GetComponent<Rigidbody>();
                cj.xMotion = ConfigurableJointMotion.Locked;
                cj.yMotion = ConfigurableJointMotion.Locked;
                cj.zMotion = ConfigurableJointMotion.Locked;
                cj.projectionMode = JointProjectionMode.PositionAndRotation;
                cj.angularXMotion = ConfigurableJointMotion.Limited;
                joints.Add(cj);

                if (t == hose.transform)
                    break;
            }
        }

    }

    void Update()
    {

        if (joints == null || joints.Count == 0)
            return;

        bool changeValues = joints[0].linearLimitSpring.damper != springDamp ||
        joints[0].linearLimitSpring.spring != spring ||
        joints[0].linearLimit.bounciness != bounciness ||
        joints[0].linearLimit.limit != limit ||
        joints[0].linearLimit.contactDistance != contactDistance ||
        joints[0].projectionDistance != projectionDistance;

        if (changeValues)
        {
            SoftJointLimitSpring sjls = new SoftJointLimitSpring();

            sjls.damper = springDamp;
            sjls.spring = spring;

            SoftJointLimit sjl = new SoftJointLimit();
            sjl.limit = limit;
            sjl.bounciness = bounciness;
            sjl.contactDistance = contactDistance;
            for (int i = 0; i < joints.Count; i++)
            {
                joints[i].linearLimitSpring = sjls;
                joints[i].linearLimit = sjl;
                joints[i].projectionDistance = projectionDistance;
            }
        }

        if (grabbed)
        {
            Vector3 way = grabbable.position - transform.position;
            float distance = Mathf.Min(maxMagnitude, way.magnitude);
            hose.transform.position = transform.position + way.normalized * distance;
            hose.transform.rotation = grabbable.rotation;
        }
    }

    void LateUpdate()
    {
        for (int i = 0; i < joints.Count-1; i++)
        {
            Vector3 pos = (joints[i].transform.position - parents[i].position);
            if (pos.magnitude > magnitudes[i])
            {
                joints[i].transform.position = Vector3.Lerp(joints[i].transform.position, parents[i].position + pos.normalized * magnitudes[i], 0.1f);
            }
        }
    }

    public void Grab(bool b)
    {
        hose.isKinematic = b;
        grabbed = b;
    }
}

