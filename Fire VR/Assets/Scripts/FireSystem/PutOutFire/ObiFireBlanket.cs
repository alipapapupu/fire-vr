﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class ObiFireBlanket : MonoBehaviour
{
    [SerializeField]
    LayerMask mask = 0;
    Transform clothObject = null;
    [SerializeField]
    ObiParticleAttachment locker = null;
    [SerializeField]
    int amount = 1;
    [SerializeField]
    float castSize = 1;
    [SerializeField]
    float effectiveness = 0;
    [SerializeField]
    float effectiveDistance = 0;
    List<int> leftHandle = null;
    List<int> rightHandle = null;

    [SerializeField]
    Transform leftCollider = null;
    [SerializeField]
    Transform rightCollider = null;
    Renderer rend = null;
    ObiCloth cloth = null;
    [SerializeField]
    GameObject fakeCloth = null;
    bool firstGrab = true;
    [SerializeField]
    GameObject clothCover = null;

    List<FireChecker> checkers = new List<FireChecker>();
    // Start is called before the first frame update
    IEnumerator Start()
    {
        clothObject = locker.gameObject.transform;
        cloth = clothObject.GetComponent<ObiCloth>();

        transform.parent.parent = null;

        locker.enabled = false;

        ObiActorBlueprint blueprint = locker.particleGroup.m_Blueprint;
        leftHandle = blueprint.groups.Find((ObiParticleGroup o) => o.name == "Left").particleIndices;
        rightHandle = blueprint.groups.Find((ObiParticleGroup o) => o.name == "Right").particleIndices;

        if (!rend)
        {
            rend = clothObject.GetComponent<MeshRenderer>();
        }


        yield return null;

        CreateFireChecker();
        clothObject.gameObject.SetActive(false);
        fakeCloth.SetActive(true);
        clothCover.transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        CheckFire();
        PositionCollider(leftHandle, leftCollider);
        PositionCollider(rightHandle, rightCollider);
    }

    void PositionCollider(List<int> points, Transform collider)
    {
        if (!rend)
            return;

        Vector3 newPos = Vector3.zero;

        for (int i = 0; i < points.Count; i++)
        {
            newPos += cloth.GetParticlePosition(points[i]);
        }

        newPos /= points.Count;

        collider.position = newPos;

        int o = 4;
        Vector3 forward = Vector3.Cross((cloth.GetParticlePosition(points[o]) - cloth.GetParticlePosition(points[o + 1])).normalized,
        (cloth.GetParticlePosition(points[o + 1]) - cloth.GetParticlePosition(points[o + 2])).normalized);
        Vector3 up = Vector3.Cross(forward, (cloth.GetParticlePosition(points[o]) - cloth.GetParticlePosition(points[o + 1])).normalized);

        collider.rotation = Quaternion.LookRotation(up, forward);
    }

    public void Grab(bool b)
    {

        if (b && firstGrab)
        {
            firstGrab = false;

            fakeCloth.SetActive(false);
            clothObject.gameObject.SetActive(transform);
        }

        locker.enabled = b;
    }

    void CheckFire()
    {
        foreach (FireChecker fc in checkers)
        {

            RaycastHit[] cols = Physics.SphereCastAll(GetPoint(fc), castSize, Vector3.down, 0.1f, mask);

            foreach (RaycastHit hit in cols)
            {
                if (hit.collider.GetComponent<BaseFire>())
                {
                    hit.collider.GetComponent<BaseFire>().ChangeFire(hit.point, -effectiveness, effectiveDistance, false);
                }
            }
        }
    }

    void CreateFireChecker()
    {
        if (!rend)
            return;

        float realAmount = (amount - 1) / 2f;

        for (float x = -realAmount; x <= realAmount; x++)
        {
            for (float y = -realAmount; y <= realAmount; y++)
            {
                Vector3 extends = transform.InverseTransformVector(rend.bounds.extents);
                extends.x *= x / realAmount;
                extends.z *= y / realAmount;

                Vector3 pos = transform.TransformVector(extends) + rend.bounds.center;

                float distance = float.PositiveInfinity;
                int vertice = 0;

                for (int i = 0; i < cloth.particleCount; i++)
                {
                    float tempDistance = Vector3.Distance(pos, cloth.GetParticlePosition(i));
                    if (tempDistance < distance)
                    {
                        distance = tempDistance;
                        vertice = i;
                    }
                }

                Debug.DrawRay(pos, Vector3.up, Color.black, 1000);

                checkers.Add(new FireChecker(vertice, pos, cloth));
            }
        }
    }

    /// <summary>
    /// Callback to draw gizmos only if the object is selected.
    /// </summary>
    void OnDrawGizmosSelected()
    {
        if (checkers.Count == 0)
        {
            CreateFireChecker();
        }

        foreach (FireChecker fc in checkers)
        {
            Gizmos.color = Color.blue;

            Gizmos.DrawWireSphere(GetPoint(fc), castSize);
        }
    }

    Vector3 GetPoint(FireChecker fc)
    {
        return fc.position;
    }

    /// <summary>
    /// Called when the script is loaded or a value is changed in the
    /// inspector (Called in the editor only).
    /// </summary>
    void OnValidate()
    {
        checkers = new List<FireChecker>();
    }

    class FireChecker
    {
        int vertice = 0;
        Vector3 offset = Vector3.zero;
        ObiCloth cloth = null;
        public FireChecker(int vertice, Vector3 position, ObiCloth cloth)
        {
            this.vertice = vertice;
            this.cloth = cloth;
            offset = position - cloth.GetParticlePosition(vertice);
        }

        public Vector3 position
        {
            get { return offset + cloth.GetParticlePosition(vertice); }
        }
    }
}
