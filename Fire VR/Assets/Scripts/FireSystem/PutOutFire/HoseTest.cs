﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoseTest : MonoBehaviour
{
    float distance = 0.02f;
    Transform parent = null;
    void Start()
    {
        parent = transform.parent;
        distance = (transform.position - parent.position).magnitude;
    }
    void LateUpdate()
    {
        Vector3 pos = (transform.position - parent.position);
        if (pos.magnitude > distance)
        {
            transform.position = parent.position + pos.normalized * distance;
        }
    }
}
