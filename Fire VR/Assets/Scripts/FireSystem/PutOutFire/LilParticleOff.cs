﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LilParticleOff : MonoBehaviour
{
    [HideInInspector]
    public ParticleFireOff parent = null;
    ParticleSystem ps = null;
    List<ParticleCollisionEvent> collisionEvents;

    void Start()
    {
        ps = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }
    void OnParticleCollision(GameObject other)
    {
        if (other.tag != "Fire")
            return;

        BaseFire fire = other.GetComponentInChildren<BaseFire>();

        if (!fire)
            return;

        ps.GetCollisionEvents(other, collisionEvents);

        foreach (ParticleCollisionEvent pe in collisionEvents)
        {
            fire.ChangeFire(pe.intersection, parent.changeValue, parent.changeDistance, parent.liquid);
        }
    }
}
