﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireExtinguishers : MonoBehaviour
{
    [SerializeField]
    ParticleSystem ps = null;

    bool on = false;
    bool safetyPinOn = true;

    public void TurnOnOff(bool on)
    {
        if (safetyPinOn)
            return;

        this.on = on;

        if (on)
            ps.Play();
        else
            ps.Stop();
            
    }

    public void SafetyPin()
    {
        safetyPinOn = false;
    }
}
