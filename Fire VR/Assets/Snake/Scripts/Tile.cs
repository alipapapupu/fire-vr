﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile {
    public int x;

    public int y;

    public MapObject mapObject;

    public Tile(int x, int y, MapObject mapObject) {
        this.x = x;
        this.y = y;
        this.mapObject = mapObject;
    }
}
