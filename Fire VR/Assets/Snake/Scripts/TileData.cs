﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class TileData : ScriptableObject {
    [SerializeField]
    private TileBase snakeBody = null;

    [SerializeField]
    private TileBase snakeHead = null;

    [SerializeField]
    public TileBase background = null;

    [SerializeField]
    private TileBase fruit = null;

    [SerializeField]
    public TileBase transParent = null;

    public TileBase GetTile(MapObject name) {
        switch (name) {
            case MapObject.Snake: return snakeBody;
            case MapObject.SnakeHead: return snakeHead;
            case MapObject.Ground: return transParent;
            case MapObject.Fruit: return fruit;
        }
        return null;
    }
}