﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MapObject {
    Ground,
    Snake,
    SnakeHead,
    Fruit
}