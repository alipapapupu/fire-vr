﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class SnakeGame : MonoBehaviour {
    [SerializeField]
    private TileData tileData = null;

    [SerializeField]
    private Tilemap background = null;

    [SerializeField]
    private Tilemap foreground = null;

    private int mapHeight;
    private int mapWidth;

    public Tile[,] map;

    public List<Tile> snakePositions;

    public Tile fruitTile;

    private Direction lastDirection;
    public Direction snakeDirection;

    private float moveDelay;

    [SerializeField]
    private int speedMulti = 1;

    private float moveTimer;

    public int score;
    [SerializeField]
    private TextMeshProUGUI scoreText = null;

    public int highScore;

    public bool gameIsOn;

    public bool alive;

    private void Start() {
        InitialiseGame();
        gameIsOn = false;
    }

    public void InitialiseGame() {
        moveDelay = 0.12f;
        moveTimer = moveDelay;

        InitialiseTiles();
        RenderMaps();

        snakeDirection = Direction.Up;
        lastDirection = Direction.Up;
        gameIsOn = true;
        alive = true;

        if (scoreText != null)
        {
            scoreText.text = "" + score;
        }
    }

    private void Update() {
        if (!alive) {
            gameIsOn = false;
        }

        if (gameIsOn) {
            if (moveTimer <= 0) {
                moveTimer = moveDelay/speedMulti;
                MoveSnake();
                RenderMaps();
            }

            moveTimer -= Time.deltaTime;
        }
    }

    public void SetSnakeDirection(int direction)
    {
        snakeDirection = (Direction)direction;
    }

    private void MoveSnake() {
        Tile snakeHead = snakePositions[snakePositions.Count - 1];
        snakePositions[snakePositions.Count - 1].mapObject = MapObject.Snake;

        int[] nCoordinates = new int[2];

        switch (snakeDirection) {
            case Direction.Up: nCoordinates[0] = snakeHead.x; nCoordinates[1] = snakeHead.y + 1; break;
            case Direction.Down: nCoordinates[0] = snakeHead.x; nCoordinates[1] = snakeHead.y - 1; break;
            case Direction.Left: nCoordinates[0] = snakeHead.x - 1; nCoordinates[1] = snakeHead.y; break;
            case Direction.Right: nCoordinates[0] = snakeHead.x + 1; nCoordinates[1] = snakeHead.y; break;
        }

        if (nCoordinates[0] >= 0 && nCoordinates[0] < mapWidth && nCoordinates[1] >= 0 && nCoordinates[1] < mapHeight) {
            Tile nSnakeHead = map[nCoordinates[1], nCoordinates[0]];

            if (nSnakeHead.mapObject == MapObject.Fruit) {
                SpawnFruit();
                score++;
                if (scoreText != null)
                {
                    scoreText.text = "" + score;
                }
            } else if (nSnakeHead.mapObject == MapObject.Snake) {
                EndGame();
            } else {
                snakePositions[0].mapObject = MapObject.Ground;
                snakePositions.RemoveAt(0);
            }

            snakePositions.Add(nSnakeHead);
            nSnakeHead.mapObject = MapObject.SnakeHead;

            lastDirection = snakeDirection;
        } else {
            EndGame();
        }
    }

    private void SpawnFruit() {
        List<Tile> tiles = new List<Tile>();

        foreach (Tile tile in map) {
            if (tile.mapObject == MapObject.Ground) {
                tiles.Add(tile);
            }
        }

        int fruitTileIndex = Random.Range(0, tiles.Count);

        tiles[fruitTileIndex].mapObject = MapObject.Fruit;
        fruitTile = tiles[fruitTileIndex];
    }

    private void EndGame() {
        alive = false;

        if (score > highScore) {
            highScore = score;
        }

        score = 0;
    }

    private void InitialiseTiles() {
        mapHeight = 40;
        mapWidth = 32;

        map = new Tile[mapHeight, mapWidth];
        snakePositions = new List<Tile>();
        
        for (int y = 0; y < mapHeight; y++) {
            for (int x = 0; x < mapWidth; x++) {
                Tile tile = map[y, x] = new Tile(x, y, MapObject.Ground);
                if (x == mapWidth / 2 && (y == 1 || y == 2)) {
                    tile.mapObject = MapObject.Snake;
                    snakePositions.Add(tile);
                } else if (x == mapWidth / 2 && y == 3) {
                    tile.mapObject = MapObject.SnakeHead;
                    snakePositions.Add(tile);
                } else {
                    tile.mapObject = MapObject.Ground;
                }
            }
        }

        for (int y = 0; y < mapHeight; y++) {
            for (int x = 0; x < mapWidth; x++) {
                Vector3Int position = new Vector3Int(x - mapWidth/2, y - mapHeight/2, 0);

                background.SetTile(position, tileData.background);
                foreground.SetTile(position, tileData.transParent);
            }
        }

        SpawnFruit();
    }

    private void RenderMaps() {
        for (int y = 0; y < mapHeight; y++) {
            for (int x = 0; x < mapWidth; x++) {
                Vector3Int position = new Vector3Int(x - mapWidth/2, y - mapHeight/2, 0);

                if (!foreground.GetTile(position).Equals(tileData.GetTile(map[y, x].mapObject))) {
                    foreground.SetTile(position, tileData.GetTile(map[y, x].mapObject));
                }
            }
        }
    }
}