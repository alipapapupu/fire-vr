﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Bool", menuName = "Variables/Bool")]
public class BoolVariable : ScriptableVariable<bool>
{
    
}