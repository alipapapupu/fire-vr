﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameSettings : ScriptableObject
{
    [SerializeField]
    public FloatVariable masterVolume;

    [SerializeField]
    public FloatVariable soundVolume;

    [SerializeField]
    public FloatVariable musicVolume;

    [SerializeField]
    public BoolVariable teleportEnabled;

    [SerializeField]
    public BoolVariable leftHandedControls;
}