﻿Shader "Custom/Mask"
{
	Properties{}

	SubShader{

		Tags {
			"RenderType" = "Opaque"
			"Queue"="Geometry"
		}

		Pass {
			ZWrite Off
			Stencil {
                Ref 1
                Comp always
                Pass replace
            }
		}
	}
}