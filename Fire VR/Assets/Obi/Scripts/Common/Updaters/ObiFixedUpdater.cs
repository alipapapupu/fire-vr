using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Obi
{
    /// <summary>
    /// Updater class that will perform simulation during FixedUpdate(). This is the most physically correct updater,
    /// and the one to be used in most cases. Also allows to perform substepping, greatly improving convergence.

    [AddComponentMenu("Physics/Obi/Obi Fixed Updater", 801)]
    [ExecuteInEditMode]
    public class ObiFixedUpdater : ObiUpdater
    {
        /// <summary>
        /// Each FixedUpdate() call will be divided into several substeps. Performing more substeps will greatly improve the accuracy/convergence speed of the simulation. 
        /// Increasing the amount of substeps is more effective than increasing the amount of constraint iterations.
        /// </summary>
        [Tooltip("Amount of substeps performed per FixedUpdate. Increasing the amount of substeps greatly improves accuracy and convergence speed.")]
        public int substeps = 4;

        private float accumulatedTime;
        [SerializeField, Tooltip("How many fixedUpdates should the program ignore.")]
        int ingoreSteps = 0;

        int allIgnoreSteps = 0;
        float passedTime = 0;

        private void OnValidate()
        {
            substeps = Mathf.Max(1, substeps);
        }

        private void Awake()
        {
            accumulatedTime = 0;
            allIgnoreSteps = ingoreSteps;
        }

        private void OnDisable()
        {
            Physics.autoSimulation = true;
        }

        private void FixedUpdate()
        {
            passedTime += Time.fixedDeltaTime;
            if (allIgnoreSteps < ingoreSteps)
            {
                allIgnoreSteps++;
                return;
            }

            allIgnoreSteps = 0;

            ObiProfiler.EnableProfiler();

            BeginStep(passedTime);

            float substepDelta = passedTime / (float)substeps;

            // Divide the step into multiple smaller substeps:
            for (int i = 0; i < substeps; ++i)
                Substep(passedTime, substepDelta, substeps - i);

            EndStep(substepDelta);

            ObiProfiler.DisableProfiler();

            accumulatedTime -= passedTime;

            passedTime = 0;
        }

        private void Update()
        {
            ObiProfiler.EnableProfiler();
            Interpolate(Time.fixedDeltaTime, accumulatedTime);
            ObiProfiler.DisableProfiler();

            accumulatedTime += Time.deltaTime;
        }
    }
}