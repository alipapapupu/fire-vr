Fire Virtual Reality was built and optimized for Oculus Quest 2 and is fully functioning with that. 

Currently the game is not compatible with other vr device, and does not support Steam VR -platform.

How to play 

EXE: (works when any Oculus device is connected to a windows-computer via usb-cable)
1: Download the PcBuild-folder to your computer, and unzip it if needed.
2: Download and install the official Oculus app to your computer.
3: Connect your oculus device to your computer via USB-cable. For best experience, use the official Oculus Link-cable
4: Put on your Oculus-device, go to settings, and enable oculus-link.
5: Your Oculus-device should now be connected to the pc. Just double click the exe-file, and the game should start on your Oculus-device.

APK: (works only on Oculus Quest and Oculus Quest 2)
1: Enable Developer mode on your Oculus Quest device
2: Download and install 3rd party software like VRsideloader to you computer.
3: Download the games apk-file to your computer device
4: Connect the oculus device to your computer via usb cable
5: Use VRsideloader to install the apk to your device
6: Unplug the Quest from your computer, and navigate to the apps-menu with it. Select Unknown Sourcers from the top-right corner, and you should have option for the game: GameAcademy-Firevr. Select it to start the game.

Controls:
Joysticks for continuous movement
Front or Side Triggers for grabbing
Side of Front Triggers for using the the item in hand, if it has special function
In case teleportation is enabled from the settings: use B button to teleport

Extinguisher: Grab with either front or side trigger, active with other trigger on same hand.
Mobile devices: Grab with either front or side trigger, point and use with the other, non-grabbing hand.

If you face problems with installation, bugs in the game, or any other issues, please contact the projects lead programmer:
severi.jokipera@gmail.com